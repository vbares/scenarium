/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.smoothing;

import org.beanmanager.editors.container.BeanInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.math.PolyFitting;
import org.scenarium.math.SmoothingCubicSpline;
import org.scenarium.math.SmoothingFunction;
import org.scenarium.struct.curve.Curved;

public class Smoothing {
	@BeanInfo(possibleSubclasses = { PolyFitting.class, SmoothingCubicSpline.class })
	private SmoothingFunction smoothingFunction;
	private final double samplingRate = 0;

	public void birth() {}

	public void death() {}

	public SmoothingFunction getSmoothingFunction() {
		return this.smoothingFunction;
	}

	@ParamInfo(in = "in", out = "out")
	public Curved process(Curved curve) {
		SmoothingFunction sf = this.smoothingFunction;
		if (sf == null || curve == null)
			return null;
		double[][] datas = curve.getData();
		try {
			sf.resolve(datas[0], datas[1], curve.getValues());
		} catch (IllegalArgumentException e) {
			return new Curved(new double[2][0]);
		}
		if (this.samplingRate <= 0) {
			double[] xs = datas[0];
			datas = new double[2][xs.length];
			for (int i = 0; i < xs.length; i++) {
				double x = xs[i];
				datas[0][i] = x;
				datas[1][i] = sf.evaluate(x);
			}
		} else {
			double[] xs = datas[0];
			double xBegin = xs[0];
			double xEnd = xs[xs.length];
			datas = new double[2][(int) ((xEnd - xBegin) * this.samplingRate)];
			int i = 0;
			for (double x = xBegin; x < xEnd; x += this.samplingRate) {
				datas[0][i] = x;
				datas[1][i++] = sf.evaluate(x);
			}
		}
		return new Curved(datas);
	}

	public void setSmoothingFunction(SmoothingFunction smoothingFunction) {
		this.smoothingFunction = smoothingFunction;
	}
}
