/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenario.dataflowdiagram.CpuUsageMeasurable;

public class OldCPUUsageTask extends TimerTask {
	private final ThreadMXBean bean = ManagementFactory.getThreadMXBean();
	private final long id;
	private final CpuUsageMeasurable cum;
	private long cpuStartTime;
	private long start;

	private Timer cpuTimer;

	private OldCPUUsageTask(long idThread, CpuUsageMeasurable cum) {
		this.id = idThread;
		this.cum = cum;
		cum.setCpuUsage(0);
		if (this.bean.isThreadCpuTimeSupported() && this.bean.isCurrentThreadCpuTimeSupported() && this.bean.isThreadCpuTimeEnabled()) {
			this.cpuTimer = new Timer();
			this.cpuStartTime = this.bean.getThreadCpuTime(this.id);
			this.start = System.nanoTime();
			this.cpuTimer.schedule(this, 1000, 1000);
		}
	}

	public void close() {
		if (this.cpuTimer != null) {
			this.cpuTimer.cancel();
			this.cpuTimer.purge();
			this.cpuTimer = null;
		}
	}

	@Override
	public void run() {
		this.cum.setCpuUsage((float) (this.bean.getThreadCpuTime(this.id) - this.cpuStartTime) / (System.nanoTime() - this.start));
		this.start = System.nanoTime();
		this.cpuStartTime = this.bean.getThreadCpuTime(this.id);
	}

	public static OldCPUUsageTask createCPUUsageTask(long idThread, CpuUsageMeasurable cum) {
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		return bean.isThreadCpuTimeSupported() && bean.isCurrentThreadCpuTimeSupported() && bean.isThreadCpuTimeEnabled() ? new OldCPUUsageTask(idThread, cum) : null;
	}
}
