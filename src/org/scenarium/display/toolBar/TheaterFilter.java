/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.struct.TreeRoot;
import org.scenarium.display.TheaterFilterPropertyChange;
import org.scenarium.display.TheaterFilterStructChange;
import org.scenarium.display.drawer.ImageDrawer;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolbarclass.Border;
import org.scenarium.display.toolbarclass.InternalTool;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.OpenListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.test.fx.FxTest;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.Region;

class FilterCell extends CheckBoxTreeCell<String> {
	public double getComputePrefWidth() {
		return computePrefWidth(-1);
	}

	@Override
	public void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		setPrefWidth(Region.USE_COMPUTED_SIZE);
		setStyle("-fx-background-color: rgba(255,255,255,.8);");
	}
}

public class TheaterFilter extends InternalTool implements TheaterFilterStructChange, TheaterFilterPropertyChange, OpenListener {
	public static void main(String[] args) {
		new FxTest().launchIHM(args, () -> {
			TheaterFilter tf = new TheaterFilter();
			return tf.getRegion();
		});
	}

	public TreeView<String> tree;
	private TheaterPanel theaterPane;

	private boolean isUpdatedTree = false;

	private void adaptSize() {
		Platform.runLater(() -> {
			this.tree.setPrefHeight(this.tree.getExpandedItemCount() * this.tree.getFixedCellSize());
			updateWidth();
		});
	}

	@Override
	public void dispose() {
		super.dispose();
		this.theaterPane.removeTheaterFilterStructChangeListener(this);
		this.theaterPane.removeTheaterFilterPropertyChangeListener(this);
		this.theaterPane = this.renderPane.getTheaterPane();
		if (this.renderPane != null) {
			DataLoader dl = this.renderPane.getDataLoader();
			if (dl != null)
				dl.removeOpenListener(this);
		}
	}

	@Override
	public Border getBorder() {
		return Border.CENTER;
	}

	@Override
	public TreeView<String> getRegion() {
		CheckBoxTreeItem<String> rootNode = new CheckBoxTreeItem<>("Theater Filter");
		this.tree = new TreeView<>(rootNode);
		rootNode.setExpanded(true);
		this.tree.setCellFactory(lv -> new FilterCell());
		this.tree.setFixedCellSize(24);
		this.tree.setRoot(rootNode);
		this.tree.setPadding(new Insets(0));
		this.tree.setShowRoot(false);
		this.tree.setStyle("-fx-background-color: transparent;");
		this.tree.expandedItemCountProperty().addListener((e) -> adaptSize());
		this.tree.setPrefWidth(10);
		this.tree.setOpacity(0);
		this.tree.setPrefHeight(this.tree.getExpandedItemCount() * this.tree.getFixedCellSize());
		this.tree.setFocusTraversable(false);
		if (this.renderPane != null)
			this.theaterPane = this.renderPane.getTheaterPane();
		populate();
		hideScrollBar();
		if (this.renderPane != null) {
			DataLoader dl = this.renderPane.getDataLoader();
			if (dl != null)
				dl.addOpenListener(this);
			this.theaterPane.addTheaterFilterStructChangeListener(this);
			this.theaterPane.addTheaterFilterPropertyChangeListener(this);
		}
		return this.tree;
	}

	private void hideScrollBar() {
		Platform.runLater(() -> {
			try {
				ObservableList<Node> child = ((Parent) this.tree.getChildrenUnmodifiable().get(0)).getChildrenUnmodifiable();
				for (Node node : child)
					if (node instanceof ScrollBar)
						((ScrollBar) node).setPrefSize(0, 0);
				updateWidth();
				this.tree.setOpacity(1);
			} catch (IndexOutOfBoundsException e) {
				hideScrollBar();
			}
		});
	}

	@Override
	public void opened(Scenario oldScenario, Scenario scenario) {
		Platform.runLater(() -> {
			this.theaterPane.removeTheaterFilterStructChangeListener(this);
			this.theaterPane.removeTheaterFilterPropertyChangeListener(this);
			this.theaterPane = this.renderPane.getTheaterPane();
			populate();
			this.theaterPane.addTheaterFilterStructChangeListener(this);
			this.theaterPane.addTheaterFilterPropertyChangeListener(this);
		});
	}

	private void populate() {
		CheckBoxTreeItem<String> rootNode = (CheckBoxTreeItem<String>) this.tree.getRoot();
		rootNode.getChildren().clear();
		if (this.theaterPane == null) {
			ImageDrawer rd = new ImageDrawer();
			rd.initialize(null, null, new ByteRaster(800, 600, 1), false);
			this.theaterPane = rd;
		}
		TreeRoot<BooleanProperty> theaterFilter = this.theaterPane.getTheaterFilter();
		ArrayList<List<String>> exclusiveFilters = new ArrayList<>();
		this.theaterPane.populateExclusiveFilter(exclusiveFilters);
		Stack<TreeNode<BooleanProperty>> st = new Stack<>();
		Stack<CheckBoxTreeItem<String>> stn = new Stack<>();
		st.push(theaterFilter);
		stn.push(rootNode);
		while (!st.isEmpty()) {
			TreeNode<BooleanProperty> currNode = st.pop();
			CheckBoxTreeItem<String> cbti = stn.pop();
			ArrayList<TreeNode<BooleanProperty>> sons = currNode.getChildren();
			if (sons != null)
				for (TreeNode<BooleanProperty> sonNode : sons) {
					String sonName = sonNode.getValue().name;
					CheckBoxTreeItem<String> node;
					if (sonNode.getChildren() != null) {
						node = new CheckBoxTreeItem<>(sonName);
						st.push(sonNode);
						stn.push(node);
						cbti.getChildren().add(node);
					} else {
						node = new CheckBoxTreeItem<>(sonName);
						cbti.getChildren().add(node);
						node.setSelected(false); // Bug RT-8151390
						node.setSelected(true);
						node.setSelected(sonNode.getValue().value);
						node.selectedProperty().addListener((obs, oldValue, newValue) -> {
							if (this.isUpdatedTree)
								return;
							ArrayList<String> path = new ArrayList<>();
							TreeItem<String> parent = node;
							while (parent != null) {
								path.add(parent.getValue());
								parent = parent.getParent();
							}
							path.remove(path.size() - 1);
							parent = node.getParent();
							String filterName = path.get(0);
							if (newValue)
								for (List<String> exclusiveFilter : exclusiveFilters)
									if (exclusiveFilter.contains(filterName))
										for (String filter : exclusiveFilter)
											if (!filter.equals(filterName))
												for (TreeItem<String> brother : parent.getChildren())
													if (brother instanceof CheckBoxTreeItem && ((CheckBoxTreeItem<?>) brother).isSelected() && brother.getValue().equals(filter))
														((CheckBoxTreeItem<?>) brother).setSelected(false);
							Collections.reverse(path);
							this.theaterPane.updateFilterWithPath(path.toArray(new String[0]), newValue, true);
						});
					}
				}
		}

	}

	@Override
	public void theaterFilterPropertyChanged(String[] path) {
		TreeItem<String> node = this.tree.getRoot();
		TreeNode<BooleanProperty> tf = this.theaterPane.getTheaterFilter();
		Boolean value = null;
		for (int i = 0; i < path.length; i++) {
			String name = path[i];
			for (TreeItem<String> subNode : node.getChildren())
				if (subNode.getValue().equals(name)) {
					node = subNode;
					TreeNode<BooleanProperty> obj = null;
					for (TreeNode<BooleanProperty> treeNode : tf.getChildren())
						if (treeNode.getValue().name == name) {
							obj = treeNode;
							break;
						}
					if (obj.getChildren() != null && !obj.getChildren().isEmpty())
						tf = obj;
					else
						value = obj.getValue().value;
				}
		}
		this.isUpdatedTree = true;
		((CheckBoxTreeItem<String>) node).setSelected(value);
		this.isUpdatedTree = false;
	}

	@Override
	public void theaterFilterStructChanged() {
		populate();
	}

	public void updateWidth() {
		Platform.runLater(() -> {
			if (this.tree.getHeight() == this.tree.getPrefHeight()) {
				double maxWidth = -1;
				ObservableList<Node> nodes = this.tree.getChildrenUnmodifiable();
				// TODO utiliser localToParent pour calculer largeur hauteur
				if (!nodes.isEmpty()) {
					while (!(nodes.get(0) instanceof FilterCell)) {
						nodes = ((Parent) nodes.get(0)).getChildrenUnmodifiable();
						if (nodes.isEmpty())
							break;
					}
					for (Node node : nodes) {
						double width = ((FilterCell) node).getComputePrefWidth();
						if (width > maxWidth)
							maxWidth = width;
					}
				}
				this.tree.setPrefWidth(maxWidth + 20);
			} else
				updateWidth();
			Platform.runLater(() -> this.tree.setPrefHeight(Math.min(this.theaterPane.getHeight(), this.tree.getExpandedItemCount() * this.tree.getFixedCellSize())));

		});
	}
}