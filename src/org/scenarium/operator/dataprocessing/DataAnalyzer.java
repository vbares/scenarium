/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import java.lang.reflect.Array;
import java.util.Timer;
import java.util.TimerTask;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.Curved;

public class DataAnalyzer extends EvolvedOperator implements DynamicEnableBean {
	private volatile long cpt;
	@PropertyInfo(index = 0, nullable = false, info = "Specify the metadata to analyze.")
	private Mode mode = Mode.DATACOUNT;
	@PropertyInfo(index = 1, unit = "ms", info = "Number of sample for the data rate curve")
	private int nbSample = 100;
	@PropertyInfo(index = 2, unit = "ms", info = "Integration time for the data rate")
	private int integrationTime = 1000;

	private Timer dataRateTimer;
	private long oldTs = -1;
	private double[][] oldCurveData;

	@Override
	public void birth() {
		this.cpt = 0;
	}

	@Override
	public void death() {
		if (this.dataRateTimer != null) {
			this.dataRateTimer.cancel();
			this.dataRateTimer = null;
		}
		this.oldCurveData = null;
		this.oldTs = -1;
		this.oldCurveData = null;
	}

	public int getIntegrationTime() {
		return this.integrationTime;
	}

	public Mode getMode() {
		return this.mode;
	}

	public int getNbSample() {
		return this.nbSample;
	}

	@Override
	public void initStruct() {
		updateOutputs(new String[] { "out" },
				new Class<?>[] { this.mode == Mode.DATACOUNT || this.mode == Mode.DATARATE ? double.class : this.mode == Mode.TIMESTAMP || this.mode == Mode.TIMEOFISSUE ? long.class : Curve.class });
	}

	public void process(Object data) {
		switch (this.mode) {
		case TIMESTAMP:
			triggerOutput(new Object[] { getTimeStamp(0) }, getTimeStamp(0));
			break;
		case TIMEOFISSUE:
			triggerOutput(new Object[] { getTimeOfIssue(0) }, getTimeStamp(0));
			break;
		case DATACOUNT:
			triggerOutput(new Object[] { ++this.cpt }, getTimeStamp(0));
			break;
		case DATARATE:
			if (this.dataRateTimer == null) {
				this.dataRateTimer = new Timer();
				long lastTs = getTimeStamp(0);
				this.dataRateTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						triggerOutput(new Object[] { DataAnalyzer.this.cpt / (DataAnalyzer.this.integrationTime / 1000.0) }, lastTs);
						DataAnalyzer.this.cpt = 0;
					}
				}, 0, this.integrationTime);
			}
			this.cpt++;
			break;
		case DATARATECURVE:
			long ts = getTimeStamp(0);
			this.cpt++;
			if (this.oldTs != -1) {
				long deltaTs = ts - this.oldTs;
				if (this.oldCurveData == null)
					this.oldCurveData = new double[2][(int) this.cpt - 2];
				double[] x = this.oldCurveData[0];
				double[] y = this.oldCurveData[1];
				if (x.length == this.nbSample)
					for (int i = 0; i < x.length - 1; i++) {
						x[i] = x[i + 1];
						y[i] = y[i + 1];
					}
				else {
					double[][] curveData = new double[2][(int) this.cpt - 1];
					double[] newX = curveData[0];
					double[] newY = curveData[1];
					for (int i = 0; i < x.length; i++) {
						newX[i] = x[i];
						newY[i] = y[i];
					}
					this.oldCurveData = curveData;
					x = this.oldCurveData[0];
					y = this.oldCurveData[1];
				}
				x[x.length - 1] = this.cpt - 1;
				y[y.length - 1] = deltaTs;
				triggerOutput(new Object[] { new Curved(this.oldCurveData) }, getTimeStamp(0));
			}
			this.oldTs = ts;
			break;
		case ARRAYSIZE:
			triggerOutput(new Object[] { data.getClass().isArray() ? Array.getLength(data) : -1 }, getTimeStamp(0));
			break;
		default:
			throw new IllegalArgumentException("Mode: " + this.mode + " is not supported");

		}
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "nbSample", this.mode == Mode.DATARATECURVE);
		fireSetPropertyEnable(this, "integrationTime", this.mode == Mode.DATARATE);
	}

	public void setIntegrationTime(int integrationTime) {
		this.integrationTime = integrationTime;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
		restartAndReloadStructLater();
		setEnable();
	}

	public void setNbSample(int nbSample) {
		this.nbSample = nbSample;
	}
}
