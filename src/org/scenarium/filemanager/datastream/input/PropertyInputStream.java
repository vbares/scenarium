/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.input;

import java.io.IOException;

import org.beanmanager.editors.PropertyEditor;

public class PropertyInputStream<T> extends DataFlowInputStream<T> {
	private final PropertyEditor<T> editor;

	public PropertyInputStream(PropertyEditor<T> editor) {
		this.editor = editor;
	}

	public PropertyEditor<T> getEditor() {
		return this.editor;
	}

	@Override
	public T pop() throws IOException {
		return this.editor.readValue(this.dataInput);
	}
}
