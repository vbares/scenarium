/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class Rescale extends EvolvedOperator {
	@PropertyInfo(index = 0)
	private int width = 640;
	@PropertyInfo(index = 1)
	private int height = 480;
	@PropertyInfo(index = 2)
	private INTERPOLATIONTYPE interpolationType = INTERPOLATIONTYPE.TYPE_BILINEAR;
	private BufferedImage outRaster;

	@Override
	public void birth() throws Exception {}

	@Override
	public void death() throws Exception {
		this.outRaster = null;
	}

	public int getHeight() {
		return this.height;
	}

	public INTERPOLATIONTYPE getInterpolationType() {
		return this.interpolationType;
	}

	public int getWidth() {
		return this.width;
	}

	@ParamInfo(in = "In", out = "Out")
	public BufferedImage process(BufferedImage raster) {
		AffineTransform tx = new AffineTransform();
		tx.scale((float) this.width / raster.getWidth(), (float) this.height / raster.getHeight());
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		if (this.outRaster == null || this.outRaster.getType() != raster.getType() || this.outRaster.getWidth() != this.width || this.outRaster.getHeight() != this.height)
			this.outRaster = new BufferedImage(this.width, this.height, raster.getType());
		return op.filter(raster, this.outRaster);
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setInterpolationType(INTERPOLATIONTYPE interpolationType) {
		this.interpolationType = interpolationType;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}