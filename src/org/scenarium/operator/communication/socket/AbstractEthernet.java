/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.socket;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.operator.communication.AbstractNetwork;
import org.scenarium.operator.communication.serial.DataType;

public abstract class AbstractEthernet extends AbstractNetwork {
	@PropertyInfo(index = 50, unit = "ms", info = "Read timeout")
	@NumberInfo(min = 0)
	protected int timeOut = 1000;
	@PropertyInfo(index = 52, unit = "Byte", info = "Receive buffer size")
	@NumberInfo(min = 0)
	protected int receiveBufferSize = 4098;
	protected Thread ethernetThread;

	public AbstractEthernet() {}

	public AbstractEthernet(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int timeOut, int receiveBufferSize) {
		super(inDataType, outDataType, connectionFlag, verbose);
		this.timeOut = timeOut;
		this.receiveBufferSize = receiveBufferSize;
	}

	@Override
	public void birth() {
		onStart(() -> start());
	}

	@Override
	public void death() {
		if (this.ethernetThread != null) {
			this.ethernetThread.interrupt();
			try {
				this.ethernetThread.join();
				stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.ethernetThread = null;
		}
	}

	public int getReceiveBufferSize() {
		return this.receiveBufferSize;
	}

	public int getTimeOut() {
		return this.timeOut;
	}

	public void setReceiveBufferSize(int bufferSize) {
		this.receiveBufferSize = bufferSize;
		restartLater();
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
		restartLater();
	}

	protected abstract void start();

	protected abstract void stop();
}
