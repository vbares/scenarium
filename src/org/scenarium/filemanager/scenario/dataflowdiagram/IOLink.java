/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public class IOLink {
	private final int outputIndex;
	private final int linkInputsIndex;
	private final Input input;
	private final boolean needToCopy;

	public IOLink(int outputIndex, int linkInputsIndex, Input input, boolean needToCopy) {
		this.outputIndex = outputIndex;
		this.linkInputsIndex = linkInputsIndex;
		this.input = input;
		this.needToCopy = needToCopy;
	}

	public Input getInput() {
		return this.input;
	}

	public int getLinkInputsIndex() {
		return this.linkInputsIndex;
	}

	public int getOutputIndex() {
		return this.outputIndex;
	}

	public boolean isNeedToCopy() {
		return this.needToCopy;
	}

	@Override
	public String toString() {
		return "oi: " + this.outputIndex + " lii: " + this.linkInputsIndex + " copy: " + this.needToCopy;
	}
}