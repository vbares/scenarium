/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import java.util.ArrayList;

import org.scenicview.ScenicView;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AccordionHeightBug extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	private static void populate(Accordion a, int nb) {
		a.getPanes().clear();
		ArrayList<TitledPane> titledPanes = new ArrayList<>();
		for (int i = 0; i < nb; i++) {
			TitledPane t1 = new TitledPane("c" + i, new Button("cool"));
			t1.setAnimated(false);
			titledPanes.add(t1);
		}
		a.getPanes().setAll(titledPanes);
		// a.applyCss();
		// a.layout();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Accordion a = new Accordion();
		populate(a, 2);
		Button addb = new Button("add");
		addb.setOnAction(e -> populate(a, 3));
		Scene s = new Scene(new VBox(addb, a), 320, 240);
		primaryStage.setScene(s);
		ScenicView.show(s);
		primaryStage.show();
	}
}
