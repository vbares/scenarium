/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.operator.communication.serial.DataType;

public class SocketTCP extends AbstractEthernet {
	@PropertyInfo(index = 0, nullable = false, info = "The desired ethernet adresse to use\nIf no ip is specified, this block will be considered as the server")
	private InetSocketAddress address;

	private Socket socket;
	private ServerSocket serveurSocket;
	private ObjectOutputStream objectOutputStream;

	public SocketTCP() {
		try {
			this.address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public SocketTCP(DataType dataTypeIn, DataType dataTypeOut, boolean connectionFlag, boolean verbose, int timeOut, int bufferSize, InetSocketAddress inetSocketAddress) {
		super(dataTypeIn, dataTypeOut, connectionFlag, verbose, timeOut, bufferSize);
		if (inetSocketAddress == null)
			throw new IllegalArgumentException("the adress cannot be null");
		this.address = inetSocketAddress;
	}

	public InetSocketAddress getAddress() {
		return this.address;
	}

	@ParamInfo(out = "Out")
	public void process() throws Exception {
		Object in = getAdditionalInputs()[0];
		java.net.Socket socket = this.socket;
		if (socket != null)
			try {
				if (this.inDataType == DataType.OBJECT) {
					if (this.objectOutputStream == null)
						this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
					this.objectOutputStream.writeObject(in);
					this.objectOutputStream.flush();
				} else if (in instanceof byte[] && this.inDataType == DataType.BYTE)
					socket.getOutputStream().write((byte[]) in);
				else if (in instanceof String && this.inDataType == DataType.STRING)
					socket.getOutputStream().write(((String) in).getBytes());
			} catch (IOException e) {
				if (this.verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				start();
			}
	}

	public void setAddress(InetSocketAddress address) {
		if (address != null) {
			this.address = address;
			restartLater();
		}
	}

	@Override
	protected synchronized void start() {
		death();
		this.ethernetThread = new Thread(() -> {
			while (isRunning()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				InputStream is = null;
				do {
					try {
						if (this.socket == null) {
							if (this.address.getAddress().equals(InetAddress.getByName("0.0.0.0"))) {
								this.serveurSocket = new ServerSocket(this.address.getPort());
								this.serveurSocket.setSoTimeout(this.timeOut);
								this.socket = this.serveurSocket.accept();
							} else
								this.socket = new Socket(this.address.getAddress(), this.address.getPort());
							this.socket.setSoTimeout(this.timeOut);
							this.socket.setReceiveBufferSize(this.receiveBufferSize);
							connectionEstablished(true);
						}
						if (this.outDataType == DataType.DISABLE)
							return;
						is = this.socket.getInputStream();
					} catch (IOException e) {
						if (this.verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!isRunning())
							return;
						if (this.socket == null) {
							if (this.serveurSocket != null)
								try {
									this.serveurSocket.close();
								} catch (IOException ex) {
									ex.printStackTrace();
								}
							try {
								Thread.sleep(this.timeOut);
							} catch (InterruptedException ex) {
								return;
							}
						}
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (is == null && isRunning());
				if (this.socket == null)
					return;
				try {
					if (this.outDataType == DataType.STRING)
						try (BufferedReader buf = new BufferedReader(new InputStreamReader(this.socket.getInputStream()))) {
							while (isRunning()) {
								if (Thread.currentThread().isInterrupted()) {
									stop();
									return;
								}
								triggerData(buf.readLine());
							}
						}
					else if (this.outDataType == DataType.BYTE) {
						byte[] readBuff = new byte[this.receiveBufferSize];
						while (isRunning()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							int nbBytes = is.read(readBuff);
							if (nbBytes > 0)
								triggerByteArrayData(readBuff, nbBytes);
						}
					} else if (this.outDataType == DataType.OBJECT) {
						ObjectInputStream objectOutput = new ObjectInputStream(this.socket.getInputStream());
						while (isRunning()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							triggerData(objectOutput.readObject());
						}
					}
					stop();
				} catch (IOException | ClassNotFoundException e) {
					if (isRunning()) {
						if (this.verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!e.getClass().equals(SocketTimeoutException.class))
							stop();
					}
				}
			}
		});
		this.ethernetThread.start();
	}

	@Override
	protected synchronized void stop() {
		if (this.socket != null) {
			try {
				this.socket.close();
			} catch (IOException e) {}
			connectionEstablished(false);
			this.socket = null;
		}
		if (this.serveurSocket != null) {
			try {
				this.serveurSocket.close();
			} catch (IOException e) {}
			this.serveurSocket = null;
		}
		if (this.objectOutputStream != null) {
			try {
				this.objectOutputStream.close();
			} catch (IOException e) {}
			this.objectOutputStream = null;
		}
	}

	private void triggerByteArrayData(byte[] readBuff, int nbBytes) {
		byte[] datas = new byte[nbBytes];
		System.arraycopy(readBuff, 0, datas, 0, datas.length);
		triggerData(datas);
	}
}
