/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.util.EventListener;

import org.scenarium.display.toolbarclass.Tool;

public interface ToolVisibleListener extends EventListener {
	void toolVisibleChanged(Class<? extends Tool> toolBarClass, boolean visible);

}
