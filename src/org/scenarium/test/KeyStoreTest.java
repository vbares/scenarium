/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class KeyStoreTest {

	private KeyStoreTest() {}

	public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException {
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		char[] password = "PASSWORD TO KEYSATORE".toCharArray();
		ks.load(new FileInputStream("PATH TO KEYSTORE.pfx"), password); // new FileInputStream("PATH TO KEY STORE")
		ks.getProvider();
		KeyStore.SecretKeyEntry key = (KeyStore.SecretKeyEntry) ks.getEntry("mykey", new KeyStore.PasswordProtection(password));
		SecretKey myPrivateKey = key.getSecretKey();
		System.out.println(new String(myPrivateKey.getEncoded()));
		SecretKey mySecretKey = new SecretKeySpec("Dauphin".getBytes(), "AES");
		KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(mySecretKey);
		ks.setEntry("mykey", skEntry, new KeyStore.PasswordProtection(password));
		ks.store(new java.io.FileOutputStream("PATH TO KEYSTORE.pfx"), password);
		System.out.println("done");
	}
}
