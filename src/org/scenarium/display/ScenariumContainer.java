/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import javax.vecmath.Point2i;

import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.timescheduler.Scheduler;

public interface ScenariumContainer {
	void adaptSizeToDrawableElement();

	Point2i getDefaultToolBarLocation(String simpleName);

	Scheduler getScheduler();

	int getSelectedElementFromTheaterEditor();

	boolean isDefaultToolBarAlwaysOnTop(String simpleName);

	boolean isManagingAccelerator();

	boolean isStatusBar();

	void saveScenario();

	void showMessage(String message, boolean error);

	void updateStatusBar(String... infos);

	void updateToolView(Class<? extends Tool> toolClass, boolean isVisible);

	void showTool(Class<? extends Tool> toolClass);
}
