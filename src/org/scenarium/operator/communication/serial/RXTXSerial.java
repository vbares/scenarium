/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.serial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

public class RXTXSerial extends EvolvedOperator implements BeanRenameListener {
	// private static final String[] RXTXLIB = {/* "rxtxParallel", */"rxtxSerial" };
	// private static boolean isAvailable = true;

	public static void main(String[] args) {
		System.out.println(Arrays.toString(new RXTXSerial().getAvailablePort()));
	}

	@PropertyInfo(index = 0)
	@DynamicPossibilities(possibleChoicesMethod = "getAvailablePort", useSwingWorker = true)
	private String commPort;
	@PropertyInfo(index = 1)
	private int baudrate = 4800;
	@PropertyInfo(index = 2)
	private DataType inDataType = DataType.STRING;
	@PropertyInfo(index = 3)
	private DataType outDataType = DataType.STRING;
	@PropertyInfo(index = 4)
	private RXTXDataBits dataBits = RXTXDataBits.DATABITS_8;
	@PropertyInfo(index = 5)
	private RXTXStopBits stopBits = RXTXStopBits.STOPBITS_1;
	@PropertyInfo(index = 6)
	private RXTXParity parity = RXTXParity.PARITY_NONE;

	@PropertyInfo(index = 7)
	private boolean verbose = false;
	private Thread sor;
	private boolean isRunning = false;
	private SerialPort sp = null;
	private OutputStream os = null;

	private ObjectOutputStream objectOutputStream;

	@Override
	public void birth() throws Exception {
		// if (isAvailable)
		onStart(() -> startReceiver());
		this.isRunning = true;
	}

	@Override
	public void death() throws Exception {
		this.isRunning = false;
		stopReceiver();
	}

	public String[] getAvailablePort() {
		Enumeration<?> ports = CommPortIdentifier.getPortIdentifiers();
		ArrayList<CommPortIdentifier> commPorts = new ArrayList<>();
		while (ports.hasMoreElements()) {
			Object e = ports.nextElement();
			if (e instanceof CommPortIdentifier)
				commPorts.add((CommPortIdentifier) e);
		}
		String[] availablePorts = new String[commPorts.size()];
		for (int i = 0; i < commPorts.size(); i++)
			availablePorts[i] = commPorts.get(i).getName();
		return availablePorts;
	}

	public int getBaudrate() {
		return this.baudrate;
	}

	public String getCommPort() {
		return this.commPort;
	}

	public RXTXDataBits getDataBits() {
		return this.dataBits;
	}

	public DataType getInDataType() {
		return this.inDataType;
	}

	public DataType getOutDataType() {
		return this.outDataType;
	}

	public RXTXParity getParity() {
		return this.parity;
	}

	public RXTXStopBits getStopBits() {
		return this.stopBits;
	}

	@Override
	public void initStruct() {
		if (this.inDataType != DataType.DISABLE)
			updateInputs(new String[] { "in" }, new Class[] { this.inDataType == DataType.BYTE ? byte[].class : String.class });
		else
			updateInputs(new String[0], new Class[0]);
		if (this.outDataType != DataType.DISABLE)
			updateOutputs(new String[] { getBlockName() }, new Class[] { this.outDataType == DataType.BYTE ? byte[].class : String.class });
		else
			updateOutputs(new String[0], new Class[0]);
		removeBlockNameChangeListener(this);
		addBlockNameChangeListener(this);
	}

	public boolean isVerbose() {
		return this.verbose;
	}

	@ParamInfo(in = "in", out = "Out")
	public void process() {
		Object in = getAdditionalInputs()[0];
		OutputStream os = this.os;
		if (os != null)
			try {
				if (this.inDataType == DataType.OBJECT) {
					if (this.objectOutputStream == null)
						this.objectOutputStream = new ObjectOutputStream(os);
					this.objectOutputStream.writeObject(in);
					this.objectOutputStream.flush();
				} else if (in instanceof byte[] && this.inDataType == DataType.BYTE)
					os.write((byte[]) in);
				else if (in instanceof String && this.inDataType == DataType.STRING)
					os.write(((String) in).getBytes());
			} catch (IOException e) {
				if (this.verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				startReceiver();
			}
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
		if (this.isRunning)
			startReceiver();
	}

	public void setCommPort(String commPort) {
		this.commPort = commPort;
		if (this.isRunning)
			startReceiver();
	}

	public void setDataBits(RXTXDataBits dataBits) {
		this.dataBits = dataBits;
		if (this.isRunning)
			startReceiver();
	}

	public void setInDataType(DataType inDataType) {
		this.inDataType = inDataType;
		initStruct();
		restart();
	}

	public void setOutDataType(DataType outDataType) {
		this.outDataType = outDataType;
		initStruct();
		restart();
	}

	public void setParity(RXTXParity parity) {
		this.parity = parity;
		if (this.isRunning)
			startReceiver();
	}

	public void setStopBits(RXTXStopBits stopBits) {
		this.stopBits = stopBits;
		if (this.isRunning)
			startReceiver();
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	private void startReceiver() {
		stopReceiver();
		this.sor = new Thread(() -> {
			InputStream is = null;
			while (this.isRunning) {
				try {
					Enumeration<CommPortIdentifier> ports = CommPortIdentifier.getPortIdentifiers();
					CommPortIdentifier id = null;
					while (ports.hasMoreElements()) {
						CommPortIdentifier e = ports.nextElement();
						if (e.getName().equals(this.commPort)) {
							id = e;
							break;
						}
					}
					if (id == null) {
						if (this.verbose)
							System.err.println(getBlockName() + ": The port: " + this.commPort + " is not available");
						return;
					}
					while (this.sp == null)
						try {
							synchronized (RXTXSerial.class) {
								this.sp = (SerialPort) id.open(getBlockName(), 1000);
							}
						} catch (PortInUseException e) {
							if (this.verbose)
								System.err.println("Port: " + this.commPort + " is in used");
							Thread.sleep(1000);
						}
					this.sp.setSerialPortParams(this.baudrate, this.dataBits.getValue(), this.stopBits.getValue(), this.parity.getValue());
					this.sp.enableReceiveTimeout(1000);
					this.os = this.sp.getOutputStream();
					if (this.outDataType == DataType.DISABLE)
						return;
					is = this.sp.getInputStream();
					if (this.outDataType == DataType.STRING)
						try (BufferedReader buf = new BufferedReader(new InputStreamReader(this.sp.getInputStream()))) {
							while (this.isRunning)
								try {
									triggerOutput(buf.readLine());
								} catch (IOException e) {
									if (this.verbose)
										System.err.println(getBlockName() + ": no serial data received");
								}
						}
					else if (this.outDataType == DataType.BYTE) {
						byte[] readBuff = new byte[40000];
						while (isRunning()) {
							int nbBytes = is.read(readBuff);
							if (nbBytes > 0) {
								byte[] datas = new byte[nbBytes];
								System.arraycopy(readBuff, 0, datas, 0, datas.length);
								triggerOutput(datas);
							}
						}
					} else if (this.outDataType == DataType.OBJECT) {
						ObjectInputStream objectOutput = new ObjectInputStream(is);
						while (isRunning())
							triggerOutput(objectOutput.readObject());
					}
				} catch (Exception e) {
					if (this.sp != null) {
						this.sp.close();
						this.sp = null;
					}
					if (!(e instanceof InterruptedException)) {
						System.err.println(getBlockName() + ": Error: " + e.getClass().getSimpleName());
						e.printStackTrace();
					}
				}
				if (this.sp != null) {
					this.sp.close();
					this.sp = null;
				}
				if (this.os != null) {
					try {
						this.os.close();
					} catch (IOException e) {}
					this.os = null;
				}
				if (this.objectOutputStream != null) {
					try {
						this.objectOutputStream.close();
					} catch (IOException e) {}
					this.objectOutputStream = null;
				}
				if (is != null)
					try {
						is.close();
					} catch (Exception e) {
						System.err.println(getBlockName() + ": Cannot close the stream: " + e.getClass().getSimpleName() + ": " + e.getMessage());
					}
			}
		});
		this.sor.start();
	}

	private void stopReceiver() {
		if (this.sor != null) {
			this.sor.interrupt();
			try {
				this.sor.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.sor = null;
		}
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		initStruct();
	}
}