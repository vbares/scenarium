/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Random;

public class Doublebyte {

	private Doublebyte() {}

	// public static void main(String[] args) {
	//
	// }

	interface IntWriter {
		void write(double[] ints);
	}

	// public static byte[] toByteArray(double[] intArray){
	// int times = Integer.SIZE / Byte.SIZE;
	// byte[] bytes = new byte[intArray.length * times];
	// for(int i=0;i<intArray.length;i++){
	// ByteBuffer.wrap(bytes, i*times, times).putdouble(intArray[i]);
	// }
	// return bytes;
	// }

	private static final int NUM_INTS = 100;

	public static void main(String[] args) {
		double[] ints = new double[NUM_INTS];
		Random r = new Random();
		for (int i = 0; i < NUM_INTS; i++)
			ints[i] = r.nextDouble();
		time("DataOutputStream", ints1 -> storeDO(ints1), ints);
		// time("ObjectOutputStream", new IntWriter() {
		// public void write(double[] ints) {
		// storeOO(ints);
		// }
		// }, ints);
		time("FileChannel", ints1 -> storeFC(ints1), ints);
	}

	private static void safeClose(OutputStream out) {
		try {
			if (out != null)
				out.close();
		} catch (IOException e) {
			// do nothing
		}
	}

	private static void safeClose(RandomAccessFile out) {
		try {
			if (out != null)
				out.close();
		} catch (IOException e) {
			// do nothing
		}
	}

	private static void storeDO(double[] ints) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new FileOutputStream("data.out"));
			for (int i = 0; i < 10000; i++)
				for (double anInt : ints)
					out.writeDouble(anInt);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void storeFC(double[] ints) {
		RandomAccessFile out = null;
		try {
			out = new RandomAccessFile("fc.out", "rw");
			for (int i = 0; i < 10000; i++) {
				ByteBuffer buff = ByteBuffer.allocate(8 * ints.length);
				for (double j : ints)
					buff.putDouble(j);
				out.write(buff.array());

				// FileChannel file = out.getChannel();
				// ByteBuffer buf = file.map(FileChannel.MapMode.READ_WRITE, i * 8 * ints.length, 8 * ints.length);
				// for (double j : ints) {
				// buf.putDouble(j);
				// }
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			safeClose(out);
		}
	}

	private static void time(String name, IntWriter writer, double[] ints) {
		long start = System.nanoTime();
		writer.write(ints);
		long end = System.nanoTime();
		double ms = (end - start) / 1000000d;
		System.out.printf("%s wrote %,d double in %,.3f ms%n", name, ints.length, ms);
	}

	public static double[] toDoubleArray(byte[] byteArray) {
		return ByteBuffer.wrap(byteArray).asDoubleBuffer().array();
	}
}
