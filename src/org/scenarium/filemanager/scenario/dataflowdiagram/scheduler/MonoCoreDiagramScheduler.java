/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.beanmanager.tools.Tuple;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOData;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLink;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLinks;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.ManagingCpuUsageListener;
import org.scenarium.struct.Triple;

public class MonoCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	protected HashMap<Block, Object[]> blocksEntries = new HashMap<>();
	protected LinkedBlockingDeque<Long> timePointerConsumptionStack;
	private final MonoScheduler ms;
	private CPUUsageTask cpuUsageTask;

	public MonoCoreDiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks, CountDownLatch startLock) {
		super(fd, schedulableBlocks);
		this.timePointerConsumptionStack = new LinkedBlockingDeque<>();
		this.ms = new MonoScheduler(this, fd, schedulableBlocks, startLock);
		launch(this.ms);
		managingCpuChanged(fd.isManagingCpuUsage());
		fd.addManagingCpuUsage(this);
		waitMonoSchedulerStart(startLock);
	}

	@Override
	public void removeBlock(Block block) {
		DiagramScheduler.destroyBlock(block, block.isEnable());
	}

	@Override
	public void addBlock(Block block) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.createBlock(block);
	}

	@Override
	public boolean isAlive() {
		MonoScheduler ms = this.ms;
		return ms != null && !ms.dead;
	}

	@Override
	public boolean isStarted() {
		MonoScheduler ms = this.ms;
		return ms != null && ms.isStarted;
	}

	protected void launch(Thread t) {
		MonoScheduler ms = this.ms;
		if (ms != null)
			ms.start();
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (managingCpu && this.cpuUsageTask == null)
			try {
				this.cpuUsageTask = CPUUsageTask.createCPUUsageTask(this.ms.getId(), this.fd);
			} catch (UnsupportedOperationException e) {
				System.err.println(e.getMessage());
				return;
			}
		else if (!managingCpu && this.cpuUsageTask != null) {
			this.cpuUsageTask.close();
			this.cpuUsageTask = null;
		}
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		addOnEvent(source, mds -> {
			if (mds.isStarted)
				runnable.run();
			else
				mds.onStart(source, runnable);
		});
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onResume(source, runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onPause(source, runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onStop(source, runnable));
	}

	private void addOnEvent(Object source, Consumer<MonoScheduler> consumer) {
		MonoScheduler mds = this.ms;
		if (mds == null || mds.dead)
			new IllegalAccessError("The scheduler is dead. The started event cannot therefore occur");
		else
			consumer.accept(mds);
	}

	@Override
	public void start() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnStartTasks();
	}

	@Override
	public void resume() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnResumeTasks();
	}

	@Override
	public void pause() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnPauseTasks();
	}

	@Override
	public void preStop() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnStopTasks();
	}

	@Override
	public void stop() {
		// System.err.println("stop " + getClass().getSimpleName() + " id: " + Thread.currentThread().getId());
		this.fd.removeManagingCpuUsageListener(this);
		if (this.cpuUsageTask != null) {
			this.cpuUsageTask.close();
			this.cpuUsageTask = null;
		}
		this.fd.setCpuUsage(0);
		if (this.ms != null) {
			this.ms.lock.lock();
			this.ms.dead = true;
			try {
				this.ms.incomingDataOrTaskSignals.signalAll();
				this.ms.schedulerAvailable.signalAll();
			} finally {
				this.ms.lock.unlock();
			}
			try {
				// System.err.println("join: " + Thread.currentThread().getId());
				this.ms.join(1000);
			} catch (InterruptedException e) {
				// System.err.println("Thread id: " + Thread.currentThread().getId());
				e.printStackTrace(); // TODO Bug Lancer une fois je sais pas pk... une fois de plus en monocore...
			}
			if (this.ms.isAlive()) {
				BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(this.ms.blockingBlock, () -> {
						this.ms.isInterrupted = true;
						this.ms.interrupt();
					}));
					stopTimeOutThread.start();
				}

				try {
					this.ms.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		}
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		for (int i = 0; i < timeStamps.length; i++)
			timeStamps[i] = timeStamp;
		triggerOutput(source, outputValues, timeStamps);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.triggerOutput((IOComponent) source, outputValues, timeStamps);
	}

	@Override
	public void triggerTask(Object source, Runnable task) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.triggerTask(source, task);
		else
			task.run();
	}

	private void waitMonoSchedulerStart(CountDownLatch startLock) {
		try {
			boolean isStarted = startLock.await(1, TimeUnit.SECONDS);
			if (!isStarted) {
				BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(this.ms.blockingBlock, () -> {
						this.ms.isInterrupted = true;
						MonoCoreDiagramScheduler.this.ms.interrupt();
						try {
							MonoCoreDiagramScheduler.this.ms.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}));
					stopTimeOutThread.start();
				}
				startLock.await();
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void blockNameChanged(Block block) {}
}

class MonoScheduler extends Thread {
	private final Object onTaskLock = new Object();
	private ArrayList<OnConsumeTask> onStartTasks;
	private ArrayList<OnConsumeTask> onPauseTasks;
	private ArrayList<OnConsumeTask> onResumeTasks;
	private ArrayList<OnConsumeTask> onStopTasks;

	private final BlockRingBuffer todoBlocks = new BlockRingBuffer();
	private final HashMap<IOComponent, Triple<Object[], long[], long[]>> blockBuffs = new HashMap<>();
	private final MonoCoreDiagramScheduler scds;
	private final FlowDiagram fd;
	private IOComponent todoSource;
	private Object[] todoOutputValues;
	private long[] todoTimeStamps;
	volatile boolean dataRead = false;
	protected boolean isStarted = false;

	protected boolean dead = false;
	final ReentrantLock lock = new ReentrantLock();
	final Condition incomingDataOrTaskSignals = this.lock.newCondition();
	final Condition schedulerAvailable = this.lock.newCondition();
	private final CountDownLatch startLock;
	protected List<Block> blockingBlock;
	public boolean isInterrupted = false;
	private long callingThreadId = -1;
	private Thread monoRemoteThread;

	public List<Tuple<Condition, Condition>> tasks = Collections.synchronizedList(new ArrayList<>());

	public MonoScheduler(MonoCoreDiagramScheduler scds, FlowDiagram fd, ArrayList<Block> schedulableBlocks, CountDownLatch startLock2) {
		this.scds = scds;
		this.fd = scds.fd;
		this.startLock = startLock2;
		this.blockingBlock = Collections.synchronizedList(new LinkedList<>());
		setName("MonoCoreDiagramScheduler");
	}

	private void consumeTasks() {
		while (!this.tasks.isEmpty()) {
			Tuple<Condition, Condition> task = this.tasks.remove(0);
			this.lock.lock();
			try {
				task.getFirst().signal();
				task.getSecond().await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				this.lock.unlock();
			}
		}
	}

	public void createBlock(Block block) {
		DiagramScheduler.initBlock(block, this.scds);
		this.scds.initOperator(block);
	}

	public void onStart(Object source, Runnable runnable) {
		synchronized (this.onTaskLock) {
			ArrayList<OnConsumeTask> onStartTasks = this.onStartTasks;
			if (onStartTasks == null)
				onStartTasks = new ArrayList<>();
			onStartTasks.add(new OnConsumeTask(source, runnable));
			this.onStartTasks = onStartTasks;
		}
	}

	public void onResume(Object source, Runnable runnable) {
		synchronized (this.onTaskLock) {
			ArrayList<OnConsumeTask> onResumeTasks = this.onResumeTasks;
			if (onResumeTasks == null)
				onResumeTasks = new ArrayList<>();
			onResumeTasks.add(new OnConsumeTask(source, runnable));
			this.onResumeTasks = onResumeTasks;
		}
	}

	public void onPause(Object source, Runnable runnable) {
		synchronized (this.onTaskLock) {
			ArrayList<OnConsumeTask> onPauseTasks = this.onPauseTasks;
			if (onPauseTasks == null)
				onPauseTasks = new ArrayList<>();
			onPauseTasks.add(new OnConsumeTask(source, runnable));
			this.onPauseTasks = onPauseTasks;
		}
	}

	public void onStop(Object source, Runnable runnable) {
		synchronized (this.onTaskLock) {
			ArrayList<OnConsumeTask> onStopTasks = this.onStopTasks;
			if (onStopTasks == null)
				onStopTasks = new ArrayList<>();
			onStopTasks.add(new OnConsumeTask(source, runnable));
			this.onStopTasks = onStopTasks;
		}
	}

	public void consumeOnStartTasks() {
		consumeOnEventTasks(this.onStartTasks);
	}

	public void consumeOnResumeTasks() {
		consumeOnEventTasks(this.onResumeTasks);
	}

	public void consumeOnPauseTasks() {
		consumeOnEventTasks(this.onPauseTasks);
	}

	public void consumeOnStopTasks() {
		consumeOnEventTasks(this.onStopTasks);
	}

	private void consumeOnEventTasks(ArrayList<OnConsumeTask> tasks) {
		if (tasks != null)
			tasks.forEach(t -> this.scds.triggerTask(t.source, t.runnable));
	}

	@Override
	public void run() {
		List<Block> blocks = this.fd.getBlocks();
		blocks.sort((a, b) -> Integer.compare(b.getStartPriority(), a.getStartPriority()));
		for (Block block : blocks) {
			this.blockingBlock.add(block);
			createBlock(block);
			this.blockingBlock.remove(block);
		}
		this.startLock.countDown();
		this.isStarted = true;
		// synchronized (this) {
		// if (onStartTasks != null) {
		// onStartTasks.forEach(r -> r.runnable.run());
		// onStartTasks = null;
		// }
		// }
		dead: while (!this.dead) {
			IOComponent source = null;
			Object[] outputValues = null;
			long[] timeStamps = null;
			this.lock.lock();
			try {
				while (true) {
					if (this.dead)
						break dead;
					if (!this.tasks.isEmpty())
						consumeTasks();
					if (this.todoSource != null)
						break;
					this.incomingDataOrTaskSignals.await();
				}
				if (this.todoSource != null) {
					source = this.todoSource;
					this.todoSource = null;
					outputValues = this.todoOutputValues;
					this.todoOutputValues = null;
					timeStamps = this.todoTimeStamps;
					this.todoTimeStamps = null;
					this.schedulerAvailable.signal();
				}
			} catch (InterruptedException e) {
				break;
			} finally {
				this.lock.unlock();
			}
			if (isInterrupted())
				break;
			if (consumeDatas(source, outputValues, timeStamps))
				break;
		}
		this.lock.lock();
		try {
			this.schedulerAvailable.signalAll();
			this.incomingDataOrTaskSignals.signalAll();
		} finally {
			this.lock.unlock();
		}
		consumeTasks();
		blocks = this.fd.getBlocks();
		blocks.sort((a, b) -> Integer.compare(b.getStopPriority(), a.getStopPriority()));
		for (Block block : blocks) {
			this.blockingBlock.add(block);
			if (this.isInterrupted)
				Thread.currentThread().interrupt();
			DiagramScheduler.destroyBlock(block, block.isEnable());
			this.blockingBlock.remove(block);
		}
	}

	public boolean consumeDatas(IOComponent source, Object[] outputValues, long[] timeStamps) {
		stackOutputsAndTrigger(source, outputValues, timeStamps);
		while (!this.todoBlocks.isEmpty()) {
			if (isInterrupted())
				return true;
			Block block = this.todoBlocks.popFirst();
			if ((!this.dead || block.isConsumesAllDataBeforeDying()) && runBlock(block))
				stackOutputsAndTrigger(block);
		}
		return false;
	}

	private boolean runBlock(Block block) {
		// if (!block.isReady())
		// return false;
		Triple<Object[], long[], long[]> blockBuff = this.blockBuffs.get(block);
		Object[] inputs;
		long[] ts;
		long[] toi;
		if (blockBuff == null || blockBuff.getFirst().length != block.getNbInput()) {
			inputs = new Object[block.getNbInput()];
			ts = new long[block.getNbInput()];
			toi = new long[block.getNbInput()];
			this.blockBuffs.put(block, new Triple<>(inputs, ts, toi));
		} else {
			inputs = blockBuff.getFirst();
			ts = blockBuff.getSecond();
			toi = blockBuff.getThird();
		}
		List<BlockInput> blockInputs = block.getInputs();
		for (int i = 0; i < inputs.length; i++) {
			IOData ioData = blockInputs.get(i).pop();
			if (ioData != null) {
				inputs[i] = ioData.getValue();
				ts[i] = ioData.getTs();
				toi[i] = ioData.getToi();
			} else {
				inputs[i] = null;
				ts[i] = -1;
				toi[i] = -1;
			}
		}
		long oldCallingThreadId = this.callingThreadId;
		this.callingThreadId = Thread.currentThread().getId();
		boolean hasReturnValue = DiagramScheduler.processBlock(block, inputs, ts, toi);
		this.callingThreadId = oldCallingThreadId;
		return hasReturnValue;
	}

	private void stackOutputsAndTrigger(Block block) {
		stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs());
	}

	private void stackOutputsAndTrigger(IOComponent comp, Object[] outputs, long[] outputsTs) {
		if (!comp.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = comp.getIndex();
		if (comp instanceof FlowDiagram)
			System.out.println("sortie de diagram de flux");
		for (IOLinks ioLinks : index) {
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered()) {
				boolean needToProcess = false;
				for (IOLink ioLink : ioLinks.getLinks()) {
					int outputIndex = ioLink.getOutputIndex();
					Object outputObject = outputs[outputIndex];
					if (outputObject == null)
						continue;
					needToProcess = true;
					if (ioLink.getInput().getBuffer().push(outputsTs[outputIndex], timeOfIssue, ioLink.isNeedToCopy() ? DiagramScheduler.clone(outputObject) : outputObject))
						DiagramScheduler.showBufferOverflow(comp, ioLink.getInput());
					Link link = ioLink.getInput().getLink();
					if (link != null)
						link.consume();
				}
				if (needToProcess)
					this.todoBlocks.push((Block) linkedComp); // Plante si c'est un flowDiagram
			}
		}
	}

	public void triggerOutput(IOComponent source, Object[] outputValues, long[] timeStamps) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread)
			triggerOutputFromMono(source, outputValues, timeStamps);
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId)) {
			Thread oldMonoThread = this.monoRemoteThread;
			this.monoRemoteThread = Thread.currentThread();
			triggerOutputFromMono(source, outputValues, timeStamps);
			this.monoRemoteThread = oldMonoThread;
		} else {
			if (currentThread.isInterrupted()) {
				Thread.currentThread().interrupt();
				return;
			}
			if (this.lock.isHeldByCurrentThread())
				consumeDatas(source, outputValues, timeStamps);
			else
				dataSend: while (!this.dead) {
					this.lock.lock();
					try {
						while (!isInterrupted()) {
							if (this.dead)
								return;
							if (this.todoSource == null) {
								this.todoSource = source;
								this.todoOutputValues = outputValues;
								this.todoTimeStamps = timeStamps;
								this.incomingDataOrTaskSignals.signal();
								break dataSend;
							}
							this.schedulerAvailable.await();
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						return;
					} finally {
						this.lock.unlock();
					}
				}
		}
	}

	private void triggerOutputFromMono(IOComponent source, Object[] outputValues, long[] timeStamps) {
		stackOutputsAndTrigger(source, outputValues, timeStamps);
		while (!this.todoBlocks.isEmpty()) {
			Block block = this.todoBlocks.popFirst();
			if (runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	public void triggerTask(Object source, Runnable task) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread)
			task.run();
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId)) {
			Thread oldMonoThread = this.monoRemoteThread;
			this.monoRemoteThread = Thread.currentThread();
			task.run();
			this.monoRemoteThread = oldMonoThread;
		} else if (this.lock.isHeldByCurrentThread())
			task.run();
		else {
			this.lock.lock();
			try {
				Condition schedulerAvailable = this.lock.newCondition();
				Condition taskCompleted = this.lock.newCondition();
				this.tasks.add(new Tuple<>(schedulerAvailable, taskCompleted));
				this.incomingDataOrTaskSignals.signal();
				try {
					schedulerAvailable.await();
					task.run();
					taskCompleted.signal();
				} catch (InterruptedException e) {
					task.run();
				}
			} finally {
				this.lock.unlock();
			}
		}
	}
}

class BlockRingBuffer {
	private int maxSize;
	private int front;
	private int rear;
	private int bufLen;
	private Block[] blocks;

	public BlockRingBuffer() {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.maxSize = 20;
		this.blocks = new Block[this.maxSize];
	}

	public BlockRingBuffer(int maxStackSize, BlockRingBuffer buffer) {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.maxSize = maxStackSize;
		this.blocks = new Block[this.maxSize];
		if (buffer != null) {
			Block block;
			while ((block = buffer.popFirst()) != null)
				push(block);
		}
	}

	public boolean isEmpty() {
		return this.bufLen == 0;
	}

	public Block popFirst() {
		if (this.bufLen == 0)
			return null;
		Block block = this.blocks[this.front];
		this.blocks[this.front] = null;
		this.front += 1;
		if (this.front == this.maxSize)
			this.front = 0;
		this.bufLen--;
		return block;
	}

	public void push(Block block) {
		if (this.bufLen == this.maxSize) {
			BlockRingBuffer newBlockRingBuffer = new BlockRingBuffer(this.maxSize * 2, this);
			this.maxSize = newBlockRingBuffer.maxSize;
			this.front = newBlockRingBuffer.front;
			this.rear = newBlockRingBuffer.rear;
			this.bufLen = newBlockRingBuffer.bufLen;
			this.blocks = newBlockRingBuffer.blocks;
		}
		this.bufLen++;
		this.blocks[this.rear] = block;
		this.rear += 1;
		if (this.rear == this.maxSize)
			this.rear = 0;
	}
}

// Record
class OnConsumeTask {
	public final Object source;
	public final Runnable runnable;

	public OnConsumeTask(Object source, Runnable runnable) {
		this.source = source;
		this.runnable = runnable;
	}
}