/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.util.HashMap;

public class DBCSignal {
	public static final int UNSIGNED = 0;
	public static final int SIGNED = 1;
	public static final int IEEEFLOAT = 2;
	public static final int IEEEDOUBLE = 3;

	public final String name;
	public final int messageSize;
	public final boolean isIntelType;
	public int format; // + ou -
	public final byte firstBit;
	public final byte msgSize;
	public final double scale;
	public final double offset;
	public final double min;
	public final double max;
	public final String unit;
	public final String driver;
	private HashMap<Integer, String> integerToenum;
	private HashMap<String, Integer> enumToInteger;

	public DBCSignal(String name, int messageSize, boolean isIntelType, int format, byte firstBit, byte msgSize, double scale, double offset, double min, double max, String unit, String driver) {
		if (format == IEEEFLOAT && msgSize != 32)
			throw new IllegalArgumentException("Float format with msgSize of " + msgSize + " is not possible, Float is 32 bits");
		if (format == IEEEDOUBLE && msgSize != 64)
			throw new IllegalArgumentException("Double format with msgSize of " + msgSize + " is not possible, Double is 64 bits");
		// if(name.contains("VOLANT"))
		this.name = name;
		this.messageSize = messageSize;
		this.isIntelType = isIntelType;
		this.format = format;
		this.firstBit = isIntelType ? firstBit : (byte) (firstBit / 8 * 8 + 7 - firstBit % 8);
		this.msgSize = msgSize;
		this.scale = scale;
		this.offset = offset;
		this.min = min;
		this.max = max;
		this.unit = unit;
		this.driver = driver;
	}

	public double decode(byte[] data) {
		// System.out.println(trame);
		long bitValue = 0;
		// boolean useOpt = false;
		// if (msgSize > Integer.BYTES * 8 + (format == 0 ? 1 : 0)) {
		// if (useOpt && firstBit % 8 == 0 && msgSize % 8 == 0) {
		// int begin = firstBit / 8;
		// int end = begin + msgSize / 8;
		// int byteid = 0;
		// if (format == 1)
		// begin++;
		// int i = end;
		// for (; i-- != begin;)
		// bitValue += (data[i] & 0xFF) << 8 * byteid++;
		// if (format == 1) {
		// int lastByte = data[i / 8] & ~(1 << 7);
		// bitValue += (lastByte & 0xFF) << 8 * byteid++;
		// if ((data[i / 8] >> 7 & 1) == 1)
		// bitValue = -bitValue;
		// begin++;
		// }
		// } else {
		// byte begin = isIntelType ? (byte) (data.length * 8 - (firstBit + msgSize)) : firstBit;
		// int lastBit = begin + msgSize;
		// if (format == 1)
		// begin++;
		// int i = lastBit;
		// int index = 0;
		// for (; i-- != begin;) {
		// // System.out.println("byte: " + i/8 + " bit: " + (((lastBit - 1) - i)%8) + " val: " + (((data[i / 8] >> (((lastBit - 1) - i) % 8)) & 1) == 1));
		// if ((data[i / 8] >> (lastBit - 1 - i) % 8 & 1) == 1)
		// bitValue += 1 << index;
		// index++;
		// }
		// if (format == 1 && isBitR(data[i / 8], 7 - i % 8))
		// bitValue = -((2 << (msgSize - 2)) - bitValue);
		// }
		// } else if (useOpt && firstBit % 8 == 0 && msgSize % 8 == 0) {
		// int begin = firstBit / 8;
		// int end = begin + msgSize / 8;
		// int byteid = 0;
		// if (format == 1)
		// begin++;
		// int i = end;
		// for (; i-- != begin;)
		// bitValue += (data[i] & 0xFF) << 8 * byteid++;
		// if (format == 1) {
		// int lastByte = data[i / 8] & ~(1 << 7);
		// bitValue += (lastByte & 0xFF) << 8 * byteid++;
		// if ((data[i / 8] >> 7 & 1) == 1)
		// bitValue = -bitValue;
		// begin++;
		// }
		// } else { // Check ok
		// if (true) {

		if (this.isIntelType) {
			byte begin = (byte) (data.length * 8 - (this.firstBit + this.msgSize));
			int lastBit = begin + this.msgSize;
			if (this.format == SIGNED) // Je décale pour ne pas lire le bit de signe
				begin++;
			int i = lastBit;
			int index = 0;
			int messageSizeM1 = this.messageSize - 1;
			for (; i-- != begin;) {
				int numBit = 7 - i % 8;
				// System.out.println("byte: " + i / 8 + " bit: " + numBit + " val: " + (isBitR(data[i / 8], numBit) ? 1 : 0));//(((data[i / 8] >> ((i) % 8)) & 1) == 1));
				if (isBitR(data[messageSizeM1 - i / 8], numBit))
					bitValue += 1L << index;
				index++;
			}
			if (this.format == SIGNED && isBitR(data[messageSizeM1 - i / 8], 7 - i % 8))
				bitValue = -((1L << this.msgSize - 1) - bitValue);
		} else {
			byte begin = this.firstBit;
			int lastBit = begin + this.msgSize;
			if (this.format == SIGNED) // Je décale pour ne pas lire le bit de signe
				begin++;
			int i = lastBit;
			int index = 0;
			for (; i-- != begin;) {
				int numBit = 7 - i % 8;
				// System.out.println("byte: " + i / 8 + " bit: " + numBit + " val: " + (isBitR(data[i / 8], numBit) ? 1 : 0));//(((data[i / 8] >> ((i) % 8)) & 1) == 1));
				if (isBitR(data[i / 8], numBit))
					bitValue += 1L << index;
				index++;
			}
			if (this.format == SIGNED && isBitR(data[i / 8], 7 - i % 8))
				bitValue = -((1L << this.msgSize - 1) - bitValue);
		}

		double value = this.format == IEEEDOUBLE ? Double.longBitsToDouble(bitValue) : this.format == IEEEFLOAT ? Float.intBitsToFloat((int) bitValue) : this.offset + bitValue * this.scale; // min
																																																// avant
																																																// mais
																																																// offset
		// maintenant
		if (value > this.max)
			value = this.max;
		else if (value < this.min)
			value = this.min;
		return value;
	}

	public String decodeAsString(byte[] data) {
		return this.integerToenum != null ? this.integerToenum.get((int) (decode(data) + 0.5)) : null;
	}

	// encode marche pas en intel je pense...
	public void encode(byte[] data, double value) {
		if (value > maxEffectiveValue())
			throw new IllegalArgumentException("Cannot encode: " + value + " the effective maximum value is: " + maxEffectiveValue());
		if (value > this.max)
			throw new IllegalArgumentException("Cannot encode: " + value + " the maximum value is: " + this.max);
		else if (value < this.min)
			throw new IllegalArgumentException("Cannot encode: " + value + " the minimum value is: " + this.max);
		long bitValue = this.format == IEEEDOUBLE ? Double.doubleToLongBits(value) : this.format == IEEEFLOAT ? Float.floatToIntBits((float) value) : Math.round((value - this.offset) / this.scale);
		byte begin = this.isIntelType ? (byte) (data.length * 8 - (this.firstBit + this.msgSize)) : this.firstBit;
		int lastBit = begin + this.msgSize;
		if (this.format == 1)
			begin++;
		int i = lastBit;
		int index = 0;
		if (this.isIntelType) {
			boolean comp = false;
			if (this.format == SIGNED && bitValue < 0) {
				bitValue = (1L << this.msgSize - 1) + bitValue; // pk 15?
				comp = true;
			}
			int messageSizeM1 = this.messageSize - 1;
			int byteIndex;
			for (; i-- != begin;) {
				byteIndex = messageSizeM1 - i / 8;
				int numBit = 7 - i % 8;
				if ((bitValue & 1L << index) != 0)
					data[byteIndex] = setBitR(data[byteIndex], numBit);
				else
					data[byteIndex] = unsetBitR(data[byteIndex], numBit);
				index++;
			}
			if (comp) {
				byteIndex = messageSizeM1 - i / 8;
				int numBit = 7 - i % 8;
				// System.out.println("byte: " + i / 8 + " bit: " + numBit + " val: " + (isBitR(data[i / 8], numBit) ? 1 : 0));//(((data[i / 8] >> ((i) % 8)) & 1) == 1));
				data[byteIndex] = setBitR(data[byteIndex], numBit);
			}

		} else {
			boolean comp = false;
			if (this.format == SIGNED && bitValue < 0) {
				bitValue = (1L << this.msgSize - 1) + bitValue;
				comp = true;
			}
			int byteIndex;
			for (; i-- != begin;) {
				byteIndex = i / 8;
				int numBit = 7 - i % 8;
				// System.out.println("byte: " + byteIndex + " bit: " + numBit);
				if ((bitValue & 1L << index) != 0)
					data[byteIndex] = setBitR(data[byteIndex], numBit);
				else
					data[byteIndex] = unsetBitR(data[byteIndex], numBit);
				index++;
			}
			if (comp) {
				byteIndex = i / 8;
				int numBit = 7 - i % 8;
				// System.out.println("byte: " + i / 8 + " bit: " + numBit + " val: " + (isBitR(data[i / 8], numBit) ? 1 : 0));//(((data[i / 8] >> ((i) % 8)) & 1) == 1));
				data[byteIndex] = setBitR(data[byteIndex], numBit);
			}
		}
	}

	public double maxEffectiveValue() {
		return this.offset + ((1L << (this.format == SIGNED ? this.msgSize - 1 : this.msgSize)) - 1) * this.scale;
	}

	public void encodeAsString(byte[] data, String value) {
		encode(data, this.enumToInteger.get(value));
	}

	public Class<?> getDataType() {
		return this.integerToenum != null ? String.class : Double.class;
	}

	public HashMap<Integer, String> getEnumMap() {
		return this.integerToenum;
	}

	public boolean isEnum() {
		return this.integerToenum != null;
	}

	public void setEnumMap(HashMap<Integer, String> enumMap) {
		this.integerToenum = enumMap;
		this.enumToInteger = new HashMap<>();
		this.integerToenum.forEach((value, enumName) -> this.enumToInteger.put(enumName, value));
	}

	@Override
	public String toString() {
		return "name: " + this.name + " messageSize: " + this.messageSize + " firstBit: " + this.firstBit + " msgSize: " + this.msgSize + " isIntelType: " + this.isIntelType + " format: "
				+ this.format + " scale: " + this.scale + " offset: " + this.offset + " min: " + this.min + " max: " + this.max + " unit: " + this.unit;
	}

	public static boolean isBit(byte b, int i) {
		return (b & 1 << 7 - i) != 0;
	}

	private static boolean isBitR(byte data, int bitPos) {
		return (data & 1 << bitPos) != 0;
	}

	public static void main(String[] args) {
		showBytes(new byte[] { (byte) 0xA3, (byte) 0x49, (byte) 0x81 });
	}

	private static byte setBitR(byte data, int bitPos) {
		return (byte) (data | 1 << bitPos);
	}

	public static void showBytes(byte[] bs) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bs.length; i++) {
			String index = Integer.toString(i);
			int nbSpace = 9 - index.length();
			int half = nbSpace / 2;
			for (int j = 0; j < half; j++)
				sb.append("_");
			sb.append(index);
			half = nbSpace - half;
			for (int j = 0; j < half; j++)
				sb.append("_");
			sb.append("  ");
		}
		System.out.println(sb.toString());
		System.out.println(toHexString(bs));
		System.out.println(toBinaryString(bs));
	}

	public static void showBytesCompact(byte[] bs) {
		System.out.println(toHexStringCompact(bs));
	}

	public static String toBinaryString(byte[] datas) {
		StringBuilder sb = new StringBuilder();
		for (byte b : datas) {
			toBinaryString(sb, b);
			sb.append("  ");
		}
		return sb.toString();
	}

	public static String toBinaryString(byte b) {
		StringBuilder sb = new StringBuilder();
		toBinaryString(sb, b);
		return sb.toString();
	}

	public static void toBinaryString(StringBuilder sb, byte b) {
		// for (int i = 0; i < 8; i++) {
		// if (i == 4)
		// sb.append(" ");
		// sb.append(isBit(b, i) ? 1 : 0);
		// }
		sb.append(new StringBuilder(Integer.toBinaryString(b & 255 | 256).substring(1)).insert(4, " ").toString());
	}

	public static String toHexString(byte[] datas) {
		StringBuilder sb = new StringBuilder();
		// sb.append(" ");
		for (byte b : datas) {
			toHexString(sb, b);
			sb.append("  ");
		}
		return sb.toString();
	}

	public static String toHexString(byte b) {
		StringBuilder sb = new StringBuilder();
		toHexString(sb, b);
		return sb.toString();
	}

	public static void toHexString(StringBuilder sb, byte b) {
		String hex = String.format("%02X ", b);
		sb.append("   " + hex.charAt(0) + "    " + hex.charAt(1));
	}

	public static String toHexStringCompact(byte[] datas) {
		StringBuilder sb = new StringBuilder();
		// sb.append(" ");
		for (byte b : datas) {
			toHexStringCompact(sb, b);
			sb.append(" ");
		}
		return sb.toString();
	}

	public static void toHexStringCompact(StringBuilder sb, byte b) {
		String hex = String.format("%02X ", b);
		sb.append(hex.charAt(0) + "" + hex.charAt(1));
	}

	private static byte unsetBitR(byte data, int bitPos) {
		return (byte) (data & ~(1 << bitPos));
	}
}
