File:Fichier
New Scenario:Nouveau scénario
Open Scenario:Ouvrir
Open Local Scenario:Ouvrir un Scénario local
Open Network Scenario:Ouvrir un flux réseau
Recent Scenario:Scénarios récents
Save Scenario:Sauvegarder scénario
Save Scenario as:Sauvegarder scénario Sous
Scenario Information:Information sur le scénario
Properties:Propriétés
Loader Properties:Propriétés du chargeur
Scenario Properties:Propriétés du scénario
Import Raster:Importer raster
Import Station:Importer station
Import Configuration:Importer configuration
Export Raster:Exporter raster
Export Station:Exporter station
Export Configuration:Exporter configuration
Export Raster To BMP:Exporter raster vers BMP
Options:Options
Exit:Fermer
Edit:Edition
Change direction path:Changer direction du chemin
Split segment Path:Découper le segment en deux
Remove Path:Supprimer le chemin
Simulation:Simulation
GRNN Simulation:Simulation GRNN
Meteo Simulation:Simulation Météo
Graph Simulation:Simulation Graphe
Skier Simulation:Simulation Skieur
Juste Neige Simulation:Simulation Juste Neige
View:Affichage
Piste Panel:Liste des pistes
TheaterFilter:Filtre du théâtre
TheaterEditor:Editeur du théâtre
StatusBar:Barre de status
Player:Lecteur
PlayList:Liste de lecture
Values:Valeurs
DrawerProperties:Propriétés de l'affichage
Generator:Générateur
RoadSurfaceMarkingGenerator:Générateur de marquage routier
RecGenerator:Générateur de rec
Threshold:Seuil
Console:Console
Operators:Operateurs
Simulated Day:Jours simulés
Language:Langage
Italian:Italien
German:Allemand
French:Français
English:Anglais
New Playlist:Nouvelle liste de lecture
Open Playlist:Ouvrir liste de lecture
Add Files:Ajouter fichier(s)
Add Folder:Ajouter dossier
Save Playlist:Sauvegarder liste de lecture
File info:Information sur le fichier
Playlist:Liste de lecture
Select all:Sélectionner tout
Select none:Effacer sélection
Invert selection:Inverser sélection
Remove selected:Supprimer sélection
Clear playlist:Effacer la liste de lecture
Sort:Tier
Remove missing files:Supprimer les éléments manquants
Sort by name:Trier par nom
Sort by path:Trier par chemin
Reverse list:Renverser la liste
Randomize list:Randomiser la liste
