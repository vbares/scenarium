/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.util.concurrent.Semaphore;

import javax.vecmath.Point2i;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.tools.FxUtils;
import org.scenarium.Scenarium;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

@BlockInfo(info = "")
@BeanPropertiesInheritanceLimit
public abstract class AbstractViewer extends EvolvedOperator implements VisuableSchedulable, BeanRenameListener {
	private static BufferedImage buffImage;
	private static WritableImage activeImage; // Image use in diagram when active
	private static WritableImage inactiveImage;
	public Stage stage;
	@PropertyInfo(index = 0, nullable = false, info = "Position of the viewer on the screen")
	protected Point2i position = new Point2i();
	@PropertyInfo(index = 1, nullable = false, info = "Dimension of the viewer on the screen")
	protected Point2i dimension = new Point2i();
	@PropertyInfo(index = 2, info = "Defines whether this windows is kept on top of other windows")
	protected boolean alwaysOnTop = false;
	@PropertyInfo(index = 3, info = "Add an output of the view image")
	private boolean generateOutputImage = false;

	@PropertyInfo(expert = true)
	protected boolean active = true;
	protected boolean propertyChanged;
	protected boolean animated;
	private Object[] oldObjsToDraw;
	private BufferedImage outputImage;
	private long oldDataRepaintTime = Long.MIN_VALUE;
	private long maxTimeStampsToDraw;
	private Object[] objsToDraw;
	protected boolean isDefaulting = false;
	private boolean generateOutputForThisIteration;
	private Semaphore generateOutputImageLock;
	private volatile boolean needToRepaint;
	private volatile boolean dead = false;
	private boolean closing = false;
	private ImageView iv;
	private StackPane sp;

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		Stage stage = this.stage;
		if (stage != null)
			FxUtils.runLaterIfNeeded(() -> stage.setTitle(beanDesc.name));
	}

	@Override
	public void birth() {
		Scenarium.waitForJavaFXThreadStart();
		if (this.active)
			this.dead = false;
		addBlockNameChangeListener(this);
	}

	protected void closeViewer() {}

	private static void createBufferedImage(int width, int height) {
		if (buffImage == null || buffImage.getWidth() != width || buffImage.getHeight() != height) {
			buffImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g = buffImage.createGraphics(); // TODO java 11 bug
			int roundSize = width / 10;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(new Color(48, 29, 7, 80));
			g.setPaint(new GradientPaint(0, 0, new Color(48, 29, 7, 80), width, 0, new Color(48, 29, 7, 0)));
			g.drawLine(width / 2, height / 2, width / 10, height / 10);
			g.drawLine(width / 2, height / 2, width - width / 10, height / 10);
			g.setComposite(AlphaComposite.Src);
			g.fillOval(width / 10 - height / 40, height / 20 + height / 40, height / 20, height / 20);
			g.fillOval(width - width / 10 - height / 40, height / 20 + height / 40, height / 20, height / 20);
			float gap = 0.9f;
			float gapy = 0.7f;
			int minX = (int) ((1 - gap) * width);
			int maxX = (int) (gap * width);
			int minY = (int) ((1 - gapy) * height);
			int maxY = (int) (gapy * height);
			g.setStroke(new BasicStroke(roundSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.drawRect(minX, minY, maxX - minX, maxY - minY);
			g.fillRect(minX, minY, maxX - minX, maxY - minY);
			g.setStroke(new BasicStroke(roundSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.setColor(new Color(112, 200, 216, 80));
			gap = 0.8f;
			gapy = 0.62f;
			minX = (int) ((1 - gap) * width);
			maxX = (int) (gap * width);
			minY = (int) ((1 - gapy) * height);
			maxY = (int) (gapy * height);
			g.drawRoundRect(minX, minY, maxX - minX, maxY - minY, roundSize, roundSize);
			g.fillRect(minX, minY, maxX - minX, maxY - minY);
		}
	}

	@Override
	public void death() {
		this.dead = true;
		this.objsToDraw = null;
		this.oldObjsToDraw = null;
		Stage stage = this.stage;
		this.stage = null;
		this.generateOutputImageLock = null;
		removeBlockNameChangeListener(this);
		if (stage != null)
			FxUtils.runLaterIfNeeded(() -> {
				this.closing = true;
				stage.close();
				this.closing = false;
				closeViewer();
			});
		this.oldDataRepaintTime = Long.MIN_VALUE;
	}

	public Point2i getDimension() {
		return this.dimension;
	}

	@Override
	public Region getNode() {
		this.iv = new ImageView();
		this.sp = new StackPane(this.iv);
		this.sp.setPickOnBounds(false);
		this.iv.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				setActive(!isActive());
				this.propertyChanged = true;
			}
			e.consume();
		});
		this.iv.setOnMouseClicked(e -> e.consume());
		this.iv.setOnMouseReleased(e -> e.consume());
		InvalidationListener il = e -> updateImage(this.sp, this.iv);
		this.sp.heightProperty().addListener(il);
		this.sp.widthProperty().addListener(il);
		return this.sp;
	}

	public Point2i getPosition() {
		return this.position;
	}

	protected Stage initStage() {
		Stage stage = this.stage;
		if (stage == null && isActive() && !this.dead) {
			stage = new Stage();
			if (this.position != null && FxUtils.isLocationInScreenBounds(new Point2D(Math.max(this.position.x, 0), Math.max(this.position.y, 0)))) {
				stage.setX(this.position.getX());
				stage.setY(this.position.getY());
			}
			if (this.dimension != null) {
				stage.setWidth(this.dimension.x);
				stage.setHeight(this.dimension.y);
			}
			stage.setAlwaysOnTop(isAlwaysOnTop());
			stage.alwaysOnTopProperty().addListener(e -> {
				Stage s = this.stage;
				if (s != null && s.isAlwaysOnTop() != this.alwaysOnTop) {
					this.alwaysOnTop = s.isAlwaysOnTop();
					this.propertyChanged = true;
				}
			});
			stage.xProperty().addListener(e -> {
				Stage s = this.stage;
				if (s != null && s.getX() != this.position.getX()) {
					this.position.setX((int) s.getX());
					this.propertyChanged = true;
				}
			});
			stage.yProperty().addListener(e -> {
				Stage s = this.stage;
				if (s != null && s.getY() != this.position.getY()) {
					this.position.setY((int) s.getY());
					this.propertyChanged = true;
				}
			});
			stage.widthProperty().addListener(e -> {
				Stage s = this.stage;
				if (s != null && s.getWidth() != this.dimension.getX()) {
					this.dimension.setX((int) s.getWidth());
					this.propertyChanged = true;
					// autoSize = false;
				}
			});
			stage.heightProperty().addListener(e -> {
				Stage s = this.stage;
				if (s != null && s.getHeight() != this.dimension.getY()) {
					this.dimension.setY((int) s.getHeight());
					this.propertyChanged = true;
					// autoSize = false;
				}
			});
			stage.setOnHidden(e -> {
				if (!this.closing)
					setActive(false);
			});
			this.stage = stage;
		}
		return stage;
	}

	@Override
	public void initStruct() {
		updateOutputs(new String[] {}, new Class<?>[] {});
	}

	public boolean isActive() {
		return this.active;
	}

	public boolean isAlwaysOnTop() {
		return this.alwaysOnTop;
	}

	public boolean isGenerateOutputImage() {
		return this.generateOutputImage;
	}

	@Override
	public boolean needToBeSaved() {
		if (this.propertyChanged) {
			this.propertyChanged = false;
			return true;
		}
		return false;
	}

	protected boolean needToForceRefresh() {
		return false;
	}

	protected boolean needToRefreshForNewAdditionalInputs() {
		return false;
	}

	@Override
	public void paint() {
		if (this.dead)
			return;
		if (needToForceRefresh())
			this.needToRepaint = true;
		if (this.objsToDraw == null || !this.needToRepaint && !this.generateOutputForThisIteration)
			return;
		Object[] objs = this.objsToDraw;
		paintDatas(objs);
		// if (dead) { // Si je suis mort entre temps je retue PK faire??? pas grave si je continue, je vais juste créer une image....
		// System.out.println("enter paint ok but dead");
		// death();
		// } else {
		this.needToRepaint = false; // Ne pas le mettre à la fin, sinon
		if (this.generateOutputImage)
			this.outputImage = SwingFXUtils.fromFXImage(this.stage.getScene().getRoot().snapshot(new SnapshotParameters(), null), null);
		if (this.generateOutputForThisIteration) {
			this.generateOutputForThisIteration = false;
			Semaphore generateOutputImageLock = this.generateOutputImageLock; // Le lock peut disparaitre si death entre temps, implique que déja mort donc pas grave
			if (generateOutputImageLock != null)
				generateOutputImageLock.release();
		}
		// }
	}

	protected abstract void paintDatas(Object[] objs);

	@ParamInfo(in = "in")
	public void process(final Object... objs) {
		if (!this.active)
			return;
		long time = System.currentTimeMillis();
		if (this.isDefaulting)
			return;
		Object[] tempObjs;
		long maxTimeStamps = 0;
		if (isExclusiveObjectInput())
			tempObjs = objs;
		else {
			tempObjs = new Object[objs.length];
			for (int i = 0; i < tempObjs.length; i++)
				if (objs[i] != null) {
					tempObjs[i] = DiagramScheduler.clone(objs[i]);
					long ts = getTimeStamp(i);
					if (ts > maxTimeStamps)
						maxTimeStamps = ts;
				}
		}
		if (this.oldObjsToDraw == null || this.oldObjsToDraw.length != objs.length)
			this.oldObjsToDraw = new Object[objs.length];
		for (int i = 0; i < tempObjs.length; i++)
			if (tempObjs[i] != null)
				this.oldObjsToDraw[i] = tempObjs[i];
		if (tempObjs.length != 0 && (tempObjs[0] != null || needToRefreshForNewAdditionalInputs())) {
			this.objsToDraw = this.oldObjsToDraw.clone();
			if (this.objsToDraw[0] != null) {
				this.maxTimeStampsToDraw = maxTimeStamps;
				if (this.generateOutputImage) {
					if (this.generateOutputImageLock == null)
						this.generateOutputImageLock = new Semaphore(0);
					try {
						this.generateOutputForThisIteration = true;
						if (!this.animated && (time - this.oldDataRepaintTime) / 1000.0 > 1 / 30.0) {
							Platform.runLater(() -> paint());
							this.oldDataRepaintTime = time;
						} else if (!this.dead)
							this.needToRepaint = true;
						this.generateOutputImageLock.acquire();
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						return;
					}
					Object[] outputs = generateOuputsVector();
					outputs[getOutputIndex("Img")] = this.outputImage;
					triggerOutput(outputs, this.maxTimeStampsToDraw);
				} else if (!this.animated && (time - this.oldDataRepaintTime) / 1000.0 > 1 / 60.0) {
					Platform.runLater(() -> paint());
					this.oldDataRepaintTime = time;
				} else if (!this.dead)
					this.needToRepaint = true;
			}
		}
	}

	// public void setActive(boolean active) {
	// if (isRunning()) {
	// runLater(() -> {
	// this.active = active;
	// if (!active)
	// death();
	// else if(isRunning())
	// birth();
	// });
	// } else
	// this.active = active;
	// FxUtils.runLaterIfNeeded(() -> updateImage(sp, iv));
	// }

	public void setActive(boolean active) {
		Runnable task = () -> runLater(() -> {
			if (isRunning()) {
				this.active = active;
				if (!active)
					death();
				else if (isRunning())
					birth();
			} else
				this.active = active;
			FxUtils.runLaterIfNeeded(() -> updateImage(this.sp, this.iv));
		});
		if (Platform.isFxApplicationThread())
			new Thread(task).start();
		else
			task.run();
	}

	public void setAlwaysOnTop(boolean alwaysOnTop) {
		this.alwaysOnTop = alwaysOnTop;
		Stage stage = this.stage;
		if (stage != null)
			FxUtils.runLaterIfNeeded(() -> stage.setAlwaysOnTop(alwaysOnTop));
	}

	@Override
	public void setAnimated(boolean animated) {
		if (this.generateOutputImageLock != null)
			this.generateOutputImageLock.release();
		this.animated = animated;
	}

	public void setDimension(Point2i dimension) {
		this.dimension = dimension;
		Stage stage = this.stage;
		if (stage != null)
			FxUtils.runLaterIfNeeded(() -> {
				stage.setWidth(dimension.getX());
				stage.setHeight(dimension.getY());
			});
	}

	public void setGenerateOutputImage(boolean generateOutputImage) {
		this.generateOutputImage = generateOutputImage;
		try {
			initStruct();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPosition(Point2i position) {
		this.position = position;
		Stage stage = this.stage;
		if (stage != null)
			FxUtils.runLaterIfNeeded(() -> {
				stage.setX(position.getX());
				stage.setY(position.getY());
			});
	}

	private void updateImage(StackPane sp, ImageView iv) {
		if (sp == null || iv == null)
			return;
		int width = (int) (sp.getPrefWidth() * 0.7);
		int height = (int) Math.min(sp.getPrefHeight() * 0.9, width * 1.5);
		iv.setFitHeight(height);
		iv.setFitWidth(width);
		if (width == 0 || height == 0)
			iv.setImage(null);
		else
			synchronized (this) {
				if (this.active) {
					if (activeImage == null || activeImage.getWidth() != width || activeImage.getHeight() != height) {
						createBufferedImage(width, height);
						activeImage = SwingFXUtils.toFXImage(buffImage, null);
					}
					iv.setImage(activeImage);
				} else {
					if (inactiveImage == null || inactiveImage.getWidth() != width || inactiveImage.getHeight() != height) {
						createBufferedImage(width, height);
						BufferedImage grayImage = new BufferedImage(buffImage.getWidth(), buffImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
						new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null).filter(buffImage, grayImage);
						inactiveImage = SwingFXUtils.toFXImage(grayImage, null);
					}
					iv.setImage(inactiveImage);
				}
			}
	}

	@Override
	public boolean updateOutputs(String[] names, Class<?>[] types) {
		if (this.generateOutputImage) {
			int size = names.length + 1;
			String[] outNames = new String[size];
			Class<?>[] outTypes = new Class<?>[size];
			outNames[0] = "Img";
			outTypes[0] = BufferedImage.class;
			for (int i = 1; i < outNames.length; i++) {
				outNames[i] = names[i - 1];
				outTypes[i] = types[i - 1];
			}
			names = outNames;
			types = outTypes;
		}
		return super.updateOutputs(names, types);
	}
}
