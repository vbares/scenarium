Create your fist opérator
=========================

Based of the tutorial: [tutorial 05](tutorial_05_create_a_library_to_create_your_operators.md)


Scenarium is able to handle two types of operators: simple and advanced.
In any case, the classes implementing an operators NUST respect the formalism of the JavaBeans (except Serialization).

Please respect the tree in the pluging to facilitate the redability
```
SampleLib (package)
	- operator (packages)
```

Set the operator in pubic, in the file ```module-info.java```
```{.java}

module SampleLib {
	
	...
	
	exports SampleLib.operator;
}
```


Simple operator
===============

Create a new class the operator package named ```AddReverseLast```


Basic Methode
-------------

```{.java}
package SampleLib.operator;

public class AddReverseLast {
	/**
	 * @brief Call One time at the start of the scenario (or when added when running)
	 * @note No parameters
	 */
	public void birth() {}
	/**
	 * @brief Call One time at the end of the scenario (or when added when running)
	 * @note No parameters
	 */
	public void death() {}
	/**
	 * @brief Call by the scenarium scheduler when a new data is availlable.
	 */
	public void process() {}
}
```

_**Note:**_ The curent operator have no input streams, no output streams and no Parameters.

Add Inputs:
-----------

The operator input is represented by adding parameters in the argument of the ```process()``` function.

The argument availlable is defined by Scenarium or by the user, BUT it must be an Object not a basic type like ```int``` or ```double```.
Use wrapped type: ```Integer``` or ```Double```.
The reason is simple: The input of the process fucntion can be ```null``` if no stream is provided.

For NO argument:
```{.java}
public void process() {
	// Do some stuff
}
```


For one argument:
```{.java}
public void process(Integer number) {
	// Do some stuff
}
```

For multiple arguments:
```{.java}
public void process(Integer number, Vegetable ingredient) {
	if (number != null) {
		// Do some stuff
		return;
	}
	if (ingredient != null) {
		// Do some stuff
		return;
	}
}
```

_**Note:**_ The Vegetable must have a special declaration (see after).

For variadic arguments:
```{.java}
public void process(Vegetable... ingredient) {
	// Do some stuff
}
```

With this modification, you can add multiple input for ingredient (the system add a new one for every new connection.

_**Note:**_ The Process fucntion is called when ONE on the input has data. It is the charge of the User do write data synchronizer.


Define a New class to be used as Input/output arguments:
-------------------------------------------------------

Use a User defined Class as Input output must force it to be cloneable:

```{.java}
public class Vegetable implements Cloneable {
	private final String variety;
	public Vegetable(String variety) {
		this.variete = variety;
	}
	public String getVariety(){
		return this.variety;
	}
	public void setVariety(String _variety){
		this.variete = _variety;
	}
	@Override
	public Object clone() {
		return new Legume(this.variety);
	}
}
```

You need now to add it in the scenarium engine:
In the file ```SamplePlugin.java```
```{.java}
	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(Vegetable.class, Vegetable::clone);
	}
```

Simple synchroniqation input:
-----------------------------

The data on the input can arrive one by one, or if the data is set before it can receive multiple value at the same call, it depent of the moment of the block
was scheduled in the process, if you are in mono-thread or in multiple thread or in multiple process.

Then we need to coice how to synchronise the input.

```{.java}
public class Multiply {
	private Integer m_number1 = null;
	private Integer m_number2 = null;
	
	...
	
	public void process(Integer _number1, Integer _number_2) {
		if (_number1 != null) {
			m_number1 = _number1;
		}
		if (_number2 != null) {
			m_number2 = _mumber2;
		}
		if (    m_number1 != null
		     && m_number2 != null) {
			Integer result = m_number1 * m_number2;
			System.out.println(m_number1 + " * " + m_number2 + " = " + result);
			m_number_1 = null;
			m_number_2 = null;
			// TODO: send result in output...
		}
	}
	
	...
	
}
```

Create output stream.
---------------------

For generic type simply declare a new getter with te pattern ```public Tttttt getOutputXxxxxxYyyyy()```

For example:
```{.java}
public class MyOperator {
    private Soupe m_outputData;
    
    public void birth() {}
    public void process(...) {}
    public void death() {}
    
    public Soupe getOutputData() {
        return m_outputData;
    }
}
```

_**Note:**_ an other way to return a value of an operator in returning the value in the process function, but this methode is deprecated.


Add Properties:
---------------

you have 2 group of properties: boolean and other.

For other, declare function ```public TYPE getXxxxxxYyyyyy()``` and ```public void setXxxxxxYyyyyy(TYPE _value)```:

```{.java}
public class MyOperator {
    public String m_value = "plouf"; //!< My internal property ...
    /**
     * @brief Set the property ...
     * @param[in] _value New value of the porperty.
     */
    public void setValue(String _value){
        m_value = _value;
    }
    /**
     * @brief Get the property ...
     * @return The current value of the porperty.
     */
    public String getValue(){
        return m_value;
    }
}
```


For boolean, declare function ```public boolean isXxxxxxYyyyyy()``` and ```public void setXxxxxxYyyyyy(boolean _value)```:

```{.java}
public class MyOperator {
    public boolean m_value = false; //!< My internal property ...
    /**
     * @brief Set the property ...
     * @param[in] _value New value of the porperty.
     */
    public void setValue(bool _value){
        m_value = _value;
    }
    /**
     * @brief Get the property ...
     * @return The current value of the porperty.
     */
    public bool isValue(){
        return m_value;
    }
}
```

Les property can be dynamique, we must understand that the ```void process(...)``` function is not called.



