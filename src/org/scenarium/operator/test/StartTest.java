/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class StartTest extends EvolvedOperator {

	@Override
	public void birth() throws Exception {
		onStart(() -> {
			triggerOutput(5);
		});
	}

	public Integer process() {
		return null;
	}

	@Override
	public void death() throws Exception {
		// TODO Auto-generated method stub

	}

}
