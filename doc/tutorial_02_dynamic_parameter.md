Tutorial 2: Parameters as imput
===============================

This tutorial is based on the [Tutorial 1](tutorial_01_first_run.md)

An Operator parameter represent a configuration of the operator algorithms and not an input of the algorithms.
But it could be interesting to dynamically configure the parameter an a generic input of the Operator.

To do this:
  - Open the **perlin noise** filter parameter (double click on the box)
  - Click right on the parameter name, you will see a contextual menu proposing to use this parameter an an input.
  - Click left to activate this input

![select parameter as input](tutorial_02/select_parameter_as_input.png)

Now you will have a new input on your operator:

![block with parameter input](tutorial_02/block_with_parameter_input.png)

You need now to add a new data in this imput,
but you need to know the type of the input. For this, stay the mouse hover the input round dot.
You will see information about the input property:

![property input](tutorial_02/property_input.png)

Add a data generator and enjoy...

![full dyagram](tutorial_02/full_dyagram.png)



