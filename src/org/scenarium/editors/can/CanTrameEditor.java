/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.editors.can;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.BitSet;

import org.beanmanager.editors.PropertyChangeListener;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.basic.BitSetEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.struct.BitSetTools;
import org.scenarium.test.fx.FxTest;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class CanTrameEditor extends PropertyEditor<CanTrame> {
	private static final String SEPARATOR = " ";
	private HBox hBox;
	private IntegerEditor idEditor;
	private BitSetEditor dataEditor;
	boolean newMode = true;

	public static void main(String[] args) throws IOException {
		// for (int i = 0; i < 100000000; i++) {
		// double val = Math.random();
		// double val2 = Double.parseDouble(Double.toString(val));
		// if(val != val2)
		// System.out.println("false");
		// }
		CanTrameEditor c = new CanTrameEditor();
		CanTrame t = new CanTrame(CanTrame.ADRESSE_MAX_CAN_STD, false, new byte[] { 0x75, 0x75 });
		CanTrame t2 = new CanTrame(CanTrame.ADRESSE_MAX_CAN_STD, false, new byte[] { (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0 });
		CanTrame t3 = new CanTrame(CanTrame.ADRESSE_MAX_CAN_EXT, true, new byte[] { 0x75, 0x75 });
		CanTrame t4 = new CanTrame(CanTrame.ADRESSE_MAX_CAN_EXT, true, new byte[] { (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0 });
		try (DataOutputStream r = new DataOutputStream(new FileOutputStream("test"))) {
			c.writeValue(r, t); // id: 1118481
			c.writeValue(r, t2); // id: 1118481
			c.writeValue(r, t3); // id: 1118481
			c.writeValue(r, t4); // id: 1118481
		}

		DataInputStream dis = new DataInputStream(new FileInputStream("test"));
		if (!c.readValue(dis).equals(t))
			System.out.println("error byte value");
		if (!c.readValue(dis).equals(t2))
			System.out.println("error byte value");
		if (!c.readValue(dis).equals(t3))
			System.out.println("error byte value");
		if (!c.readValue(dis).equals(t4))
			System.out.println("error byte value");
		// for (int i = 0; i < 100000000; i++) {
		// int size = (int) (Math.random() * 8.99);
		// byte[] datas = new byte[size];
		// for (int j = 0; j < datas.length; j++)
		// datas[j] = (byte) (Math.random() * 255);
		// boolean isExtended = Math.random() < 0.5;
		// int id = (int) (Math.random() * (isExtended ? CanTrame.ADRESSEMAXCANEXT : CanTrame.ADRESSEMAXCANSTD));
		// CanTrame ct = new CanTrame(id, isExtended, datas);
		// CanTrameEditor cantrameEditor = new CanTrameEditor();
		// DataOutputStream raf = new DataOutputStream(new FileOutputStream("test"));
		// cantrameEditor.setValue(ct);
		// cantrameEditor.writeValue(raf, ct);
		// raf.close();
		// DataInputStream raf2 = new DataInputStream(new FileInputStream("test"));
		// if (!cantrameEditor.readValue(raf2).equals(ct))
		// System.out.println("error byte value");
		// raf.close();
		// String text = cantrameEditor.getAsText();
		// cantrameEditor.setAsText(text);
		// if (!ct.equals(cantrameEditor.getValue()))
		// System.out.println("error string value");
		// System.out.println(i + " done: " + ct);
		// }
		new FxTest().launchIHM(args, () -> new VBox(new CanTrameEditor().getEditor(), new CanTrameEditor().getEditor()), true);
	}

	@Override
	public String getAsText() {
		CanTrame canTrame = getValue();
		if (canTrame == null)
			canTrame = new CanTrame(new byte[CanTrame.TRAME_MAX_SIZE]);
		StringBuilder sb = new StringBuilder();
		int dataLength = canTrame.getData().length;
		for (int i = 0; i < dataLength; i++)
			sb.append(String.format("%02X", canTrame.getData()[i]));
		return (canTrame.isExtendedFrame() ? "e" : "s") + Integer.toString(canTrame.getId()) + SEPARATOR + sb.toString();
	}

	@Override
	protected Region getCustomEditor() {
		if (this.hBox != null) {
			updateGUI();
			return this.hBox;
		}
		if (getValue() == null)
			setValue(new CanTrame(new byte[CanTrame.TRAME_MAX_SIZE]));
		PropertyChangeListener pcl = () -> setValue(new CanTrame(this.idEditor.getValue(), true, BitSetTools.getDataFromBitSet(this.dataEditor.getValue(), CanTrame.CAN_DATA_MAX_LENGHT)));
		CanTrame canTrame = getValue();
		this.idEditor = new IntegerEditor(0, CanTrame.ADRESSE_MAX_CAN_EXT, ControlType.TEXTFIELD);
		this.idEditor.setValue(canTrame.getId());
		this.idEditor.addPropertyChangeListener(pcl);
		TextField idComp = (TextField) ((HBox) this.idEditor.getNoSelectionEditor()).getChildren().get(0);
		idComp.setPrefWidth(100);
		this.dataEditor = new BitSetEditor(0, CanTrame.CAN_DATA_MAX_LENGHT * Byte.SIZE);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < canTrame.getData().length; i++)
			sb.append(String.format("%02X", canTrame.getData()[i]));
		this.dataEditor.setAsText(new BigInteger(sb.toString(), 16).toString(2));
		this.dataEditor.addPropertyChangeListener(pcl);
		Region ei = this.idEditor.getNoSelectionEditor();
		Region ed = this.dataEditor.getNoSelectionEditor();
		HBox.setHgrow(ei, Priority.ALWAYS);
		HBox.setHgrow(ed, Priority.ALWAYS);
		this.hBox = new HBox(new Label("Id: "), ei, new Label("Data: "), ed);
		this.hBox.setAlignment(Pos.BASELINE_CENTER);
		this.hBox.setSpacing(3);
		return this.hBox;
	}

	@Override
	public int getPitch() {
		return CanTrame.TRAME_MAX_SIZE;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public CanTrame readValue(DataInput raf) throws IOException {
		byte[] trame = new byte[CanTrame.TRAME_MAX_SIZE];
		raf.readFully(trame);
		if (this.newMode) {
			boolean isExt = (trame[0] & 0x80) != 0;
			boolean isFull = (trame[0] & 0b01000000) != 0;
			if (isExt && isFull)
				trame[0] &= 0b10111111;
			else {
				byte nbData = (trame[0] & 0b01000000) != 0 ? 8 : trame[trame.length - 1];
				byte[] tmptTrame = new byte[nbData + (isExt ? CanTrame.CAN_B_ID_LENGHT : CanTrame.CAN_A_ID_LENGHT)];
				if (isExt) {
					tmptTrame[0] = trame[0];
					tmptTrame[1] = trame[1];
					tmptTrame[2] = trame[2];
					tmptTrame[3] = trame[3];
					nbData += CanTrame.CAN_B_ID_LENGHT;
					for (int i = CanTrame.CAN_B_ID_LENGHT; i < nbData; i++)
						tmptTrame[i] = trame[i];
					tmptTrame[0] &= 0b10111111;
				} else {
					tmptTrame[0] = trame[2];
					tmptTrame[1] = trame[3];
					nbData += CanTrame.CAN_A_ID_LENGHT;
					for (int i = CanTrame.CAN_A_ID_LENGHT; i < nbData; i++)
						tmptTrame[i] = trame[i + CanTrame.CAN_A_ID_LENGHT];
				}
				trame = tmptTrame;
			}
		}
		return new CanTrame(trame);
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null || text.isEmpty())
			setValue(null);
		else {
			int sepaIndex = text.indexOf(SEPARATOR);
			String hex = text.substring(sepaIndex + 1);
			byte[] data = new byte[hex.length() / 2];
			for (int i = 0; i < data.length; i++)
				data[i] = (byte) (Integer.parseInt(hex.substring(2 * i, 2 * (i + 1)), 16) & 0xff);
			try {
				setValue(new CanTrame(Integer.parseInt(text.substring(1, sepaIndex)), text.charAt(0) == 'e', data));
			} catch (StringIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateCustomEditor() {
		CanTrame canTrame = getValue();
		this.idEditor.setValue(canTrame.getId());
		BitSet data = BitSet.valueOf(canTrame.getData());
		data.set(CanTrame.CAN_DATA_MAX_LENGHT * Byte.SIZE + 1);
		this.dataEditor.setValue(data);
		this.idEditor.updateGUI();
		this.dataEditor.updateGUI();
	}

	@Override
	public void writeValue(DataOutput dos, CanTrame value) throws IOException {
		byte[] trame = value.getTrame();
		if (this.newMode)
			if (trame.length == CanTrame.TRAME_MAX_SIZE) {
				byte[] tmpTrame = trame.clone();
				tmpTrame[0] |= 0b01000000;
				trame = tmpTrame;
			} else {
				byte[] tmpTrame = new byte[CanTrame.TRAME_MAX_SIZE];
				boolean isExt = (trame[0] & 0x80) != 0;
				byte nbData;
				if (isExt) { // ext
					System.arraycopy(trame, 0, tmpTrame, 0, trame.length);
					nbData = (byte) (trame.length - CanTrame.CAN_B_ID_LENGHT);
				} else {
					tmpTrame[0] = 0;
					tmpTrame[1] = 0;
					tmpTrame[2] = trame[0];
					tmpTrame[3] = trame[1];
					nbData = (byte) (trame.length - CanTrame.CAN_A_ID_LENGHT);
					for (int i = 0; i < nbData; i++)
						tmpTrame[i + CanTrame.CAN_B_ID_LENGHT] = trame[i + CanTrame.CAN_A_ID_LENGHT];
				}
				if (nbData == 8)
					tmpTrame[0] |= 0b01000000;
				else
					tmpTrame[tmpTrame.length - 1] = nbData;
				trame = tmpTrame;
			}
		dos.write(trame);
	}
}
