/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.output;

import java.io.IOException;

import org.scenarium.struct.raster.ByteRaster;

public class ByteRasterOutputStream extends DataFlowOutputStream<ByteRaster> {

	@Override
	public void push(ByteRaster raster) throws IOException {
		this.dataOutput.writeInt(raster.getWidth());
		this.dataOutput.writeInt(raster.getHeight());
		this.dataOutput.writeInt(raster.getType());
		this.dataOutput.write(raster.getData());
	}
}
