/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

import org.scenarium.ModuleManager;
import org.scenarium.filemanager.scenario.ScenariumScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.Trigger;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.operator.basic.Recorder;
import org.scenarium.test.versionconverter.FilesGrabber;
import org.scenarium.timescheduler.EvenementScheduler;
import org.scenarium.timescheduler.ScheduleTask;
import org.scenarium.timescheduler.SchedulerInterface;
import org.scenarium.timescheduler.TriggerMode;

//TODO remettre final sur timeofIssue
//TODO remettre process recorder
//TODO FileStreamRecorder remove ++
public class LogSylvain {

	private LogSylvain() {}

	public static void main(String[] args) {
		File rootFile = new File("/home/revilloud/Mansart/Datasets/VEDECOM/Scenic_Kurt/Scenarium");
		ModuleManager.loadEmbeddedAndInternalModules();
		HashSet<File> files = FilesGrabber.getAllFiles(rootFile, fileName -> fileName.endsWith(".psf") && fileName.startsWith("Rec"));
		files.removeIf(f -> f.getAbsolutePath().contains("20190114"));

		// ArrayList<File> files = new ArrayList<File>();
		// for (File file : new File("/home/revilloud/Mansart/Datasets/VEDECOM/Scenic_Kurt/Scenarium/").listFiles()) {// args[0]
		// if(file.getAbsolutePath().contains("20190114"))
		// continue;
		// if (file.isDirectory()) {
		// for (File subFile : file.listFiles()) {
		// if (!subFile.isDirectory() && subFile.getName().endsWith(".psf"))
		// files.add(subFile);
		// }
		// } else if (file.getName().endsWith(".psf"))
		// files.add(file);
		// }
		for (File file : files) {
			// new Thread(() -> {
			System.out.println("update file: " + file);
			updateLog(file);
			System.out.println("update file: " + file + " end");
			// }).start();
		}
		// updateLog(new File("/mnt/Log/Log/Scenarium/Versailles_Synchro/Rec_20190114_133711/Rec_20190207_173558.psf"));
	}

	private static void updateLog(File file) {
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		ScenariumScheduler sc = new ScenariumScheduler();
		String[] scenariosNames = null;
		try {
			sc.load(file, false);
			sc.setScheduler(new SchedulerInterface(new EvenementScheduler(false) {
				@Override
				public void setTasks(ArrayList<ScheduleTask> st) {
					schedulableTasks.addAll(st);
				}
			}));
			scenariosNames = sc.getScenariosName();
		} catch (IOException | ScenarioException ex) {
			ex.printStackTrace();
		}
		// for (ScheduleTask st : schedulableTasks) {
		// // System.out.println(st.task.toString() + " " + st.seekIndex + " " + st.timeStamp + " " + st.timeofIssue);
		// if (st.task.toString().contains("10_GPS.ppd") || st.task.toString().contains("10_GPS.pindex"))
		// ;// st.timeStamp = st.timeofIssue;
		// }

		TriggerMode triggerMode = TriggerMode.TIMEOFISSUE;
		Collections.sort(schedulableTasks, triggerMode == TriggerMode.TIMESTAMP ? (a, b) -> Long.compare(a.timeStamp, b.timeStamp) : (a, b) -> Long.compare(a.timeofIssue, b.timeofIssue));
		LinkedHashMap<String, Class<?>> nameTypemap = new LinkedHashMap<>();
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			String name = ScenariumScheduler.getScenarioName(ls.getFile().getName());
			Class<?> c = nameTypemap.get(name);
			if (c == null)
				nameTypemap.put(name, ((LocalScenario) st.task).getDataType());
			else if (c != ((LocalScenario) st.task).getDataType()) {
				System.err.println("Conflict, there is two log with the name " + name + " be with differrent type: " + c.getSimpleName() + " and " + ((LocalScenario) st.task).getDataType());
				return;
			}
		}
		String[] names = new String[nameTypemap.size()];
		Class<?>[] types = nameTypemap.values().toArray(new Class<?>[nameTypemap.size()]);
		ArrayList<String> toRemove = new ArrayList<>();
		for (String name : nameTypemap.keySet())
			for (int j = 0; j < scenariosNames.length; j++) {
				String scenarioName = scenariosNames[j];
				if (scenarioName.equals(name) && names[j] == null) {
					names[j] = name;
					types[j] = nameTypemap.get(name);
					toRemove.add(name);
					break;
				}
			}
		for (String name : toRemove)
			nameTypemap.remove(name);
		nameTypemap.forEach((name, type) -> {
			for (int i = 0; i < names.length; i++)
				if (names[i] != null) {
					names[i] = name;
					types[i] = type;
					return;
				}
		});
		class TimeManagedRecorder extends Recorder {
			private long ts;
			private long toi;

			@Override
			public String[] getOutputLinkToInputName() {
				String[] oltin = new String[names.length];
				for (int i = 0; i < names.length; i++) {
					String name = names[i];
					int index;
					if ((index = name.indexOf(Recorder.RECORINDEXSEPARATOR)) != -1)
						try {
							Integer.parseInt(name.substring(0, index));
							oltin[i] = name.substring(index + 1);
							continue;
						} catch (NumberFormatException e) {}
					oltin[i] = name;
				}
				return oltin;
			}

			@Override
			public Class<?>[] getOutputLinkToInputType() {
				return types;
			}

			@Override
			public long getTimeOfIssue(int i) {
				return this.toi;
			};

			@Override
			public long getTimeStamp(int i) {
				return this.ts;
			};

			public void setTimeOfIssue(long toi) {
				this.toi = toi;
			}

			public void setTimeStamp(long ts) {
				this.ts = ts;
			}
		}
		TimeManagedRecorder recorder = new TimeManagedRecorder();
		recorder.setRecordDirectory(file.getParentFile());
		class IndexedTrigger implements Trigger {
			Object[] recordObjects = new Object[names.length];
			private int indexOfRecord;

			public void setIndex(int index) {
				this.indexOfRecord = index;
			}

			@Override
			public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
				Arrays.fill(this.recordObjects, null);
				this.recordObjects[this.indexOfRecord] = outputValues[0];
				recorder.process(this.recordObjects);
			}
		}
		IndexedTrigger trigger = new IndexedTrigger();
		HashMap<LocalScenario, Integer> indexOfLocalScenario = new HashMap<>();
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			if (indexOfLocalScenario.get(ls) == null) {
				String name = ScenariumScheduler.getScenarioName(ls.getFile().getName());
				for (int j = 0; j < names.length; j++)
					if (name.equals(names[j])) {
						indexOfLocalScenario.put(ls, j);
						break;
					}
				ls.setTrigger(trigger);
			}
		}
		recorder.birth();
		for (ScheduleTask st : schedulableTasks) {
			LocalScenario ls = (LocalScenario) st.task;
			trigger.setIndex(indexOfLocalScenario.get(ls));
			recorder.setTimeOfIssue(st.timeofIssue);
			recorder.setTimeStamp(st.timeStamp);
			ls.update(st.seekIndex);
			if (Thread.currentThread().isInterrupted())
				break;
		}
		File recPath = new File(recorder.getRecordedPath()).listFiles()[0];
		recorder.death();
		String newRec = null;
		for (String oldRec : recPath.getParentFile().getParentFile().list())
			if (oldRec.endsWith(".psf")) {
				newRec = recPath.getParentFile().getParent() + File.separator + "New" + oldRec;
				break;
			}
		try {
			Files.copy(recPath.toPath(), new File(newRec).toPath());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		recPath.delete();
		recPath.getParentFile().delete();
		// if (Thread.currentThread().isInterrupted())
		// try {
		// Files.walk(Paths.get(recorder.getRecordedPath()), FileVisitOption.FOLLOW_LINKS).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}
}
