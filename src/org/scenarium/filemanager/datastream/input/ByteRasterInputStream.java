/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.input;

import java.io.IOException;

import org.scenarium.struct.raster.ByteRaster;

public class ByteRasterInputStream extends DataFlowInputStream<ByteRaster> {

	@Override
	public ByteRaster pop() throws IOException {
		ByteRaster rb = new ByteRaster(this.dataInput.readInt(), this.dataInput.readInt(), this.dataInput.readInt());
		this.dataInput.readFully(rb.getData());
		return rb;
	}
}
