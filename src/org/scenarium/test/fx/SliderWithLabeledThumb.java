/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class SliderWithLabeledThumb extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Slider slider = new Slider();
		StackPane root = new StackPane(slider);
		root.setPadding(new Insets(20));
		Scene scene = new Scene(root);
		slider.applyCss();
		slider.layout();
		Label label = new Label();
		label.textProperty().bind(slider.valueProperty().asString("%.1f"));
		((Pane) slider.lookup(".thumb")).getChildren().add(label);
		label.setOnMouseClicked(e -> {
			if (e.getClickCount() == 2)
				System.out.println("cool");
		});
		primaryStage.setScene(scene);
		primaryStage.show();

	}
}