/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;

import org.beanmanager.tools.Tuple;

public class NoGuiMainThread extends Thread {
	private static final LinkedBlockingDeque<Tuple<Runnable, CountDownLatch>> BLOCKINGQUEUE = new LinkedBlockingDeque<>();

	public NoGuiMainThread() {
		setName("ScenariumMainThread");
	}

	public void runTask(Runnable runnable) {
		if (Thread.currentThread().getId() == getId())
			runnable.run();
		else
			try {
				BLOCKINGQUEUE.put(new Tuple<>(runnable, null));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

	public void runTaskAndWait(Runnable runnable) {
		if (Thread.currentThread().getId() == getId())
			runnable.run();
		else {
			CountDownLatch cdl = new CountDownLatch(1);
			try {
				BLOCKINGQUEUE.put(new Tuple<>(runnable, cdl));
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		while (!isInterrupted())
			try {
				Tuple<Runnable, CountDownLatch> task = BLOCKINGQUEUE.take();
				task.getFirst().run();
				CountDownLatch cdl = task.getSecond();
				if (cdl != null)
					cdl.countDown();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}
