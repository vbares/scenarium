/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.WeakHashMap;

import org.beanmanager.testbean.TypeTest;
import org.scenarium.Scenarium;
import org.scenarium.operator.basic.Beep;
import org.scenarium.operator.basic.CSVRecorder;
import org.scenarium.operator.basic.FlowViewer;
import org.scenarium.operator.basic.Oscilloscope;
import org.scenarium.operator.basic.Recorder;
import org.scenarium.operator.basic.Synchronizer;
import org.scenarium.operator.basic.Viewer;
import org.scenarium.operator.communication.MjpegStreamer;
import org.scenarium.operator.communication.can.CAN;
import org.scenarium.operator.communication.can.CanDBCDecoder;
import org.scenarium.operator.communication.can.CanDBCEncoder;
import org.scenarium.operator.communication.nmea.NMEA0183Decoder;
import org.scenarium.operator.communication.serial.RXTXSerial;
import org.scenarium.operator.communication.socket.SocketTCP;
import org.scenarium.operator.communication.socket.SocketUDP;
import org.scenarium.operator.conversion.ToCrossMarker;
import org.scenarium.operator.dataprocessing.DataAnalyzer;
import org.scenarium.operator.dataprocessing.DataGenerator;
import org.scenarium.operator.dataprocessing.Devectorizer;
import org.scenarium.operator.dataprocessing.Expression;
import org.scenarium.operator.dataprocessing.PerlinNoise;
import org.scenarium.operator.dataprocessing.Vectorizer;
import org.scenarium.operator.image.Convolution;
import org.scenarium.operator.image.Crop;
import org.scenarium.operator.image.Gain;
import org.scenarium.operator.image.Histogram;
import org.scenarium.operator.image.ImageMuxer;
import org.scenarium.operator.image.ImageToVideo;
import org.scenarium.operator.image.Noiser;
import org.scenarium.operator.image.Rescale;
import org.scenarium.operator.image.Reverse;
import org.scenarium.operator.image.Threshold;
import org.scenarium.operator.image.VideoToImage;
import org.scenarium.operator.image.conversion.ImgDecoder;
import org.scenarium.operator.image.conversion.ImgEncoder;
import org.scenarium.operator.image.conversion.Mp4Encoder;
import org.scenarium.operator.image.conversion.TypeConverter;
import org.scenarium.operator.smoothing.Smoothing;
import org.scenarium.operator.test.ArrayGenerator;
import org.scenarium.operator.test.B1;
import org.scenarium.operator.test.B2;
import org.scenarium.operator.test.CompA;
import org.scenarium.operator.test.CompB;
import org.scenarium.operator.test.CompC;
import org.scenarium.operator.test.CompD;
import org.scenarium.operator.test.CompE;
import org.scenarium.operator.test.DoubleConsumer;
import org.scenarium.operator.test.IOTest;
import org.scenarium.operator.test.LifeGame;
import org.scenarium.operator.test.MyOperator;
import org.scenarium.operator.test.PersonBean;
import org.scenarium.operator.test.RasterGenerator;
import org.scenarium.operator.test.RmiOperatorTest;
import org.scenarium.operator.test.StartTest;
import org.scenarium.operator.wrapper.COrCpp;
import org.scenarium.operator.wrapper.Simulink;

public class OperatorManager {
	private static WeakHashMap<Class<?>, Boolean> operatorListCache = new WeakHashMap<>();
	private static final ArrayList<Class<?>> INTERN_OPERATORS = new ArrayList<>();

	static {
		// addInternOperator(Patate.class);
		// addInternOperator(SubClass.class);
		addInternOperator(Threshold.class);
		addInternOperator(Rescale.class);
		addInternOperator(Mp4Encoder.class);
		addInternOperator(MjpegStreamer.class);
		addInternOperator(Gain.class);
		addInternOperator(ImageMuxer.class);
		addInternOperator(Convolution.class);
		addInternOperator(Crop.class);
		addInternOperator(Histogram.class);
		addInternOperator(VideoToImage.class);
		addInternOperator(ImageToVideo.class);
		addInternOperator(ImgEncoder.class);
		addInternOperator(ImgDecoder.class);
		addInternOperator(Reverse.class);
		addInternOperator(TypeConverter.class);
		addInternOperator(Noiser.class);
		addInternOperator(CAN.class);
		addInternOperator(CanDBCDecoder.class);
		addInternOperator(CanDBCEncoder.class);
		addInternOperator(SocketUDP.class);
		addInternOperator(SocketTCP.class);
		addInternOperator(NMEA0183Decoder.class);
		addInternOperator(RXTXSerial.class);
		addInternOperator(ToCrossMarker.class);
		// addInternOperator(JSerialComm.class);
		addInternOperator(Oscilloscope.class);
		addInternOperator(PerlinNoise.class);
		addInternOperator(DataAnalyzer.class);
		addInternOperator(Beep.class);
		addInternOperator(Viewer.class);
		addInternOperator(FlowViewer.class);
		addInternOperator(Recorder.class);
		addInternOperator(Synchronizer.class);
		addInternOperator(CSVRecorder.class);
		addInternOperator(Vectorizer.class);
		addInternOperator(Devectorizer.class);
		addInternOperator(Expression.class);
		addInternOperator(Simulink.class);
		addInternOperator(COrCpp.class);
		addInternOperator(DataGenerator.class);
		addInternOperator(StartTest.class);

		if (Scenarium.DEBUG_MODE) {
			addInternOperator(TypeTest.class);
			addInternOperator(LifeGame.class);
			addInternOperator(MyOperator.class);
			addInternOperator(PersonBean.class);
			addInternOperator(RmiOperatorTest.class);
			addInternOperator(ArrayGenerator.class);
			addInternOperator(IOTest.class);
			addInternOperator(CompA.class);
			addInternOperator(CompB.class);
			addInternOperator(CompC.class);
			addInternOperator(CompD.class);
			addInternOperator(CompE.class);
			addInternOperator(DoubleConsumer.class);
			addInternOperator(B1.class);
			addInternOperator(B2.class);
			addInternOperator(Smoothing.class);
			addInternOperator(RasterGenerator.class);
		}
	}

	private OperatorManager() {}

	/** Add an operator to ScenariumFx. The operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operator the operator to add to ScenariumFx */
	public static void addInternOperator(Class<?> operator) {
		if (OperatorManager.isOperator(operator))
			INTERN_OPERATORS.add(operator);
		else
			System.err.println(operator + " is not an operator, an operator have no contructor, a public birth and death method without argument and with no return, and a public process method");
	}

	public static void purgeOperators(Module module) {
		INTERN_OPERATORS.removeIf(op -> op.getModule().equals(module));
	}

	/** Return a copy of the list of all operators of ScenariumFx.
	 *
	 * @return a copy of the list of all operators */
	public static ArrayList<Class<?>> getInternOperators() {
		return new ArrayList<>(INTERN_OPERATORS);
	}

	/** Tell if the operator class is an ScenariumFx valid operator. A valid operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operatorClass the class to test
	 * @return true if the operatorClass is a valid ScenariumFx operator */
	public static boolean isOperator(Object operatorClass) {
		if (operatorClass == null)
			return false;
		Class<?> type = operatorClass instanceof Class<?> ? (Class<?>) operatorClass : operatorClass.getClass();
		Boolean isOperator = operatorListCache.get(type);
		if (isOperator != null)
			return isOperator;
		boolean isBirthMethod = false;
		boolean isProcessMethod = false;
		boolean isDeathMethod = false;
		for (Method method : type.getMethods()) {
			String methoName = method.getName();
			if (methoName.equals("birth") && method.getParameterTypes().length == 0)
				isBirthMethod = true;
			else if (methoName.equals("process"))
				isProcessMethod = true;
			else if (methoName.equals("death") && method.getParameterTypes().length == 0)
				isDeathMethod = true;
			if (isBirthMethod && isProcessMethod && isDeathMethod) {
				operatorListCache.put(type, true);
				return true;
			}
		}
		operatorListCache.put(type, false);
		return false;
	}
}
