/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import org.beanmanager.BeanAndSubBeanPropertyChangeListener;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.rmi.RMIBeanManager;
import org.beanmanager.tools.Logger;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.display.drawer.geo.CrossMarker;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.Input;
import org.scenarium.filemanager.scenario.dataflowdiagram.ModificationType;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.VisuableSchedulableRemoteOperator;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.struct.CSVData;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.GeographicalPose;
import org.scenarium.struct.Mesh;
import org.scenarium.struct.OldObject3D;
import org.scenarium.struct.Text3D;
import org.scenarium.struct.Texture3D;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.EvolvedCurveSeries;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.DoubleRaster;
import org.scenarium.struct.raster.FloatRaster;
import org.scenarium.struct.raster.IntegerRaster;
import org.scenarium.struct.raster.ShortRaster;

import javafx.scene.paint.Color;
import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.Sentence;

public abstract class DiagramScheduler implements Trigger {
	private static final ConcurrentHashMap<Class<?>, Function<Object, Object>> CLONER_FACTORY = new ConcurrentHashMap<>();
	protected final ArrayList<Block> schedulableBlocks;
	protected final FlowDiagram fd;

	public DiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks) {
		this.schedulableBlocks = schedulableBlocks;
		this.fd = fd;
	}

	private static void addSynchronization(BeanManager[] beanManagers, BeanAndSubBeanPropertyChangeListener[] synchronizationListeners) {
		beanManagers[0].addBeanAndSubBeanPropertyChangeListener(synchronizationListeners[0]);
		beanManagers[1].addBeanAndSubBeanPropertyChangeListener(synchronizationListeners[1]);
	}

	public abstract void removeBlock(Block block);

	public abstract void addBlock(Block block);

	protected abstract void blockNameChanged(Block block);

	@Override
	public abstract void onStart(Object source, Runnable runnable);

	@Override
	public abstract void onResume(Object source, Runnable runnable);

	@Override
	public abstract void onPause(Object source, Runnable runnable);

	@Override
	public abstract void onStop(Block source, Runnable runnable);

	public abstract void resume();

	public abstract void start();

	public abstract void pause();

	public abstract void preStop();

	public abstract void stop();

	private static void populateSynchronization(Block block, BeanManager[] beanManagers, BeanAndSubBeanPropertyChangeListener[] synchronizationListeners) {
		RMIBeanManager rbm = new RMIBeanManager(block.getRemoteOperator(), BeanManager.defaultDir);
		BeanAndSubBeanPropertyChangeListener remoteListener = (beanType, beanName, propertyName) -> {
			Object value = rbm.getSubBeanValue(beanType, beanName, propertyName);
			Logger.log(Logger.RMI, "Remote property changed for block " + block + ":  bean type: " + beanType + ", beanName: " + beanName + ", property name: " + propertyName + ", value: " + value
					+ " transfer to local");
			new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").setValue(propertyName, value);
		};
		BeanAndSubBeanPropertyChangeListener localListener = (beanType, beanName, propertyName) -> {
			Object value = new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").getValue(propertyName);
			Logger.log(Logger.RMI, "Local property changed for block " + block + ": bean type: " + beanType + ", beanName: " + beanName + ", property name: " + propertyName + ", value: " + value
					+ " transfer to remote");
			rbm.setSubBeanValue(beanType, beanName, propertyName, value);
		};
		BeanManager lbm = new BeanManager(block.operator, BeanManager.defaultDir);
		synchronizationListeners[0] = localListener;
		synchronizationListeners[1] = remoteListener;
		beanManagers[0] = lbm;
		beanManagers[1] = rbm;
	}

	// @Override
	// public synchronized void onPause(Object source, Runnable runnable) {
	//// ArrayList<Runnable> _onPauseTasks = new ArrayList<>(onPauseTasks);
	//// _onPauseTasks.add(runnable);
	//// onPauseTasks = _onPauseTasks;
	// }
	//
	// public void pause() {
	// ArrayList<Runnable> _onPauseTasks = onPauseTasks;
	// if(_onPauseTasks != null)
	// _onPauseTasks.forEach(Runnable::run);
	// }
	//
	// @Override
	// public synchronized void onResume(Object source, Runnable runnable) {
	// ArrayList<Runnable> _onResumeTasks = new ArrayList<>(onResumeTasks);
	// _onResumeTasks.add(runnable);
	// onResumeTasks = _onResumeTasks;
	// }
	//
	// public void resume() {
	// ArrayList<Runnable> _onResumeTasks = onResumeTasks;
	// if(_onResumeTasks != null)
	// _onResumeTasks.forEach(Runnable::run);
	// }

	static {
		addCloner(Boolean.class, o -> o);
		addCloner(Character.class, o -> o);
		addCloner(Byte.class, o -> o);
		addCloner(Double.class, o -> o);
		addCloner(Float.class, o -> o);
		addCloner(Integer.class, o -> o);
		addCloner(Short.class, o -> o);
		addCloner(Long.class, o -> o);
		addCloner(String.class, o -> o);
		addCloner(File.class, o -> o);
		addCloner(InetSocketAddress.class, o -> o);
		addCloner(Color.class, o -> o);
		addCloner(Curve.class, Curve::clone);
		addCloner(CurveSeries.class, CurveSeries::clone);
		addCloner(EvolvedCurveSeries.class, EvolvedCurveSeries::clone);
		addCloner(CanTrame.class, CanTrame::clone);
		addCloner(BufferedImage.class, o -> {
			BufferedImage bi = o;
			ColorModel cm = bi.getColorModel();
			return new BufferedImage(cm, bi.copyData(null), cm.isAlphaPremultiplied(), null);
		});
		addCloner(Point2d.class, o -> o.clone());
		addCloner(Point2f.class, o -> o.clone());
		addCloner(Point2i.class, o -> o.clone());
		addCloner(Vector2d.class, o -> o.clone());
		addCloner(Vector2f.class, o -> o.clone());
		addCloner(Point3d.class, o -> o.clone());
		addCloner(Point3f.class, o -> o.clone());
		addCloner(Point3i.class, o -> o.clone());
		addCloner(Vector3d.class, o -> o.clone());
		addCloner(Vector3f.class, o -> o.clone());
		addCloner(Vector4d.class, o -> o.clone());
		addCloner(Vector4f.class, o -> o.clone());
		addCloner(GVector.class, o -> o.clone());
		addCloner(Color4f.class, o -> o.clone());
		addCloner(Matrix3f.class, o -> o.clone());
		addCloner(Matrix3d.class, o -> o.clone());
		addCloner(Matrix4f.class, o -> o.clone());
		addCloner(Matrix4d.class, o -> o.clone());
		addCloner(GMatrix.class, o -> o.clone());
		addCloner(BitSet.class, o -> (BitSet) o.clone());
		addCloner(Sentence.class, o -> SentenceFactory.getInstance().createParser(o.toString()));
		addCloner(CSVData.class, CSVData::clone);
		addCloner(OldObject3D.class, OldObject3D::clone);
		addCloner(Text3D.class, Text3D::clone);
		addCloner(Texture3D.class, Texture3D::clone);
		addCloner(Mesh.class, Mesh::clone);
		addCloner(GeographicCoordinate.class, GeographicCoordinate::clone);
		addCloner(GeographicalPose.class, GeographicalPose::clone);
		addCloner(CrossMarker.class, CrossMarker::clone);
		addCloner(ByteRaster.class, ByteRaster::clone);
		addCloner(ShortRaster.class, ShortRaster::clone);
		addCloner(IntegerRaster.class, IntegerRaster::clone);
		addCloner(FloatRaster.class, FloatRaster::clone);
		addCloner(DoubleRaster.class, DoubleRaster::clone);
		addCloner(LocalTime.class, o -> o);
		addCloner(LocalDate.class, o -> o);
		addCloner(LocalDateTime.class, o -> o);
		addCloner(byte[].class, byte[]::clone);
		addCloner(short[].class, short[]::clone);
		addCloner(int[].class, int[]::clone);
		addCloner(long[].class, long[]::clone);
		addCloner(float[].class, float[]::clone);
		addCloner(double[].class, double[]::clone);
		addCloner(boolean[].class, boolean[]::clone);
		addCloner(char[].class, char[]::clone);
		addCloner(byte[][].class, o -> {
			byte[][] arr = o;
			byte[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(short[][].class, o -> {
			short[][] arr = o;
			short[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(int[][].class, o -> {
			int[][] arr = o;
			int[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(long[][].class, o -> {
			long[][] arr = o;
			long[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(float[][].class, o -> {
			float[][] arr = o;
			float[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(double[][].class, o -> {
			double[][] arr = o;
			double[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(boolean[][].class, o -> {
			boolean[][] arr = o;
			boolean[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		addCloner(char[][].class, o -> {
			char[][] arr = o;
			char[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object> boolean addCloner(Class<T> type, Function<T, T> cloneFunction) {
		return CLONER_FACTORY.putIfAbsent(type, (Function<Object, Object>) cloneFunction) == null;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object> void replaceCloner(Class<T> type, Function<T, T> cloneFunction) {
		CLONER_FACTORY.put(type, (Function<Object, Object>) cloneFunction);
	}

	public static void purgeCloners(Module module) {
		CLONER_FACTORY.keySet().removeIf(c -> c.getModule().equals(module));
	};

	public static Object clone(Object object) {
		if (object == null)
			return null;
		Class<? extends Object> c = object.getClass();
		Function<Object, Object> clonemethod = CLONER_FACTORY.get(c);
		Object val = null;
		if (clonemethod != null)
			val = clonemethod.apply(object);
		else if (c.isArray()) {
			Class<?> type = c.getComponentType();
			if (type.isArray()) {
				Class<?> subType = type.getComponentType();
				Function<Object, Object> method = CLONER_FACTORY.get(subType);
				if (method == null) {
					method = loadCloneMethod(type);
					if (method == null) {
						System.err.println("Cannot clone: " + object);
						return val;
					}
				}
				Object[][] in = (Object[][]) object;
				Object[][] out = (Object[][]) Array.newInstance(type, in.length);
				for (int i = 0; i < out.length; i++) {
					Object[] subin = in[i];
					if (subin != null) {
						Object[] subout = (Object[]) Array.newInstance(subType, subin.length);
						for (int j = 0; j < subout.length; j++) {
							Object v = subin[j];
							subout[j] = v == null ? null : method.apply(subin[j]);
						}
						out[i] = subout;
					}
				}
				val = out;
			} else {
				Object[] in = (Object[]) object;
				Object[] out = (Object[]) Array.newInstance(type, in.length);
				Function<Object, Object> method = CLONER_FACTORY.get(type);
				if (method == null) {
					method = loadCloneMethod(type);
					if (method == null) {
						System.err.println("Cannot clone: " + object);
						return val;
					}
				}
				for (int i = 0; i < out.length; i++) {
					Object v = in[i];
					out[i] = v == null ? null : method.apply(v);
				}
				val = out;
			}
		} else {
			clonemethod = loadCloneMethod(c);
			if (clonemethod != null)
				val = clonemethod.apply(object);
		}
		if (val == null) {
			System.err.println("Cannot clone: " + object);
			return null;
		}
		return val;
	}

	public static void initBlock(Block block, Trigger trigger) {
		Object op = block.operator;
		if (op instanceof EvolvedOperator)
			((EvolvedOperator) op).setTrigger(trigger);
		else
			block.setRunLaterFunction(runnable -> trigger.triggerTask(block, runnable));
		block.setDefaulting(false);
	}

	public void initOperator(Block block) {
		BeanAndSubBeanPropertyChangeListener[] synchronizationListeners = new BeanAndSubBeanPropertyChangeListener[2];
		BeanManager[] beanManagers = new BeanManager[2];
		block.setRunTimePropertyChangeListener(evt -> {
			if (!block.isRunTimePropertyChangeListener())
				return;
			switch (evt.getPropertyName()) {
			case "consumesAllDataBeforeDying":
			case "startPriority":
			case "stopPriority":
			case "remoteProperty":
				return;
			case "name":
				blockNameChanged(block);
				return;
			case "synchronizeProperties":
				if ((Boolean) evt.getNewValue()) {
					populateSynchronization(block, beanManagers, synchronizationListeners);
					addSynchronization(beanManagers, synchronizationListeners);
					beanManagers[0].copyTo(beanManagers[1]);
				} else {
					beanManagers[1].removeBeanAndSubBeanPropertyChangeListener(synchronizationListeners[1]);
					beanManagers[1] = null;
					synchronizationListeners[1] = null;
					beanManagers[0].removeBeanAndSubBeanPropertyChangeListener(synchronizationListeners[0]);
					beanManagers[0] = null;
					synchronizationListeners[0] = null;
				}
				break;
			default:
				Logger.log(Logger.RUNTIME, "Required restart of " + block + " due to run mode change");
				if (block.operator instanceof Scenario) // No other way... Scheduler remains blocked otherwise due to restTime if re-enable
					((Scenario) block.operator).stopAndWait();
				else
					triggerTask(block, () -> {
						Logger.log(Logger.RUNTIME, "Relaunch " + block + " due to run mode change");
						destroyBlock(block, true);
						initBlock(block, DiagramScheduler.this);
						initOperator(block);
						Logger.log(Logger.RUNTIME, "Relaunch done " + block);
						this.fd.blockModeChanged(block, ModificationType.CHANGE);
					});
			}
		});
		if (block.isEnable())
			try {
				if (block.getProcessMode() == ProcessMode.ISOLATED || block.getProcessMode() == ProcessMode.REMOTE) {
					// System.out.println("rmi operator, main thread pid: " + ProcessHandle.current().pid());
					RemoteOperator remoteOperator = RemoteOperator.createRemoteOperator(block, true);
					if (remoteOperator.connectionError()) {
						block.setWarning("Cannot connect to the RMI process. It could be a port already in use error. See general console for more information");
						block.setBirthFailed(true);
					}
					block.setRemoteOperator(remoteOperator);
					if (block.isSynchronizeProperties()) {
						populateSynchronization(block, beanManagers, synchronizationListeners);
						addSynchronization(beanManagers, synchronizationListeners);
					}
					// remoteOperator.loadBeanProperties(ObjectEditor.getBeanDesc(op).name);
					remoteOperator.birth();
					block.setBirthFailed(false);
				} else {
					Method declaredMethod = block.operator.getClass().getMethod("birth", new Class<?>[0]);
					declaredMethod.setAccessible(true);
					MethodHandles.lookup().unreflect(declaredMethod).invoke(block.operator);
					block.setBirthFailed(false);
				}
			} catch (Throwable e) {
				if (e instanceof InvocationTargetException) {
					Throwable te = ((InvocationTargetException) e).getTargetException();
					if (te instanceof Exception)
						e = te;
				}
				block.setDefaulting(true);
				block.setBirthFailed(true);
				if (e instanceof InterruptedException)
					System.err.println("The initialisation of " + block.getName() + " was interrupted");
				else {
					String em = ScenarioManager.getErrorMessage(e);
					if (em == null)
						e.printStackTrace();
					else
						System.err.println(block.getName() + ": " + em);
				}
				destroyBlock(block, true);
			}
	}

	static void destroyBlock(Block block, boolean destroyOperator) {
		// System.err.println("destroy block: " + block);
		Object operator = block.operator;
		if (operator instanceof EvolvedOperator)
			((EvolvedOperator) operator).setTrigger(null);
		else
			block.setRunLaterFunction(null);
		block.setRunTimePropertyChangeListener(null);
		if (destroyOperator)
			try {
				RemoteOperator remoteOperator = block.getRemoteOperator();
				if (remoteOperator != null) {
					remoteOperator.death();
					if (remoteOperator instanceof VisuableSchedulableRemoteOperator)
						((VisuableSchedulableRemoteOperator) remoteOperator).setAnimated(false);
					remoteOperator.destroy();
					block.setRemoteOperator(null);
				} else {
					Method declaredMethod = operator.getClass().getMethod("death", new Class<?>[0]);
					declaredMethod.setAccessible(true);
					MethodHandles.lookup().unreflect(declaredMethod).invoke(operator);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		block.clearTemporaryBuffer();
		block.reset();
	}

	public static boolean isCloneable(Class<?> type) {
		if (type.isArray()) {
			type = type.getComponentType();
			if (type.isArray())
				type = type.getComponentType();
		}
		Class<?> superClass = type;
		do {
			if (CLONER_FACTORY.get(superClass) != null)
				return true;
			Class<?>[] inter = superClass.getInterfaces();
			for (Class<?> class1 : inter)
				if (CLONER_FACTORY.get(class1) != null)
					break;
		} while ((superClass = superClass.getSuperclass()) != null);

		return true;
	}

	private static Function<Object, Object> loadCloneMethod(Class<? extends Object> c) {
		Class<?> superClass = c;
		Function<Object, Object> clonemethod = null;
		do {
			Class<?>[] inter = superClass.getInterfaces();
			clonemethod = CLONER_FACTORY.get(superClass);
			if (clonemethod != null)
				break;
			for (Class<?> class1 : inter) {
				clonemethod = CLONER_FACTORY.get(class1);
				if (clonemethod != null)
					break;
			}
		} while (clonemethod == null && (superClass = superClass.getSuperclass()) != null);
		if (clonemethod != null)
			CLONER_FACTORY.put(c, clonemethod);
		return clonemethod;
	}

	/** Process a block with the given inputs and their corresponding timeStamps and timeOfIssue
	 *
	 * @param block the block to process
	 * @param inputsData the inputs data to be processed
	 * @param timeStamps the timeStamps for each inputs datas
	 * @param toithe timeOfIssue for each inputs datas
	 * @return true if there is at least one output for this block */
	static boolean processBlock(Block block, Object[] inputsData, long[] timeStamps, long[] timeOsIssue) {
		try {
			long maxTimeStamp = Long.MIN_VALUE;
			for (long ts : timeStamps)
				if (ts > maxTimeStamp)
					maxTimeStamp = ts;
			block.setInputTs(timeStamps);
			block.setTimeOfIssue(timeOsIssue);
			int nbPropertyInput = block.getNbPropertyInput();
			int nbStaticInput = block.getNbStaticInput();
			int nbDynamicInput = block.getNbDynamicInput();
			int nbVarArgsInput = block.getNbVarArgsInput();
			int ip = 0;
			for (; ip < nbPropertyInput; ip++) {
				Object propertyValue = inputsData[ip];
				if (propertyValue != null)
					try {
						block.getPropertyMethodHandles(ip).invoke(propertyValue);
						block.setInputError(ip, null);
					} catch (Exception e) {
						System.err.println(block.getName() + ": error while setting the property as input due to: " + e.getMessage());
						block.setInputError(ip, e.getMessage());
					}
			}
			int l = ip;
			for (; l < inputsData.length; l++)
				if (inputsData[l] != null) {
					l = 0;
					break;
				}
			if (l != 0)
				return false;
			ip += nbStaticInput;
			if (nbDynamicInput != 0) {
				Object[] additionalInputs = ((EvolvedOperator) block.operator).getAdditionalInputs();
				if (additionalInputs == null || additionalInputs.length != nbDynamicInput) {
					additionalInputs = new Object[nbDynamicInput]; // Ok que a l'init ou si changement de taille des entrées additionnelles
					((EvolvedOperator) block.operator).setAdditionalInputs(additionalInputs);
				}
				for (int i = 0; i < additionalInputs.length; i++)
					additionalInputs[i] = inputsData[ip++];
			}
			List<BlockOutput> blockOutputs = block.getOutputs();
			RemoteOperator remoteOp = block.getRemoteOperator();
			Object val;
			if (nbVarArgsInput >= 0) { // Cas avec des varArgs
				Object[] varArgs = block.getVarArgsBuff();
				// System.out.println("Block: " + block + " Varargs: " + Arrays.toString(varArgs));
				for (int j = 0; j < varArgs.length; j++)
					varArgs[j] = inputsData[ip++];
				ip -= nbStaticInput + nbDynamicInput + nbVarArgsInput;
				if (remoteOp != null) {
					Object[] staticInputs = new Object[nbStaticInput + 1];
					for (int index = 0; index < nbStaticInput; index++)
						staticInputs[index] = inputsData[ip++];
					staticInputs[staticInputs.length - 1] = varArgs;
					boolean[] linkedStaticOutput = new boolean[block.getNbStaticOutput()];
					for (int i = 0; i < linkedStaticOutput.length; i++)
						linkedStaticOutput[i] = blockOutputs.get(i).getLinkInputs() != null;
					val = remoteOp.process(staticInputs, nbDynamicInput != 0 ? ((EvolvedOperator) block.operator).getAdditionalInputs() : null, linkedStaticOutput);
				} else {
					MethodHandle ph = block.getProcessHandler();
					if (nbStaticInput == 0)
						val = ph.invoke(varArgs);
					else if (nbStaticInput == 1)
						val = ph.invoke(inputsData[ip++], varArgs); // id[0] avant
					else if (nbStaticInput == 2)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 3)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 4)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 5)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 6)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 7)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 8)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 9)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], varArgs);
					else if (nbStaticInput == 10)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 11)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 12)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 13)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 14)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 15)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else {
						Object[] staticInputs = new Object[nbStaticInput + 1]; // Ok que si très gros block
						for (int index = 0; index < nbStaticInput; index++)
							staticInputs[index] = inputsData[ip++];
						staticInputs[staticInputs.length - 1] = varArgs;
						val = ph.invokeWithArguments(staticInputs);
					}
				}
			} else { // Cas sans varArgs
				ip -= nbStaticInput + nbDynamicInput;
				if (remoteOp != null) {
					Object[] staticInputs;
					if (nbStaticInput != block.getNbInput()) {
						staticInputs = new Object[nbStaticInput]; // Ok que si très gros block
						for (int index = 0; index < nbStaticInput; index++)
							staticInputs[index] = inputsData[ip++];
					} else
						staticInputs = inputsData;
					boolean[] linkedStaticOutput = new boolean[block.getNbStaticOutput()];
					for (int i = 0; i < linkedStaticOutput.length; i++)
						linkedStaticOutput[i] = blockOutputs.get(i).getLinkInputs() != null;
					val = remoteOp.process(staticInputs, nbDynamicInput != 0 ? ((EvolvedOperator) block.operator).getAdditionalInputs() : null, linkedStaticOutput);
				} else {
					MethodHandle ph = block.getProcessHandler();
					if (nbStaticInput == 0)
						val = ph.invoke();
					else if (nbStaticInput == 1)
						val = ph.invoke(inputsData[ip]);
					else if (nbStaticInput == 2)
						val = ph.invoke(inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 3)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 4)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 5)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 6)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 7)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 8)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 9)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 10)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 11)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 12)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 13)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 14)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 15)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else {
						Object[] staticInputs;
						if (nbStaticInput != block.getNbInput()) {
							staticInputs = new Object[nbStaticInput]; // Ok que si très gros block
							for (int index = 0; index < nbStaticInput; index++)
								staticInputs[index] = inputsData[ip++];
						} else
							staticInputs = inputsData;
						val = ph.invokeWithArguments(staticInputs);
					}
				}
			}
			long timeOfIssue = System.currentTimeMillis();
			block.consume();
			// if(beginTime == -1 && block.getConsume() == 1) {
			// beginTime = System.currentTimeMillis();
			// }
			// int nbIt = 1000000;
			// if(block.getConsume() == nbIt) {
			// double seconde = (System.currentTimeMillis() - beginTime) / 1000.0;
			// System.out.println("end Time: " + seconde + "s " + String.format("%.4f", seconde / 20.0 / nbIt * 1000000) + "μs/T");
			// }

			block.setDefaulting(false);

			Object[] outputValues = block.getOutputBuff();
			if (outputValues.length == 0)
				return false;
			int i = 0;
			long[] outputValuesTs = block.getOutputBuffTs();
			long[] outputValuesToi = block.getOutputBuffToi();
			int nbStaticOutput = block.getNbStaticOutput();
			boolean isAtLeastOnOutputs = false;
			if (remoteOp != null) {
				if (val == null)
					return false;
				Object[] outputs = (Object[]) val;
				if (outputs.length == 0)
					return false;
				if (blockOutputs.get(0).getLinkInputs() != null) {
					outputValues[0] = outputs[0];
					outputValuesTs[0] = maxTimeStamp;
					outputValuesToi[0] = timeOfIssue;
					isAtLeastOnOutputs = true;
					i++;
				} else if (!block.isProcessMethodReturnVoid()) { // TODO et si le return est pas cloneable et qu'il existe donc pas? Il faut check isProcessMethodReturnVoid avec un isCloneable
					outputValues[0] = null;
					i++;
				}
				for (; i < nbStaticOutput; i++) {
					BlockOutput output = blockOutputs.get(i);
					if (output.getLinkInputs() != null && (val = outputs[i]) != null) {
						outputValues[i] = val;
						outputValuesTs[i] = maxTimeStamp;
						outputValuesToi[i] = timeOfIssue;
						isAtLeastOnOutputs = true;
					} else
						outputValues[i] = null;
				}
			} else {
				if (val != null && /* outputValues != null && */ blockOutputs.get(0).getLinkInputs() != null) {
					outputValues[0] = clone(val);
					outputValuesTs[0] = maxTimeStamp;
					outputValuesToi[0] = timeOfIssue;
					isAtLeastOnOutputs = true;
					i++;
				} else if (!block.isProcessMethodReturnVoid()) { // TODO et si le return est pas cloneable et qu'il existe donc pas? Il faut check isProcessMethodReturnVoid avec un isCloneable
					outputValues[0] = null;
					i++;
				}
				for (; i < nbStaticOutput; i++) {
					BlockOutput output = blockOutputs.get(i);
					if (output.getLinkInputs() != null && (val = output.getMethodHandle().invoke()) != null) {
						outputValues[i] = clone(val);
						outputValuesTs[i] = maxTimeStamp;
						outputValuesToi[i] = timeOfIssue;
						isAtLeastOnOutputs = true;
					} else
						outputValues[i] = null;
				}
			}
			return isAtLeastOnOutputs;
		} catch (Throwable e) {
			block.setDefaulting(true);
			System.err.println("failed to process: " + block.getName());
			e.printStackTrace();
			return false;
		}
	}

	public static void showBufferOverflow(IOComponent block, Input input) {
		if (input.getBuffer().getNbMissedData() == 1)
			System.err.println(block + ": BufferOverflow on input: " + input + ", max buffer size: " + input.getBuffer().getMaxSize());
	}
}
