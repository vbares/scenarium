/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

import javax.swing.event.EventListenerList;
import javax.vecmath.Point2i;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;
import org.scenarium.Scenarium;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolBar.Console;
import org.scenarium.display.toolBar.DrawerProperties;
import org.scenarium.display.toolBar.Operators;
import org.scenarium.display.toolBar.PlayList;
import org.scenarium.display.toolBar.Player;
import org.scenarium.display.toolBar.StatusBar;
import org.scenarium.display.toolBar.TheaterFilter;
import org.scenarium.display.toolBar.Values;
import org.scenarium.display.toolBar.receditor.LogMerger;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.display.toolbarclass.InternalTool;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class RenderPane implements VisuableSchedulableContainer, LoadToolBarListener {
	private static ArrayList<ToolBarInfo> toolBarsInfo = new ArrayList<>();
	private static final EventListenerList TOOLBAR_LISTENERS = new EventListenerList();

	public static final String DRAWER_PROPERTIES_DIR = Scenarium.getLocalDir() + File.separator + "DrawerProperties" + File.separator;
	static {
		toolBarsInfo.add(new ToolBarInfo(Console.class, KeyCode.C, true));
		toolBarsInfo.add(new ToolBarInfo(Values.class, KeyCode.V, false));
		// toolBarsInfo.add(new ToolBarInfoFx(TheaterEditorFx.class, KeyCode.E, false));
		toolBarsInfo.add(new ToolBarInfo(TheaterFilter.class, KeyCode.F, false));
		toolBarsInfo.add(new ToolBarInfo(Player.class, KeyCode.P, true));
		toolBarsInfo.add(new ToolBarInfo(StatusBar.class, KeyCode.S, false));
		toolBarsInfo.add(new ToolBarInfo(PlayList.class, KeyCode.L, true));
		toolBarsInfo.add(new ToolBarInfo(Operators.class, KeyCode.O, true));
		toolBarsInfo.add(new ToolBarInfo(DrawerProperties.class, KeyCode.D, false));
		toolBarsInfo.add(new ToolBarInfo(LogMerger.class, KeyCode.M, true));
		File dpd = new File(DRAWER_PROPERTIES_DIR);
		if (!new File(DRAWER_PROPERTIES_DIR).exists())
			dpd.mkdirs();
	}

	public static boolean addToolBarDescs(ToolBarInfo toolBarDesc) {
		Objects.requireNonNull(toolBarDesc);
		for (ToolBarInfo tbi : toolBarsInfo)
			if (tbi.keyCode == toolBarDesc.keyCode) {
				System.err.println("Cannot add toolBarDesc: " + toolBarDesc + " tbd has the same accelerator");
				return false;
			}
		toolBarsInfo.add(toolBarDesc);
		fireToolBarLoaded(toolBarDesc);
		return true;
	}

	public static void purgeToolBars(Module module) {
		for (Iterator<ToolBarInfo> iterator = toolBarsInfo.iterator(); iterator.hasNext();) {
			ToolBarInfo toolBarInfo = iterator.next();
			if (toolBarInfo.type.getModule().equals(module)) {
				iterator.remove();
				fireToolBarUnloaded(toolBarInfo);
			}
		}
	}

	public static void addLoadToolBarListener(LoadToolBarListener listener) {
		TOOLBAR_LISTENERS.add(LoadToolBarListener.class, listener);
	}

	public static void removeLoadToolBarListener(LoadToolBarListener listener) {
		TOOLBAR_LISTENERS.remove(LoadToolBarListener.class, listener);
	}

	private static void fireToolBarLoaded(ToolBarInfo toolBarDesc) {
		for (LoadToolBarListener listener : TOOLBAR_LISTENERS.getListeners(LoadToolBarListener.class))
			listener.loaded(toolBarDesc);
	}

	private static void fireToolBarUnloaded(ToolBarInfo toolBarInfo) {
		for (LoadToolBarListener listener : TOOLBAR_LISTENERS.getListeners(LoadToolBarListener.class))
			listener.unloaded(toolBarInfo);
	}

	public static TheaterPanel createTheaterPanel(Object de)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, NullPointerException {
		return DrawerManager.getRenderPanelClass(de.getClass()).getConstructor().newInstance();
	}

	public static ArrayList<ToolBarInfo> getToolBarDescs() {
		return toolBarsInfo;
	}

	private static ToolBarInfo getToolBarDesc(Class<? extends Tool> toolClass) {
		for (ToolBarInfo tbi : toolBarsInfo)
			if (tbi.type.equals(toolClass))
				return tbi;
		return null;
	}

	private final EventListenerList listeners = new EventListenerList();
	private final boolean isMainFrame;
	private DataLoader dataLoader;
	private TheaterPanel theaterPane;
	private StackPane stackPane;
	private final HashMap<Class<? extends Tool>, Tool> displayedTools = new HashMap<>();
	private final HashMap<InternalTool, Region> internalToolParents = new HashMap<>();
	private ScenariumContainer scenariumContainer;
	private BorderPane toolPane;
	private FlowPane centerPane;
	private AnimationTimerConsumer animationTimerConsumer;
	private final HashSet<Class<? extends Tool>> excludedTools = new HashSet<>();

	public RenderPane(DataLoader dataLoader, Object de, ScenariumContainer scenariumContainer, TheaterPanel theaterPanel, boolean isMainFrame, boolean autoFitIfResize) {
		this(scenariumContainer.getScheduler(), de, scenariumContainer, theaterPanel, isMainFrame, autoFitIfResize);
		this.dataLoader = dataLoader;
	}

	public RenderPane(DataLoader dataLoader, ScenariumContainer scenariumContainer, boolean isMainFrame, boolean autoFitIfResize) {
		this(dataLoader, getScenarioData(dataLoader.getScenario()), scenariumContainer, null, isMainFrame, autoFitIfResize);
	}

	private static Object getScenarioData(Scenario scenario) {
		Object scenarioData = scenario.getScenarioData();
		if (scenarioData instanceof BufferedStrategy<?>)
			scenarioData = ((BufferedStrategy<?>) scenarioData).getElement();
		return scenarioData;
	}

	public RenderPane(Scheduler scheduler, Object de, ScenariumContainer scenariumContainer, TheaterPanel theaterPanel, boolean isMainFrame, boolean autoFitIfResize) {
		// if (de == null)
		// de = dataLoader.getScenario().getScenarioData();
		this.scenariumContainer = scenariumContainer;
		if (theaterPanel == null)
			try {
				this.theaterPane = createTheaterPanel(de);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// JOptionPane.showMessageDialog((Component) (scenariumContainer instanceof Component ? scenariumContainer : null), "Cannot create a renderPanel for the scenario", "RenderPanel error",
				// JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
		else
			this.theaterPane = theaterPanel;
		if (isMainFrame) { // rajout, les renderpane des viewer ne doivent pas charger les propriétées par default
			String tpName = this.theaterPane.getClass().getSimpleName();
			File drawerConfigFile = new File(DRAWER_PROPERTIES_DIR + tpName + BeanDesc.SEPARATOR + tpName + ".txt");
			if (drawerConfigFile.exists() && !new BeanManager(this.theaterPane, DRAWER_PROPERTIES_DIR).load(drawerConfigFile)) {
				BeanEditor.registerCreatedBeanAndCreateSubBean(this.theaterPane, tpName, DRAWER_PROPERTIES_DIR);
				System.err.println("pas de fichier");
			}
		}
		this.theaterPane.initialize(scenariumContainer, scheduler, de, autoFitIfResize);// déplacement ici pour theaterfilter

		this.stackPane = new StackPane();
		this.theaterPane.prefWidthProperty().bind(this.stackPane.widthProperty());
		this.theaterPane.prefHeightProperty().bind(this.stackPane.heightProperty());
		this.stackPane.setMinSize(0, 0);
		adaptSizeToDrawableElement();
		this.stackPane.getChildren().add(this.theaterPane);

		this.toolPane = new BorderPane();
		this.toolPane.setFocusTraversable(false);
		this.toolPane.setPickOnBounds(false);

		this.centerPane = new FlowPane(Orientation.VERTICAL);
		this.centerPane.setMinSize(0, 0);
		this.centerPane.setFocusTraversable(false);
		this.centerPane.setPickOnBounds(false);
		this.centerPane.setVisible(false);
		this.toolPane.setCenter(this.centerPane);
		this.toolPane.setVisible(false);
		this.stackPane.getChildren().add(this.toolPane);

		if (!scenariumContainer.isManagingAccelerator()) {
			ArrayList<ToolBarInfo> toolBarInfos = new ArrayList<>();
			for (ToolBarInfo toolBarInfo : toolBarsInfo)
				if (isMainFrame || !toolBarInfo.onlyMainFrame)
					toolBarInfos.add(toolBarInfo);
			// if (!scenariumContainer.isManagingAccelerator()) {
			this.stackPane.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
				if (e.isControlDown() || e.isAltDown() || e.isShiftDown())
					return;
				Object keyCode = e.getCode();
				for (int i = 0; i < toolBarInfos.size(); i++) {
					ToolBarInfo tbi = toolBarInfos.get(i);
					if (tbi.keyCode == keyCode) {
						updateToolView(tbi.type);
						e.consume();
						return;
					}
				}
			});
			// }
		}
		RenderPane.addLoadToolBarListener(this);
		this.isMainFrame = isMainFrame;
	}

	public void adaptSizeToDrawableElement() {
		Dimension2D dim = this.theaterPane.getDimension();
		double width = dim.getWidth();
		double height = dim.getHeight();
		if (this.stackPane.getWidth() == width && this.stackPane.getHeight() == height)
			return;
		Scene scene = this.theaterPane.getScene();
		if (scene != null) {
			Window windows = scene.getWindow();
			if (windows instanceof Stage) {
				Stage stage = (Stage) windows;

				this.stackPane.setPrefSize(1, 1); // Ajout sinon bug de taille si taille > screensize (voir FxTestSizeToScene)
				stage.sizeToScene();

				this.stackPane.setPrefSize(width, height);
				stage.sizeToScene();

				final boolean resizable = stage.isResizable();
				stage.setResizable(!resizable);
				stage.setResizable(resizable);
			}
		} else
			this.stackPane.setPrefSize(width, height); // ici avant
	}

	public void addPaneToRenderPane(Pane pane) {
		pane.setFocusTraversable(false);
		pane.setPickOnBounds(false);
		this.stackPane.getChildren().add(this.stackPane.getChildren().size(), pane);
	}

	public void addToolVisibleListener(final ToolVisibleListener listener) {
		this.listeners.add(ToolVisibleListener.class, listener);
	}

	public void close() {
		RenderPane.removeLoadToolBarListener(this);
		if (this.isMainFrame)
			saveDrawerProperties();
		for (Tool tb : this.displayedTools.values())
			if (tb != null)
				tb.dispose();
		this.theaterPane.death();
	}

	public boolean closeTool(Class<? extends Tool> toolType) {
		Tool tool = this.displayedTools.get(toolType);
		if (tool == null)
			return false;
		updateToolView(toolType);
		return true;
	}

	public void excludeToolBar(Class<? extends Tool> toolToRemove) {
		this.excludedTools.add(toolToRemove);
	}

	private void fireToolVisibleChanged(Class<? extends Tool> toolBarClass, boolean visible) {
		for (ToolVisibleListener listener : this.listeners.getListeners(ToolVisibleListener.class))
			listener.toolVisibleChanged(toolBarClass, visible);
	}

	public Region getCenterPane() {
		return this.centerPane;
	}

	public ScenariumContainer getContainer() {
		return this.scenariumContainer;
	}

	public DataLoader getDataLoader() {
		return this.dataLoader;
	}

	public StackPane getPane() {
		return this.stackPane;// theaterPanel instanceof PointCloudDrawer || theaterPanel instanceof MeshDrawer ? theaterPanel : stackPane;
	}

	public Dimension2D getPreferredSize() {
		return this.theaterPane.getPreferredSize();
	}

	public TheaterPanel getTheaterPane() {
		return this.theaterPane;
	}

	public boolean isStatusBar() {
		return isDisplayed(StatusBar.class);
	}

	public boolean isDisplayed(Class<? extends Tool> toolClass) {
		return this.displayedTools.get(toolClass) != null;
	}

	public void loadingOperation(ReadOnlyDoubleProperty progressProperty) {
		StatusBar statusBar = (StatusBar) this.displayedTools.get(StatusBar.class);
		if (statusBar != null)
			Platform.runLater(() -> statusBar.update(progressProperty, false));
		else {
			progressProperty.addListener((a, b, c) -> {
				if (c.doubleValue() == 1)
					Platform.runLater(() -> closeTool(StatusBar.class));
			});
			Platform.runLater(() -> {
				if (progressProperty.getValue().doubleValue() == 1)
					return;
				updateToolView(StatusBar.class);
				((StatusBar) this.displayedTools.get(StatusBar.class)).update(progressProperty, true);
			});
		}
	}

	public void openTool(Class<? extends Tool> toolClass) {
		if (this.displayedTools.get(toolClass) != null)
			return;
		updateToolView(toolClass);
	}

	public void reload(boolean resize) {
		Object de = this.dataLoader.getScenario().getScenarioData();
		if (de == null)
			return;
		if (DrawerManager.isCompatibleRenderPanel(this.theaterPane, de.getClass())) {
			this.theaterPane.setDrawableElement(de);
			if (resize)
				/* if ( */adaptSizeToDrawableElement()/* ) */;
			// theaterPane.sizeToScene();
		} else {
			this.stackPane.getChildren().remove(this.theaterPane);
			this.theaterPane.prefWidthProperty().unbind();
			this.theaterPane.prefHeightProperty().unbind();
			this.theaterPane.death();
			this.theaterPane = null;
			try {
				this.theaterPane = createTheaterPanel(de);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// JOptionPane.showMessageDialog(theaterPanel.getParent(), "Cannot create a renderPanel for the scenario", "RenderPanel error", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
			this.theaterPane.initialize(this.scenariumContainer, this.scenariumContainer.getScheduler(), de, true);
			this.theaterPane.prefWidthProperty().bind(this.stackPane.widthProperty());
			this.theaterPane.prefHeightProperty().bind(this.stackPane.heightProperty());
			this.stackPane.getChildren().add(0, this.theaterPane);
			adaptSizeToDrawableElement();
			// theaterPane.sizeToScene();
			this.theaterPane.ignoreRepaint = false;
			this.theaterPane.fitDocument();
		}
	}

	public void removePaneToLayeredPane(Pane pane) {
		this.stackPane.getChildren().remove(pane);
	}

	public void removeToolVisibleListener(final ToolVisibleListener listener) {
		this.listeners.remove(ToolVisibleListener.class, listener);
	}

	public void saveDrawerProperties() {
		File drawerPropertiesFile = new File(DRAWER_PROPERTIES_DIR);
		if (!drawerPropertiesFile.exists())
			drawerPropertiesFile.mkdir();
		String tpn = this.theaterPane.getClass().getSimpleName();
		new BeanManager(this.theaterPane, DRAWER_PROPERTIES_DIR).save(new File(DRAWER_PROPERTIES_DIR + tpn + BeanDesc.SEPARATOR + tpn + ".txt"), true);
	}

	@Override
	public void setAnimated(boolean animated, AnimationTimerConsumer animationTimerConsumer) {
		this.animationTimerConsumer = animationTimerConsumer;
		this.theaterPane.animated = animated;
		if (animated) {
			for (Tool toolBar : this.displayedTools.values())
				if (toolBar != null && toolBar instanceof VisuableSchedulable)
					animationTimerConsumer.register((VisuableSchedulable) toolBar);
			this.theaterPane.setAnimated(animated, animationTimerConsumer);
		}
	}

	public void setCursor(Cursor cursor) {
		this.stackPane.setCursor(cursor);
	}

	public void showMessage(String message, boolean error) {
		StatusBar statusBar = (StatusBar) this.displayedTools.get(StatusBar.class);
		if (statusBar != null)
			statusBar.update(message, error, false);
		else {
			updateToolView(StatusBar.class);
			((StatusBar) this.displayedTools.get(StatusBar.class)).update(message, error, true);
		}
	}

	public void showTool(Class<? extends Tool> toolClass) {
		if (this.excludedTools.contains(toolClass))
			return;
		Tool toolBar = this.displayedTools.get(toolClass);
		if (toolBar == null)
			updateToolView(toolClass);
		else if (toolBar instanceof ExternalTool)
			((ExternalTool) toolBar).requestFocus();
	}

	public void updateStatusBar(String... infos) {
		StatusBar statusBar = (StatusBar) this.displayedTools.get(StatusBar.class);
		if (statusBar != null)
			statusBar.update(infos);
	}

	void updateToolView(Class<? extends Tool> toolClass) {
		if (this.excludedTools.contains(toolClass))
			return;
		Tool toolBar = this.displayedTools.get(toolClass);
		this.scenariumContainer.updateToolView(toolClass, toolBar == null);
		if (toolBar == null) {
			ToolBarInfo tbd = getToolBarDesc(toolClass);
			if (tbd == null) {
				System.err.println("ToolBar: " + toolClass.getSimpleName() + " not registered");
				return;
			}
			try {
				toolBar = toolClass.getConstructor().newInstance();
				if (!toolBar.instantiate(this, tbd.keyCode))
					return;
				if (toolBar instanceof ExternalTool) {
					Point2i toolPos = this.scenariumContainer.getDefaultToolBarLocation(toolClass.getSimpleName());
					if (toolPos != null)
						((ExternalTool) toolBar).setPosition(new Point2D(Math.max(toolPos.x, 0), Math.max(toolPos.y, 0)));
					((ExternalTool) toolBar).setAlwaysOnTop(this.scenariumContainer.isDefaultToolBarAlwaysOnTop(toolClass.getSimpleName()));
					((ExternalTool) toolBar).show();
				}
				if (toolBar instanceof VisuableSchedulable && this.animationTimerConsumer != null)
					this.animationTimerConsumer.register((VisuableSchedulable) toolBar);
				this.displayedTools.put(toolClass, toolBar);
				fireToolVisibleChanged(toolClass, true);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
				ex.printStackTrace();
			}
			if (toolBar instanceof InternalTool) {
				Region region = toolBar.getRegion();
				if (region == null)
					throw new IllegalArgumentException("getValue on the class : " + toolBar.getClass() + " return forbidden value null");
				if (!this.toolPane.isVisible())
					this.toolPane.setVisible(true);
				switch (((InternalTool) toolBar).getBorder()) {
				case CENTER:
					this.centerPane.getChildren().add(region);
					if (!this.centerPane.isVisible())
						this.centerPane.setVisible(true);
					this.internalToolParents.put((InternalTool) toolBar, region);
					break;
				case TOP:
					this.toolPane.setTop(region);
					break;
				case RIGHT:
					this.toolPane.setRight(region);
					break;
				case BOTTOM:
					this.toolPane.setBottom(region);
					break;
				case LEFT:
					this.toolPane.setLeft(region);
					break;
				default:
					break;
				}
			}
		} else {
			if (toolBar instanceof InternalTool) {
				switch (((InternalTool) toolBar).getBorder()) {
				case CENTER:
					Region internalToolParent = this.internalToolParents.get(toolBar);
					this.internalToolParents.remove(toolBar);
					ObservableList<Node> children = this.centerPane.getChildren();
					children.remove(internalToolParent);
					if (children.isEmpty())
						this.centerPane.setVisible(false);
					break;
				case TOP:
					this.toolPane.setTop(null);
					break;
				case RIGHT:
					this.toolPane.setRight(null);
					break;
				case BOTTOM:
					this.toolPane.setBottom(null);
					break;
				case LEFT:
					this.toolPane.setLeft(null);
					break;
				default:
					break;
				}
				if (this.toolPane.getChildren().size() == 1 && this.centerPane.getChildren().isEmpty() && this.toolPane.isVisible())
					this.toolPane.setVisible(false);
			}
			if (toolBar instanceof VisuableSchedulable && this.animationTimerConsumer != null)
				this.animationTimerConsumer.unRegister((VisuableSchedulable) toolBar);
			this.displayedTools.remove(toolClass);
			fireToolVisibleChanged(toolClass, false);
			toolBar.dispose();
		}
	}

	Tool getDisplayedTool(Class<? extends Tool> toolClass) {
		return this.displayedTools.get(toolClass);
	}

	@Override
	public void loaded(ToolBarInfo toolBarInfo) {}

	@Override
	public void unloaded(ToolBarInfo toolBarInfo) {
		closeTool(toolBarInfo.type);
	}
}
