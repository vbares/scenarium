/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Objects;

/******************************************************************************* Copyright (c) 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019 Revilloud Marc. All rights reserved. This
 * program and the accompanying materials are made available under the terms of the GNU Public License v3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
public class CSVData implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedHashMap<String, Integer> typeIndex = new LinkedHashMap<>();
	private int nbElements;
	private Object[] data;

	@SuppressWarnings("unchecked")
	@Override
	public CSVData clone() {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = (LinkedHashMap<String, Integer>) this.typeIndex.clone();
		newCvsData.nbElements = this.nbElements;
		if (this.data != null)
			newCvsData.data = this.data.clone();
		return newCvsData;
	}

	public CSVData cloneData() {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = this.typeIndex;
		newCvsData.nbElements = this.nbElements;
		if (this.data != null)
			newCvsData.data = this.data.clone();
		return newCvsData;
	}

	public CSVData cloneData(String... excludeFilter) {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = this.typeIndex;
		newCvsData.nbElements = this.nbElements;
		ArrayList<Integer> exclud = new ArrayList<>();
		for (String filter : excludeFilter)
			if (this.typeIndex.containsKey(filter))
				exclud.add(getIndexFromName(filter));
		if (this.data != null)
			newCvsData.data = this.data.clone();
		for (Integer id : exclud)
			newCvsData.data[id] = null;
		return newCvsData;
	}

	@Override
	public boolean equals(Object e) {
		if (!(e instanceof CSVData))
			return false;
		Object[] oData = ((CSVData) e).getData();
		for (int i = 0; i < this.data.length; i++)
			if (!(oData[i] == null && this.data[i] == null || oData[i].equals(this.data[i])))
				return false;
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.data);
	}

	public Object[] getData() {
		return this.data;
	}

	public Integer getIndexFromName(String name) {
		return this.typeIndex.get(name);
	}

	public String getNameFromIndex(int i) {
		for (String name : this.typeIndex.keySet())
			if (this.typeIndex.get(name) == i)
				return name;
		return null;
	}

	public int getNbElements() {
		return this.nbElements;
	}

	public void initStruct(LinkedHashMap<String, Integer> typeIndex) {
		this.typeIndex = typeIndex;
		this.nbElements = typeIndex.values().size();
	}

	public void setData(Object[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (String name : this.typeIndex.keySet()) {
			sb.append(name);
			sb.append("= ");
			sb.append(this.data[i++]);
			if (i >= this.data.length)
				break;
			sb.append("; ");
		}
		return sb.toString();
	}
}
