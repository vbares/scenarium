/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class CompB extends EvolvedOperator {
	static Integer res = Integer.valueOf(0);
	private static int nbIter = 1000000;
	private static int nbBlock = 5;

	int cpt = 0;

	private long time;

	@Override
	public void birth() {
		new Thread(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			triggerOutput(5);
		}).start();
	}

	@Override
	public void death() {

	}

	public Integer process(Integer a) {
		if (this.cpt == 0)
			this.time = System.currentTimeMillis();
		this.cpt++;
		if (this.cpt == nbIter)
			System.out.println((System.currentTimeMillis() - this.time) * 1000.0 / nbIter / nbBlock + "us");
		return res;
	}
}
