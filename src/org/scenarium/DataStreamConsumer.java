/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import org.scenarium.filemanager.datastream.DataStreamManager;
import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.filemanager.datastream.output.DataFlowOutputStream;

public class DataStreamConsumer extends EditorConsumer {
	public <T> void accept(Class<T> type, Class<? extends DataFlowInputStream<T>> inputStream, Class<? extends DataFlowOutputStream<T>> outputStream) {
		DataStreamManager.registerDataStream(type, inputStream, outputStream);
	}
}
