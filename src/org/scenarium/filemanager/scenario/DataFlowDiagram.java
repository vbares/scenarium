/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.vecmath.Point2i;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.BeanUnregisterListener;
import org.beanmanager.LoadModuleListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.basic.EnumEditor;
import org.beanmanager.editors.basic.InetSocketAdressEditor;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.tools.Tuple;
import org.scenarium.ModuleManager;
import org.scenarium.display.AnimationTimerConsumer;
import org.scenarium.display.RenderPane;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.VisuableSchedulableContainer;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.IllegalInputArgument;
import org.scenarium.filemanager.scenario.dataflowdiagram.Input;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.ModificationType;
import org.scenarium.filemanager.scenario.dataflowdiagram.Output;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.ReaderType;
import org.scenarium.filemanager.scenario.dataflowdiagram.RunningMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.VisuableSchedulableRemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.MonoCoreDiagramScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.MultiCoreDiagramScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.NoThreadedDiagramScheduler;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.MetaScenario;
import org.scenarium.filemanager.scenariomanager.PeriodListener;
import org.scenarium.filemanager.scenariomanager.RecordingListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.SourceChangeListener;
import org.scenarium.filemanager.scenariomanager.StartStopChangeListener;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.timescheduler.Schedulable;
import org.scenarium.timescheduler.ScheduleTask;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.SchedulerInterface;
import org.scenarium.timescheduler.SchedulerPropertyChangeListener;
import org.scenarium.timescheduler.SchedulerState;
import org.scenarium.timescheduler.TriggerMode;
import org.scenarium.timescheduler.VisuableSchedulable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class DataFlowDiagram extends LocalScenario implements FlowDiagramChangeListener, SourceChangeListener, PeriodListener, BeanUnregisterListener, BeanRenameListener, VisuableSchedulableContainer,
		SchedulerPropertyChangeListener, PropertyChangeListener, StartStopChangeListener, MetaScenario, RecordingListener, LoadModuleListener {
	private static BufferedImage buffImage;
	private long nbGap;
	private ArrayList<Block> schedulableBlocks = new ArrayList<>();
	@PropertyInfo(index = 1, nullable = false, info = "Specify the running mode of the diagram.\nNoThreaded: Run the diagram without any specific thread, to avoid for main diagram.\nMonoCore: Run the diagram in a single thread.\nMultiCore: Each block run in its own thread.")
	private RunningMode runningMode = RunningMode.MULTICORE;
	@PropertyInfo(index = 2, nullable = false, info = "Specify if the diagram is scheduled with the time stamp of record or with the time of issue.\nTimeStamp: replay the record based on the time when the data is available by recorder.\nTimeOfIssue: replay the record based on the time stamp of the data received by the recorder.")
	private TriggerMode triggerMode = TriggerMode.TIMEOFISSUE;
	private DiagramScheduler diagramScheduler;
	private AnimationTimerConsumer animationTimerConsumer;

	private SchedulerInterface oldSchedulerInterface;

	private final LinkedHashMap<Object, Stage> visibleSubPanes = new LinkedHashMap<>();

	private CountDownLatch processModeCountDownLatch;
	private double periodFromSubScenario;

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		if (OperatorManager.isOperator(beanDesc.bean))
			((FlowDiagram) scenarioData).beanRename(beanDesc.bean);
	}

	@Override
	public void beanUnregister(BeanDesc<?> beanDesc, boolean delete) {
		if (OperatorManager.isOperator(beanDesc.bean)) {
			FlowDiagram fd = (FlowDiagram) scenarioData;
			for (Block block : fd.getBlocks())
				if (block.operator == beanDesc.bean) {
					ArrayList<Object> h = new ArrayList<>();
					h.add(block);
					fd.removeElements(h, false, false);
					break;
				}
		}
	}

	@Override
	public boolean canCreateDefault() {
		return true;
	}

	public synchronized boolean close() {
		if (schedulerInterface != null) {
			schedulerInterface.stop(); // stop vers stopAndWait, j'attend la fin avant de continuer
			schedulerInterface.removePropertyChangeListener(this);
		}
		if (this.diagramScheduler != null) {
			this.diagramScheduler.stop();
			this.diagramScheduler = null;
		}
		if (scenarioData != null) {
			((FlowDiagram) scenarioData).removeFlowDiagramChangeListener(this);
			((FlowDiagram) scenarioData).removeRecordingListener(this);
			((FlowDiagram) scenarioData).death();
		}
		scenarioData = null;
		BeanEditor.removeStrongRefBeanUnregisterListener(this);
		BeanEditor.removeStrongRefBeanRenameListener(this);
		ModuleManager.removeLoadModuleListener(this);
		return true;
	}

	private static void createBufferedImage(int width, int height) {
		if (buffImage == null || buffImage.getWidth() != width || buffImage.getHeight() != height) {
			buffImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g = buffImage.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(new Color(0, 0, 0, 20));
			int roundRect = width / 8;
			g.fillRoundRect(width / 10, height / 20, 8 * width / 10, 9 * height / 10, roundRect * 2, roundRect * 2);
			int rectWidth = 3 * width / 15;
			int rectHeight = 4 * width / 15;
			g.setColor(new Color(112, 200, 216, 80));
			g.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.drawLine(3 * width / 15 + rectWidth, 2 * height / 15 + rectHeight / 2, 8 * width / 15, 3 * height / 15 + rectHeight / 2);
			g.drawLine(3 * width / 15 + rectWidth / 2, 2 * height / 15 + rectHeight, 4 * width / 15 + rectWidth / 2, 8 * height / 15);
			g.drawLine(8 * width / 15 + rectWidth / 2, 3 * height / 15 + rectHeight, 9 * width / 15 + rectWidth / 2, 9 * height / 15);
			g.drawLine(4 * width / 15 + rectWidth, 8 * height / 15 + rectHeight / 2, 9 * width / 15, 9 * height / 15 + rectHeight / 2);
			g.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.setColor(new Color(48, 29, 7, 80));
			g.drawRoundRect(3 * width / 15, 2 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(8 * width / 15, 3 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(4 * width / 15, 8 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(9 * width / 15, 9 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.setColor(new Color(112, 200, 216, 80));
			g.fillRoundRect(3 * width / 15, 2 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(8 * width / 15, 3 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(4 * width / 15, 8 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(9 * width / 15, 9 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
		}
	}

	private static boolean createLink(FlowDiagram fd, String iName, String iOp, String oName, String oOp) {
		Input lInput = null;
		if (iOp.equals("i")) {
			for (FlowDiagramInput fdInput : fd.getInputs())
				if (fdInput.getName().equals(iName)) {
					lInput = fdInput;
					break;
				}
		} else
			for (Block block : fd.getBlocks()) {
				Object op = block.getOperator();
				if (op != null)
					if (BeanEditor.getBeanDesc(op).getSimpleDescriptor().equals(iOp))
						for (BlockInput input : block.getInputs())
							if (input.getName().equals(iName)) {
								lInput = input;
								break;
							}
				if (lInput != null)
					break;
			}
		if (lInput == null)
			return false;
		Output lOutput = null;
		if (oOp.equals("o")) {
			if (lInput instanceof FlowDiagramInput)
				return true;
			for (FlowDiagramOutput fdOutput : fd.getOutputs())
				if (fdOutput.getName().equals(oName)) {
					lOutput = fdOutput;
					break;
				}
		} else
			for (Block block : fd.getBlocks())
				if (BeanEditor.getBeanDesc(block.getOperator()).getSimpleDescriptor().equals(oOp)) {
					for (BlockOutput output : block.getOutputs())
						if (output.getName().equals(oName)) {
							lOutput = output;
							break;
						}
					if (lOutput != null)
						break;
				}
		if (lOutput == null)
			return false;
		// if (lInput instanceof FlowDiagramInput) //Inutile car fait si setLink
		// ((FlowDiagramInput) lInput).setType(lOutput.getType());
		lInput.setLink(new Link(lInput, lOutput));
		return true;
	}

	public void flowDiagramChanged(Object element, String property, ModificationType modificationType) {
		updateDiagramInfo(element, modificationType);
	}

	@Override
	public long getBeginningTime() {
		long minBeginTime = -1;
		for (Block block : this.schedulableBlocks) {
			Object op = block.operator;
			if (op instanceof Scenario) {
				long beginTime = ((Scenario) op).getBeginningTime();
				if (beginTime >= 0 && (minBeginTime == -1 || beginTime < minBeginTime))
					minBeginTime = beginTime;
			}
		}
		return minBeginTime;
	}

	@Override
	public Class<?> getDataType() {
		return DataFlowDiagram.class;
	}

	@Override
	public long getEndTime() {
		long maxEndTime = -1;
		for (Block block : this.schedulableBlocks) {
			Object op = block.operator;
			if (op instanceof Scenario) {
				long endTime = ((Scenario) op).getEndTime();
				if (endTime > maxEndTime)
					maxEndTime = endTime;
			}
		}
		return maxEndTime;
	}

	public long getNbGap() {
		return this.nbGap;
	}

	@Override
	public Region getNode() {
		ImageView iv = new ImageView();
		StackPane sp = new StackPane(iv);
		sp.setPickOnBounds(false);
		iv.setOnMouseClicked(e -> {
			if (e.isConsumed())
				return;

			if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
				e.consume();
				Stage subPane = this.visibleSubPanes.get(scenarioData);
				if (subPane != null) {
					subPane.requestFocus();
					return;
				}
				class ViewSubPane {
					RenderPane renderPane;

					RenderPane show() {
						this.renderPane = new RenderPane((Scheduler) null, getScenarioData(), new ScenariumContainer() {

							@Override
							public void adaptSizeToDrawableElement() {
								ViewSubPane.this.renderPane.adaptSizeToDrawableElement();
							}

							@Override
							public Point2i getDefaultToolBarLocation(String simpleName) {
								return null;
							}

							@Override
							public Scheduler getScheduler() {
								return null;
							}

							@Override
							public int getSelectedElementFromTheaterEditor() {
								return -1;
							}

							@Override
							public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
								return false;
							}

							@Override
							public boolean isManagingAccelerator() {
								return false;
							}

							@Override
							public boolean isStatusBar() {
								return ViewSubPane.this.renderPane.isStatusBar();
							}

							@Override
							public void saveScenario() {

							}

							@Override
							public void showMessage(String message, boolean error) {
								ViewSubPane.this.renderPane.showMessage(message, error);
							}

							@Override
							public void updateStatusBar(String... infos) {
								ViewSubPane.this.renderPane.updateStatusBar(infos);
							}

							@Override
							public void showTool(Class<? extends Tool> toolClass) {}

							@Override
							public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}
						}, null, false, true);
						return this.renderPane;
					}
				}
				RenderPane renderPane = new ViewSubPane().show();
				Stage stage = new Stage();
				stage.setTitle(toString());
				renderPane.getTheaterPane().ignoreRepaint = false;
				StackPane pane = renderPane.getPane();
				pane.setOnKeyPressed(e1 -> {
					if (e1.isControlDown() && e1.getCode() == KeyCode.S) {
						try {
							System.out.println("save: " + file);
							save(file);
							initStruct();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
						e.consume();
					}
				});
				renderPane.getTheaterPane().prefWidthProperty().bind(pane.widthProperty());
				renderPane.getTheaterPane().prefHeightProperty().bind(pane.heightProperty());
				Scene scene = new Scene(pane, -1, -1);
				stage.setScene(scene);
				stage.sizeToScene();
				stage.setOnCloseRequest(e1 -> this.visibleSubPanes.remove(scenarioData));
				this.visibleSubPanes.put(scenarioData, stage);
				stage.show();
			}
		});
		InvalidationListener il = e -> updateImage(sp, iv);
		sp.heightProperty().addListener(il);
		sp.widthProperty().addListener(il);
		return sp;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "pdfd" };
	}

	public RunningMode getRunningMode() {
		return this.runningMode;
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.EVENT_SCHEDULER;
		// if (schedulableBlocks.size() == 0)
		// return Scheduler.STREAMSCHEDULER;
		// for (Block block : schedulableBlocks) {
		// Object op = block.getOperator();
		// if (op instanceof Scenario && ((Scenario) op).getSchedulerType() == Scheduler.EVENTSCHEDULER)
		// return Scheduler.EVENTSCHEDULER;
		// if (op instanceof Scenario && ((Scenario) op).getSchedulerType() == Scheduler.STREAMSCHEDULER)
		// return Scheduler.STREAMSCHEDULER;
		// }
		// return Scheduler.TIMERSCHEDULER;
	}

	public TriggerMode getTriggerMode() {
		return this.triggerMode;
	}

	public void initOrdo() {
		if (this.oldSchedulerInterface != null)
			this.oldSchedulerInterface.removePropertyChangeListener(this);
		schedulerInterface.addPropertyChangeListener(this);
		this.oldSchedulerInterface = schedulerInterface;
		schedulerInterface.setTriggerMode(this.triggerMode);
		int colorIndex = 0;
		for (Block block : this.schedulableBlocks) {
			Object op = block.operator;
			if (op instanceof Scenario) {
				SchedulerInterface schedulerInterface = this.schedulerInterface;
				if (op instanceof TimedScenario) {
					((TimedScenario) op).addPeriodListenerIfNotPresent(this);
					((TimedScenario) op).addStartStopChangeListener(this);
					block.addPropertyChangeListenerIfNotPresent(this);
					schedulerInterface = block.getProcessMode() == ProcessMode.LOCAL ? this.schedulerInterface
							: this.schedulerInterface.derivateAsRemote(block.getProcessMode() != ProcessMode.LOCAL ? block : null);
					((LocalScenario) op).setColorIndex(colorIndex++);
					if (colorIndex == COLORS.length)
						colorIndex = 0;
				}
				((Scenario) op).setScheduler(schedulerInterface);
				((Scenario) op).addSourceChangeListenerIfNotPresent(this);
			}
		}
		for (Block block : this.schedulableBlocks) {
			Object op = block.operator;
			if (op instanceof TimedScenario && ((TimedScenario) op).getSchedulerType() == Scheduler.TIMER_SCHEDULER) {
				TimedScenario ls = (TimedScenario) op;
				double lsPeriod = ls.getPeriod();
				int nbFrame = (int) ls.getNbFrame();
				if (lsPeriod > 0 && nbFrame > 0) {
					ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>(nbFrame);
					long beginTime = schedulerInterface.getBeginTime();
					for (int i = 0; i < nbFrame; i++) {
						long time = beginTime + (long) (i * lsPeriod);
						schedulableTasks.add(new ScheduleTask(time, time, ls, time));
					}
					schedulerInterface.addTasks(schedulableTasks, null);
				}
			}
		}
		startStopChanged();
	}

	@Override
	public void initStruct() throws IOException {
		if (scenarioData == null)
			try {
				load(getFile(), false);
			} catch (ScenarioException e) {}
		if (scenarioData == null)
			return;
		FlowDiagram fd = (FlowDiagram) scenarioData;
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		for (FlowDiagramInput fdInput : fd.getInputs())
			if (fdInput.getType() != null) {
				names.add(fdInput.getName());
				types.add(fdInput.getType());
			}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
		names = new ArrayList<>();
		types = new ArrayList<>();

		for (FlowDiagramOutput fdOutput : fd.getOutputs()) {
			names.add(fdOutput.getName());
			if (fdOutput.getType() == null)
				System.err.println("peu pas être null!!!!!");
			types.add(fdOutput.getType());
		}
		updateInputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	// boolean stopping = false;

	@Override
	public boolean isPaintableProperties() {
		return true;
	}

	// @Override
	// public boolean isTimeRepresentation() {
	// return true;
	// }

	public synchronized void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		System.out.println("DataFlowDiagram load: " + scenarioFile);
		// Charge le diagramme
		if (backgroundLoading) {
			SimpleDoubleProperty sdp = new SimpleDoubleProperty();
			sdp.setValue(0.5);
			progressProperty = sdp;
			new Thread(() -> loadImmediately(scenarioFile)).start();
		} else
			loadImmediately(scenarioFile);
	}

	private void loadImmediately(File scenarioFile) {
		if (scenarioData != null) {
			fireLoadChanged();
			return;
		}
		FlowDiagram fd = new FlowDiagram();
		if (scenarioFile != null)
			try {
				Element racine = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(scenarioFile).getDocumentElement();
				String diagramRunningModeName = racine.getAttribute("rm");
				if (!diagramRunningModeName.isEmpty()) {
					EnumEditor<RunningMode> rm = new EnumEditor<>(RunningMode.class);
					rm.setAsText(diagramRunningModeName);
					RunningMode value = rm.getValue();
					if (value != null)
						this.runningMode = value;
				}
				String diagramTriggerModeName = racine.getAttribute("tm");
				if (!diagramTriggerModeName.isEmpty()) {
					EnumEditor<TriggerMode> rm = new EnumEditor<>(TriggerMode.class);
					rm.setAsText(diagramTriggerModeName);
					TriggerMode value = rm.getValue();
					if (value != null)
						this.triggerMode = value;
				}
				ArrayList<Tuple<Block, String>> missedInputs = new ArrayList<>();
				HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap = new HashMap<>();
				// Load and create blocks
				Node blocksElement = racine.getElementsByTagName("Blocks").item(0);
				if (blocksElement != null) {
					NodeList bsNodeList = blocksElement.getChildNodes();
					for (int i = 0; i < bsNodeList.getLength(); i++) {
						Node item = bsNodeList.item(i);
						if (item instanceof Element) {
							Block block = getBlockFromElement((Element) item, missedInputs, varArgsInputChangeListenerBlockMap);
							if (block != null)
								fd.addBlock(block);
						}
					}
				}
				// Load and create flow diagram inputs
				Node inputsElement = racine.getElementsByTagName("Inputs").item(0);
				if (inputsElement != null) {
					NodeList bsNodeList = inputsElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element) {
							int x = (int) Double.parseDouble(((Element) item).getAttribute("x"));
							int y = (int) Double.parseDouble(((Element) item).getAttribute("y"));
							String name = ((Element) item).getAttribute("n");
							fd.addInput(new FlowDiagramInput(fd, null, name, new Point2D(x, y)));
						}
					}
				}
				// Load and create flow diagram outputs
				Node outputsElement = racine.getElementsByTagName("Outputs").item(0);
				if (outputsElement != null) {
					NodeList bsNodeList = outputsElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element) {
							int x = (int) Double.parseDouble(((Element) item).getAttribute("x"));
							int y = (int) Double.parseDouble(((Element) item).getAttribute("y"));
							String name = ((Element) item).getAttribute("n");
							fd.addOutput(new FlowDiagramOutput(null, name, new Point2D(x, y)));
						}
					}
				}
				// Load and create all links
				Node linksElement = racine.getElementsByTagName("Links").item(0);
				ArrayList<String[]> missedLink = new ArrayList<>();
				if (linksElement != null) {
					NodeList bsNodeList = linksElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element)
							createLinkFromElement(fd, (Element) item, missedLink);
					}
				}
				setVarArgsLinksAndCreatePropertyInput(fd, missedLink, varArgsInputChangeListenerBlockMap, missedInputs);
			} catch (SAXException | ParserConfigurationException e) {} catch (IOException ex) {
				ex.printStackTrace();
			}
		fd.addFlowDiagramChangeListener(this);
		fd.addRecordingListener(this);
		BeanEditor.addStrongRefBeanUnregisterListener(this);
		BeanEditor.addStrongRefBeanRenameListener(this);
		ModuleManager.addLoadModuleListener(this);
		this.file = scenarioFile;
		scenarioData = fd;
		updateDiagramInfo(null, ModificationType.CHANGE); // Je recharge le diagram encore une fois sinon..., mais je rajoute à schedulableBlocks et donc initOrdo père et donc scheduler
		if (progressProperty != null)
			((SimpleDoubleProperty) progressProperty).set(1);
		fireLoadChanged();
		fireRecordingChanged(isRecording());
	}

	private static void setVarArgsLinksAndCreatePropertyInput(FlowDiagram fd, ArrayList<String[]> missedLink, HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap,
			ArrayList<Tuple<Block, String>> missedInputs) {
		// Set links for varargs
		if (!missedLink.isEmpty()) {
			int oldSize;
			do {
				oldSize = missedLink.size();
				for (Iterator<String[]> iterator = missedLink.iterator(); iterator.hasNext();) {
					String[] att = iterator.next();
					if (createLink(fd, att[0], att[1], att[2], att[3]))
						iterator.remove();
				}
			} while (oldSize != missedLink.size());
		}
		varArgsInputChangeListenerBlockMap.forEach((block, listenList) -> listenList.forEach(listener -> block.removeVarArgsInputChangeListener(listener))); // Remove all listener even if
		// Create all inputs of type: property as input
		for (Tuple<Block, String> blockAndInputName : missedInputs)
			if (blockAndInputName.getFirst().operator instanceof EvolvedOperator) {
				if (!((EvolvedOperator) blockAndInputName.getFirst().operator).setPropertyAsInput(blockAndInputName.getSecond(), true))
					System.err.println("cannot set the input: " + blockAndInputName.getSecond() + " of the block: " + blockAndInputName.getFirst());
			} else if (!blockAndInputName.getFirst().setPropertyAsInput(blockAndInputName.getSecond(), true))
				System.err.println("cannot set the input: " + blockAndInputName.getSecond() + " of the block: " + blockAndInputName.getFirst());
		// else // Useless, already done with the StructChangeListener of FlowDiagram::addBlock
		// blockAndInputName.getFirst().getInputs().get(blockAndInputName.getFirst().getNbPropertyInput() - 1).addLinkChangeListener(fd);
		// Set links for newly created property as input
		if (!missedLink.isEmpty())
			for (Iterator<String[]> iterator = missedLink.iterator(); iterator.hasNext();) {
				String[] att = iterator.next();
				if (createLink(fd, att[0], att[1], att[2], att[3]))
					iterator.remove();
			}
		for (String[] att : missedLink)
			System.err.println("cannot set the link: " + att[3] + ": " + att[2] + " -> " + att[1] + ": " + att[0]);

	}

	private static void createLinkFromElement(FlowDiagram fd, Element item, ArrayList<String[]> missedLink) {
		String iName = item.getAttribute("in");
		String iOp = item.getAttribute("io");
		String oName = item.getAttribute("on");
		String oOp = item.getAttribute("oo");
		if (!createLink(fd, iName, iOp, oName, oOp))
			missedLink.add(new String[] { iName, iOp, oName, oOp });

	}

	private static Block getBlockFromElement(Element element, ArrayList<Tuple<Block, String>> missedInputs, HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap) {
		Object operator = null;
		String operatorName = element.getAttribute("o");
		try {
			String operatorTypeName = operatorName.substring(0, operatorName.indexOf(BeanDesc.SEPARATOR));
			for (Class<?> operatorType : OperatorManager.getInternOperators())
				if (BeanManager.getDescriptorFromClassWithSimpleName(operatorType).equals(operatorTypeName)) {
					try {
						operatorName = operatorName.substring(operatorTypeName.length() + 1);
						BeanDesc<?> regBeanDesc = BeanEditor.getRegisterBean(operatorType, operatorName);
						if (regBeanDesc == null) {
							operator = operatorType.getConstructor().newInstance();
							BeanManager beanManager = new BeanManager(operator, BeanManager.defaultDir);
							BeanEditor.registerBean(operator, operatorName, BeanManager.defaultDir); // Les fils n'existe pas encore...
							if (!beanManager.load()) {
								BeanEditor.unregisterBean(operator, true);
								operator = null;
							}
						} else
							operator = regBeanDesc.bean;
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
						if (e instanceof IllegalArgumentException)
							System.err.println(e.getMessage());
						operator = null;
					}
					break;
				}
			if (operator == null)
				System.err.println("Cannot load the operator: " + operatorName);
		} catch (StringIndexOutOfBoundsException e) {
			System.err.println("Error during initialisation of operator: " + operatorName + "Error: " + e.getMessage());
		}
		if (operator != null) {
			int x = (int) Double.parseDouble(element.getAttribute("x"));
			int y = (int) Double.parseDouble(element.getAttribute("y"));
			try {
				Block block = new Block(operator, new Point2D(x, y), false);
				String a = element.getAttribute("enable");
				if (!a.isEmpty())
					block.setEnable(Boolean.parseBoolean(a));
				a = element.getAttribute("waitAllDataConsumedBeforeDying");
				if (!a.isEmpty())
					block.setConsumesAllDataBeforeDying(Boolean.parseBoolean(a));
				a = element.getAttribute("startPriority");
				if (!a.isEmpty())
					block.setStartPriority(Integer.parseInt(a));
				a = element.getAttribute("stopPriority");
				if (!a.isEmpty())
					block.setStopPriority(Integer.parseInt(a));
				a = element.getAttribute("threadPriority");
				if (!a.isEmpty())
					block.setThreadPriority(Integer.parseInt(a));
				a = element.getAttribute("processMode");
				if (!a.isEmpty())
					block.setProcessMode(ProcessMode.valueOf(a));
				a = element.getAttribute("isolatePort");
				if (!a.isEmpty())
					block.setIsolatePort(Integer.parseInt(a));
				a = element.getAttribute("synchronizeProperties");
				if (!a.isEmpty())
					block.setSynchronizeProperties(Boolean.parseBoolean(a));
				a = element.getAttribute("remoteIp");
				if (!a.isEmpty()) {
					InetSocketAdressEditor isae = new InetSocketAdressEditor();
					isae.setAsText(a);
					block.setRemoteIp(isae.getValue());
				}
				a = element.getAttribute("remoteProperty");
				if (!a.isEmpty())
					block.setRemoteProperty(Boolean.parseBoolean(a));
				// fd.addBlock(block);
				NodeList iNodeList = element.getChildNodes();
				for (int j = 0; j < iNodeList.getLength(); j++) {
					Node iItem = iNodeList.item(j);
					if (iItem instanceof Element) {
						String inputName = ((Element) iItem).getAttribute("n");
						boolean success = false;
						for (BlockInput input : block.getInputs())
							if (input.getName().equals(inputName)) {
								EnumEditor<ReaderType> ee = new EnumEditor<>(ReaderType.class);
								ee.setAsText(((Element) iItem).getAttribute("t"));
								input.setReaderType(ee.getValue());
								input.setMaxStackSize(Integer.parseInt(((Element) iItem).getAttribute("s")));
								success = true;
								break;
							}
						if (!success) { // VarArgs and property inputs cannot be set now, delayed after blocks loaded
							Tuple<Block, String> blockAndInputName = new Tuple<>(block, inputName);
							missedInputs.add(blockAndInputName);
							ArrayList<VarArgsInputChangeListener> varArgsInputChangeListenerList = varArgsInputChangeListenerBlockMap.get(block);
							if (varArgsInputChangeListenerList == null) { // If there is at least two VarArgs or property inputs that failed to load
								varArgsInputChangeListenerList = new ArrayList<>();
								varArgsInputChangeListenerBlockMap.put(block, varArgsInputChangeListenerList);
							}
							if (block.getNbInput() != 0 && block.getInputs().get(block.getNbInput() - 1).isVarArgs()) {
								ArrayList<Tuple<Block, String>> finalMissedInput = missedInputs;
								ArrayList<VarArgsInputChangeListener> finalVarArgsInputChangeListenerList = varArgsInputChangeListenerList;
								int listenIndex = varArgsInputChangeListenerList.size(); // index of the last one, the current one
								VarArgsInputChangeListener l = (index, type) -> { // Add listener witch load properties of input when created
									if (type == VarArgsInputChangeListener.NEW) {
										BlockInput input = block.getInputs().get(index);
										if (input.getName().equals(inputName)) {
											EnumEditor<ReaderType> ee = new EnumEditor<>(ReaderType.class);
											ee.setAsText(((Element) iItem).getAttribute("t"));
											input.setReaderType(ee.getValue());
											input.setMaxStackSize(Integer.parseInt(((Element) iItem).getAttribute("s")));
											finalMissedInput.remove(blockAndInputName);
											// Remove Listener when properties set, remove from list because cannot make reference to l
											block.removeVarArgsInputChangeListener(finalVarArgsInputChangeListenerList.get(listenIndex));
											return;
										}
									}
								};
								block.addVarArgsInputChangeListener(l);
								varArgsInputChangeListenerList.add(l);
							}
						}
					}
				}
				return block;
			} catch (IllegalInputArgument e) {
				System.err.println(e.getMessage());
			}
		}
		return null;
	}

	private void firePeriodChanged() {
		for (PeriodListener listener : listeners.getListeners(PeriodListener.class))
			listener.periodChanged(this.periodFromSubScenario);
	}

	@Override
	public void periodChanged(double period) {
		this.periodFromSubScenario = period;
		firePeriodChanged();
		schedulerInterface.clean();
		initOrdo();
	}

	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		if (scenarioData == null)
			try {
				load(getFile(), false);
			} catch (ScenarioException e) {}
		info.put("Number of frames", Long.toString(this.nbGap));
		info.put("Number of frames", Long.toString(this.nbGap));
		info.put("Schedulable block", Integer.toString(this.schedulableBlocks.size()));
	}

	@Override
	public void process(Long timePointer) throws Exception {
		Object[] addInputs = getAdditionalInputs();
		long[] ts = new long[addInputs.length];
		for (int i = 0; i < ts.length; i++)
			ts[i] = getTimeStamp(i);
		this.diagramScheduler.triggerOutput(scenarioData, addInputs, ts);
		System.out.println("process: " + this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("processMode")) {
			CountDownLatch processModeCountDownLatch = this.processModeCountDownLatch;
			if (processModeCountDownLatch != null)
				try {
					processModeCountDownLatch.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			this.processModeCountDownLatch = new CountDownLatch(1);
			new Thread(() -> {
				sourceChanged();
				this.processModeCountDownLatch.countDown();
			}).start();
		}
	}

	private void registerVisuableSchedulable(Block block) {
		this.animationTimerConsumer.register(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	public void save(File file) throws IOException {
		FlowDiagram fd = (FlowDiagram) scenarioData;
		List<Block> blocks = fd.getBlocks();
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element racine = document.createElement(FlowDiagram.class.getSimpleName());
			racine.setAttribute("rm", this.runningMode.name());
			racine.setAttribute("tm", this.triggerMode.name());
			Element blocksElement = document.createElement("Blocks");
			for (int i = 0; i < blocks.size(); i++)
				blocksElement.appendChild(getElementFromBlock(blocks.get(i), document, i));
			racine.appendChild(blocksElement);
			Element linksElement = document.createElement("Links");
			ArrayList<Link> links = fd.getAllLinks();
			int k = 0;
			for (int i = 0; i < links.size(); i++)
				linksElement.appendChild(getElementFromLink(links.get(i), document, i));
			racine.appendChild(linksElement);

			Element inputsElement = document.createElement("Inputs");
			for (FlowDiagramInput fdInput : fd.getInputs()) {
				Element iElement = document.createElement("i" + k++);
				Point2D pos = fdInput.getPosition();
				iElement.setAttribute("x", Integer.toString((int) pos.getX()));
				iElement.setAttribute("y", Integer.toString((int) pos.getY()));
				iElement.setAttribute("n", fdInput.getName());
				inputsElement.appendChild(iElement);
			}
			racine.appendChild(inputsElement);

			Element outputsElement = document.createElement("Outputs");
			for (FlowDiagramOutput fdOutput : fd.getOutputs()) {
				Element oElement = document.createElement("i" + k++);
				Point2D pos = fdOutput.getPosition();
				oElement.setAttribute("x", Integer.toString((int) pos.getX()));
				oElement.setAttribute("y", Integer.toString((int) pos.getY()));
				oElement.setAttribute("n", fdOutput.getName());
				outputsElement.appendChild(oElement);
			}
			racine.appendChild(outputsElement);
			document.appendChild(racine);
			try (FileOutputStream os = new FileOutputStream(file)) {
				TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document), new StreamResult(os));
			}
		} catch (ParserConfigurationException | TransformerException | TransformerFactoryConfigurationError e) {
			System.err.println("error during saving data flow diagram");
		}
		// Save all blocks
		HashSet<Object> visitedOperators = new HashSet<>();
		for (Block block : blocks) {
			Object op = block.operator;
			if (!visitedOperators.contains(op) && op instanceof EvolvedOperator) {
				RemoteOperator remoteOp = block.getRemoteOperator();
				try {
					if (remoteOp != null ? remoteOp.needToBeSaved() : ((EvolvedOperator) op).needToBeSaved())
						new BeanManager(op, BeanManager.defaultDir).save(true);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				visitedOperators.add(op);
			}
		}
	}

	private static Element getElementFromBlock(Block block, Document document, int i) {
		Element bElement = document.createElement("b" + i);
		bElement.setAttribute("o", BeanEditor.getBeanDesc(block.getOperator()).getSimpleDescriptor());
		Rectangle2D rect = block.getRectangle();
		bElement.setAttribute("x", Integer.toString((int) rect.getMinX()));
		bElement.setAttribute("y", Integer.toString((int) rect.getMinY()));
		if (!block.isEnable())
			bElement.setAttribute("enable", Boolean.toString(block.isEnable()));
		if (block.isConsumesAllDataBeforeDying())
			bElement.setAttribute("waitAllDataConsumedBeforeDying", Boolean.toString(block.isConsumesAllDataBeforeDying()));
		if (block.getStartPriority() != 0)
			bElement.setAttribute("startPriority", Integer.toString(block.getStartPriority()));
		if (block.getStopPriority() != 0)
			bElement.setAttribute("stopPriority", Integer.toString(block.getStopPriority()));
		if (block.getThreadPriority() != Thread.NORM_PRIORITY)
			bElement.setAttribute("threadPriority", Integer.toString(block.getThreadPriority()));
		if (block.getProcessMode() != ProcessMode.LOCAL)
			bElement.setAttribute("processMode", block.getProcessMode().toString());
		if (block.getIsolatePort() != 22)
			bElement.setAttribute("isolatePort", Integer.toString(block.getIsolatePort()));
		if (!block.isSynchronizeProperties())
			bElement.setAttribute("synchronizeProperties", Boolean.toString(block.isSynchronizeProperties()));
		if (block.getRemoteIp() != null && !block.getRemoteIp().equals(new InetSocketAddress(0))) {
			InetSocketAdressEditor isae = new InetSocketAdressEditor();
			isae.setValue(block.getRemoteIp());
			bElement.setAttribute("remoteIp", isae.getAsText());
		}
		if (block.isRemoteProperty())
			bElement.setAttribute("remoteProperty", Boolean.toString(block.isRemoteProperty()));
		List<BlockInput> inputs = block.getInputs();
		for (int j = 0; j < inputs.size(); j++) {
			BlockInput input = inputs.get(j);
			Element iElement = document.createElement("i" + j);
			iElement.setAttribute("n", input.getName());
			int indexOfInput = input.getBlock().getInputIndex(input.getName());
			iElement.setAttribute("p", Boolean.toString(indexOfInput < input.getBlock().getNbPropertyInput()));
			ReaderType rt = input.getReaderType();
			EnumEditor<ReaderType> ee = new EnumEditor<>(ReaderType.class);
			ee.setValue(rt);
			iElement.setAttribute("t", ee.getAsText());
			iElement.setAttribute("s", Integer.toString(input.getMaxStackSize()));
			bElement.appendChild(iElement);
		}
		return bElement;
	}

	private static Element getElementFromLink(Link link, Document document, int i) {
		Element lElement = document.createElement("l" + i);
		Input input = link.getInput();
		lElement.setAttribute("io", input instanceof BlockInput ? BeanEditor.getBeanDesc(((BlockInput) input).getBlock().operator).getSimpleDescriptor() : "i");
		lElement.setAttribute("in", input.getName());
		Output output = link.getOutput();
		lElement.setAttribute("oo", output instanceof BlockOutput ? BeanEditor.getBeanDesc(((BlockOutput) output).getBlock().operator).getSimpleDescriptor() : "o");
		lElement.setAttribute("on", output.getName());
		return lElement;
	}

	@Override
	public void setAnimated(boolean animated, AnimationTimerConsumer animationTimerConsumer) {
		this.animationTimerConsumer = animationTimerConsumer;
		if (animated) {
			FlowDiagram fd = (FlowDiagram) scenarioData;
			for (Link link : fd.getAllLinks()) {
				Input input = link.getInput();
				if (input instanceof BlockInput)
					if (((BlockInput) input).getBlock().operator instanceof VisuableSchedulable)
						registerVisuableSchedulable(((BlockInput) input).getBlock());
			}
		}
	}

	public void setRunningMode(RunningMode runningMode) {
		this.runningMode = runningMode;
		stopIfWanted(() -> schedulerInterface.stop());
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		this.triggerMode = triggerMode;
		stopIfWanted(() -> sourceChanged());
	}

	private void stopIfWanted(Runnable runnable) {
		if (schedulerInterface != null && schedulerInterface.isRunning()) {
			Alert alert = new Alert(AlertType.CONFIRMATION, "", new ButtonType("Yes", ButtonData.YES), new ButtonType("No", ButtonData.NO));
			((Stage) alert.getDialogPane().getScene().getWindow()).setAlwaysOnTop(true);
			alert.setTitle("Scheduler stop");
			alert.setHeaderText("Do you want to stop the diagram to take into account the modification?");
			if (alert.showAndWait().get().getButtonData() == ButtonData.YES)
				new Thread(new Task<>() {
					@Override
					protected Boolean call() throws Exception {
						runnable.run();
						return true;
					}
				}).start();
		}
	}

	@Override
	public void sourceChanged() {
		if (schedulerInterface == null)
			return;
		schedulerInterface.clean();
		initOrdo();
	}

	@Override
	public void stateChanged(SchedulerState state) {
		if (state == SchedulerState.STARTING || state == SchedulerState.STARTED || state == SchedulerState.SUSPENDED || state == SchedulerState.UNSUSPENDED || state == SchedulerState.PRESTOP
				|| state == SchedulerState.STOPPING)
			synchronized (this) {
				if (state == SchedulerState.STARTING) {
					if (this.diagramScheduler != null || scenarioData == null)
						return;
					FlowDiagram fd = (FlowDiagram) scenarioData;
					if (this.runningMode == RunningMode.MULTICORE)
						this.diagramScheduler = new MultiCoreDiagramScheduler(fd, this.schedulableBlocks);
					else if (this.runningMode == RunningMode.MONOCORE) {
						CountDownLatch startLock = new CountDownLatch(1);
						this.diagramScheduler = new MonoCoreDiagramScheduler(fd, this.schedulableBlocks, startLock);
						schedulerInterface.setStartLock(startLock);
					} else
						this.diagramScheduler = new NoThreadedDiagramScheduler(fd, this.schedulableBlocks);
				} else if (state == SchedulerState.STARTED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.start();
				} else if (state == SchedulerState.SUSPENDED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.pause();
				} else if (state == SchedulerState.UNSUSPENDED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.resume();
				} else if (state == SchedulerState.PRESTOP) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.preStop();
				} else if (state == SchedulerState.STOPPING)
					if (this.diagramScheduler != null) {
						this.diagramScheduler.stop();
						this.diagramScheduler = null;
					}
			}
	}

	private void unRegisterVisuableSchedulable(Block block) {
		this.animationTimerConsumer.unRegister(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	private void updateDiagramInfo(Object element, ModificationType modificationType) {
		// System.out.println("updateDiagramInfo");
		FlowDiagram fd = (FlowDiagram) scenarioData;
		if (fd == null)
			return;
		ArrayList<Block> newSchedulableBlocks = new ArrayList<>();
		for (Block block : fd.getBlocks())
			if (block.operator instanceof Schedulable)
				newSchedulableBlocks.add(block);
		if (!this.schedulableBlocks.equals(newSchedulableBlocks)) {
			this.schedulableBlocks = newSchedulableBlocks;
			fireScheduleChanged();
		}
		long newNbGap = 0;
		for (Block block : newSchedulableBlocks)
			if (block.operator instanceof LocalScenario) {
				long nbGapOp = ((LocalScenario) block.operator).getEndTime();
				if (nbGapOp > newNbGap)
					newNbGap = nbGapOp;
			}
		if (newNbGap != this.nbGap)
			this.nbGap = newNbGap;
		// fireNbGapChanged();
		if (this.animationTimerConsumer != null)
			if (element instanceof Link) {
				if (modificationType == ModificationType.NEW) {
					Input input = ((Link) element).getInput();
					if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable)
						registerVisuableSchedulable(((BlockInput) input).getBlock());
				} else if (modificationType == ModificationType.DELETE) {
					Input input = ((Link) element).getInput();
					if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable) {
						Block block = ((BlockInput) input).getBlock();
						boolean needToUnregister = true;
						for (BlockInput bi : block.getInputs())
							if (bi.getLink() != null) {
								needToUnregister = false;
								break;
							}
						if (needToUnregister)
							unRegisterVisuableSchedulable(((BlockInput) input).getBlock());
					}
				}
			} else if (element instanceof Block && ((Block) element).operator instanceof VisuableSchedulable) {
				Block block = (Block) element;
				VisuableSchedulable vs = (VisuableSchedulable) block.operator;
				this.animationTimerConsumer.unRegister(vs);
				this.animationTimerConsumer.register(block.getRemoteOperator() != null ? (VisuableSchedulable) block.getRemoteOperator() : vs);
			}
		if (element instanceof Block && this.diagramScheduler != null)
			if (modificationType == ModificationType.NEW)
				this.diagramScheduler.addBlock((Block) element);
			else if (modificationType == ModificationType.DELETE)
				this.diagramScheduler.removeBlock((Block) element);
	}

	private static void updateImage(StackPane sp, ImageView iv) {
		int width = (int) (sp.getPrefWidth() * 0.7);
		int height = (int) (sp.getPrefWidth() * 0.7);
		iv.setFitHeight(height);
		iv.setFitWidth(width);
		if (width == 0 || height == 0)
			iv.setImage(null);
		else {
			createBufferedImage(width, height);
			iv.setImage(SwingFXUtils.toFXImage(buffImage, null));
		}
	}

	@Override
	public void startStopChanged() {
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;
		for (Block sb : this.schedulableBlocks) {
			Object op = sb.operator;
			if (op instanceof Scenario) {
				Date std = ((Scenario) op).getStartTime();
				long beginningTime = std != null ? std.getTime() : ((Scenario) op).getBeginningTime();
				if (beginningTime < min)
					min = beginningTime;
				std = ((Scenario) op).getStopTime();
				long endTime = std != null ? std.getTime() : ((Scenario) op).getEndTime();
				if (endTime > max)
					max = endTime;
			}
		}
		if (schedulerInterface != null) {
			schedulerInterface.setStartTime(min);
			schedulerInterface.setStopTime(max);
		}
		fireStartStopTimeChanged();
	}

	@Override
	public Scenario[] getAdditionalScenario() {
		return this.schedulableBlocks.stream().map(b -> (Scenario) b.operator).toArray(Scenario[]::new);
	}

	@Override
	public Date getStartTime() {
		return null;
	}

	@Override
	public Date getStopTime() {
		return null;
	}

	@Override
	public double getPeriod() {
		return this.periodFromSubScenario;
	}

	@Override
	public boolean canRecord() {
		return true;
	}

	@Override
	public boolean isRecording() {
		FlowDiagram fd = (FlowDiagram) getScenarioData();
		return fd == null ? false : fd.isRecording();
	}

	@Override
	public void setRecording(boolean recording) {
		((FlowDiagram) getScenarioData()).setRecording(recording);
	}

	@Override
	public void recordingPropertyChanged(boolean recording) {
		fireRecordingChanged(recording);
	}

	@Override
	public void loaded(Module module) {}

	@Override
	public void unloaded(Module module) {}

	@Override
	public Runnable modified(Module module) { // Save old diagrams info and reload them after the reload of the module
		FlowDiagram fd = (FlowDiagram) scenarioData;
		ArrayList<Element> blockElements = new ArrayList<>();
		ArrayList<Element> linkElements = new ArrayList<>();
		if (fd != null) {
			List<Block> blocks = fd.getBlocks();
			Block[] blocksToRemove = fd.getBlocks().stream().filter(block -> block.operator.getClass().getModule().equals(module)).toArray(size -> new Block[size]);
			for (Block blockToRemove : blocksToRemove) {
				System.out.println("Reload operator of block: " + blockToRemove.getName());
				Document document;
				try {
					document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
					// Save Block properties
					blockElements.add(getElementFromBlock(blockToRemove, document, 0));
					// Save Links properties
					int linkIndex = 0;
					for (Block block : blocks)
						for (BlockInput input : block.getInputs()) {
							Link link = input.getLink();
							if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == blockToRemove) {
								System.out.println("Save output link: " + link);
								linkElements.add(getElementFromLink(link, document, linkIndex++));
							}
						}
					for (BlockInput input : blockToRemove.getInputs()) {
						Link link = input.getLink();
						if (link != null) {
							System.out.println("Save input link: " + link);
							linkElements.add(getElementFromLink(link, document, linkIndex++));
						}
					}
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
			}
		}
		return () -> {
			ArrayList<Tuple<Block, String>> missedInputs = new ArrayList<>();
			HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap = new HashMap<>();
			// Reload block properties
			for (Element blockElement : blockElements) {
				Block block = getBlockFromElement(blockElement, missedInputs, varArgsInputChangeListenerBlockMap);
				if (block != null)
					fd.addBlock(block);
			}
			// Reload links properties
			ArrayList<String[]> missedLink = new ArrayList<>();
			for (Element linkElement : linkElements)
				createLinkFromElement(fd, linkElement, missedLink);
			setVarArgsLinksAndCreatePropertyInput(fd, missedLink, varArgsInputChangeListenerBlockMap, missedInputs);
		};
	}
}