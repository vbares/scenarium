/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

public class WatchServiceTest {

	private WatchServiceTest() {}

	public static void main(String[] args) throws IOException, InterruptedException {
		WatchService watchService = FileSystems.getDefault().newWatchService();
		Path testPath = Path.of("/home/marc/test");
		testPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);

		System.out.println("Start watchService");
		WatchKey key;
		while ((key = watchService.take()) != null) {
			ArrayList<Path> createdPath = new ArrayList<>();
			List<WatchEvent<?>> newEvents = key.pollEvents();
			System.out.println("------new event received: " + newEvents.size() + "-------");
			for (WatchEvent<?> event : newEvents) {
				Kind<?> kind = event.kind();
				Path path = testPath.resolve((Path) event.context());
				if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
					System.out.println("ENTRY_CREATE: " + path);
					createdPath.add(path);
				} else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
					System.out.println("ENTRY_DELETE: " + path);
				else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
					System.out.println("ENTRY_MODIFY: " + path);
					if (!createdPath.contains(path))
						System.out.println("ENTRY_MODIFY2: " + path);
				}
			}
			key.reset();
			System.out.println("-----------------------------------");
			System.out.println();
		}
	}
}
