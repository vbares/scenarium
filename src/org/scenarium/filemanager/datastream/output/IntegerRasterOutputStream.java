/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.struct.raster.IntegerRaster;

public class IntegerRasterOutputStream extends DataFlowOutputStream<IntegerRaster> {
	private byte[] tempData;

	@Override
	public void push(IntegerRaster raster) throws IOException {
		this.dataOutput.writeInt(raster.getWidth());
		this.dataOutput.writeInt(raster.getHeight());
		this.dataOutput.writeInt(raster.getType());
		int[] pixels = raster.getData();
		if (this.tempData == null || this.tempData.length != pixels.length * Integer.BYTES)
			this.tempData = new byte[pixels.length * Integer.BYTES];
		ByteBuffer.wrap(this.tempData).asIntBuffer().put(pixels);
		this.dataOutput.write(this.tempData);
	}
}
