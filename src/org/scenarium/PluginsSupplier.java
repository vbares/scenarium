/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.scenarium.display.ToolBarInfo;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenariomanager.Scenario;

public interface PluginsSupplier {
	public default void populateOperators(Consumer<Class<?>> operatorConsumer) {}

	public default void populateCloners(ClonerConsumer clonerConsumer) {}

	public default void populateEditors(EditorConsumer editorConsumer) {}

	public default void populateDrawers(BiConsumer<Class<? extends Object>, Class<? extends TheaterPanel>> drawersConsumer) {}

	public default void populateToolBars(Consumer<ToolBarInfo> toolBarConsumer) {}

	public default void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {}

	public default void populateDataStream(DataStreamConsumer dataStreamConsumer) {};

	public default void populateFileRecorder(BiConsumer<Class<?>, Class<? extends FileStreamRecorder>> fileRecorderConsumer) {};
}