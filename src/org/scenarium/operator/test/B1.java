/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class B1 extends EvolvedOperator {
	private Timer timer;
	private ReentrantLock lock;

	@Override
	public void birth() {
		this.timer = new Timer();
		this.lock = new ReentrantLock();
		triggerOutput(new Object[] { new byte[] { 1, 2, 3, 4, 5 } }, new long[] { System.currentTimeMillis() });
		this.timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				B1.this.lock.lock();
				try {
					triggerOutput(new Object[] { new byte[] { 1, 2, 3, 4, 5 } }, new long[] { System.currentTimeMillis() });
				} finally {
					B1.this.lock.unlock();
				}
			}
		}, 0, 1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void death() {
		this.lock.lock();
		try {
			this.timer.cancel();
			this.timer = null;
		} finally {
			this.lock.unlock();
		}
		try {
			System.out.println("begin wait");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
	}

	public int getOutputA() {
		return 6;
	}

	public byte[] process() {
		return null;
	}
}
