/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Objects;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.DynamicAnnotationBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.basic.DynamicPathInfo;
import org.beanmanager.editors.basic.PathInfo;
import org.scenarium.editors.NotChangeableAtRuntime;

import javafx.scene.paint.Color;

public abstract class LocalScenario extends Scenario implements DynamicAnnotationBean {
	protected static final Color[] COLORS = new Color[] { new Color(77 / 255.0, 175 / 255.0, 74 / 255.0, 1), new Color(55 / 255.0, 126 / 255.0, 184 / 255.0, 1),
			new Color(255 / 255.0, 127 / 255.0, 0 / 255.0, 1), new Color(166 / 255.0, 86 / 255.0, 40 / 255.0, 1), new Color(247 / 255.0, 129 / 255.0, 191 / 255.0, 1),
			new Color(152 / 255.0, 78 / 255.0, 163 / 255.0, 1), new Color(228 / 255.0, 26 / 255.0, 26 / 255.0, 1), };
	@PropertyInfo(index = 0)
	@PathInfo(directory = false)
	@DynamicPathInfo(filtersMethodName = "getFilters")
	@NotChangeableAtRuntime
	protected File file;

	protected final EventListenerList listeners = new EventListenerList();

	private int colorIndex;

	@Override
	public void birth() throws IOException, ScenarioException {
		if (this.file != null)
			load((Object) this.file, false);
	}

	public abstract boolean canCreateDefault();

	public abstract double getPeriod();

	public File getFile() {
		return this.file;
	}

	public String[] getFilters() {
		String[] rf = getReaderFormatNames();
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		for (int i = 0; i < rf.length; i++)
			sb.append(" " + rf[i]);
		return new String[] { sb.toString() };
	}

	@Override
	public void getInfo(LinkedHashMap<String, String> info) throws IOException {
		info.put("Type of scenario", "Local-" + getClass().getSimpleName());
		populateInfo(info);
		File scenarioFile = getFile();
		String fileName = scenarioFile.getName();
		info.put("File type", fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase());
		info.put("File path", scenarioFile.getParent());
		info.put("File Name", fileName);
		info.put("File size", NumberFormat.getInstance().format(getFile().length()));
		info.put("Last Modified", new SimpleDateFormat("dd/MM/YYYY, kk:mm:ss").format(new Date(scenarioFile.lastModified())));
	}

	@Override
	public Object getSource() {
		return getFile();
	}

	@Override
	public void initOrdo() {}

	public abstract void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException;

	@Override
	public synchronized void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		// if(scenarioSource == null)
		// return;
		if (scenarioSource == null || ((File) scenarioSource).exists()) {
			load((File) scenarioSource, backgroundLoading);
			if (this.scenarioData == null && !backgroundLoading)
				System.err.println("Error while loading the scenario source: " + scenarioSource + "\nThe loader associated: " + getClass().getSimpleName() + " didn't return a scenarioData");
		}
		// if (!backgroundLoading) //scenarioData != null ne pas faire l'ancienne version sinon 2 event loadchanged si chargement rapide
		// fireLoadChanged();
		fireAnnotationChanged(this, "startTime");
		fireAnnotationChanged(this, "stopTime");
	}

	public void addPeriodListener(PeriodListener listener) {
		this.listeners.add(PeriodListener.class, listener);
	}

	public void addPeriodListenerIfNotPresent(PeriodListener listener) {
		for (Object list : this.listeners.getListenerList())
			if (listener == list)
				return;
		addPeriodListener(listener);
	}

	public void removePeriodListener(PeriodListener listener) {
		this.listeners.remove(PeriodListener.class, listener);
	}

	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		if (file != null) {
			String fileName = file.getName();
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			if (!Arrays.asList(getReaderFormatNames()).contains(fileExt))
				if (fileName.length() != 0)
					System.err.println(getClass().getSimpleName() + " reader cannot take " + fileExt + " file");
		}
		this.file = file;
		fireSourceChanged();
	}

	public void addStartStopChangeListener(StartStopChangeListener listener) {
		this.listeners.add(StartStopChangeListener.class, listener);
	}

	public void addStartStopChangeListenerIfNotPresent(StartStopChangeListener listener) {
		for (Object list : this.listeners.getListenerList())
			if (listener == list)
				return;
		addStartStopChangeListener(listener);
	}

	public void removeStartStopChangeListener(StartStopChangeListener listener) {
		this.listeners.remove(StartStopChangeListener.class, listener);
	}

	protected void fireStartStopTimeChanged() {
		for (StartStopChangeListener listener : this.listeners.getListeners(StartStopChangeListener.class))
			listener.startStopChanged();
	}

	@Override
	public void setSource(Object source) {
		setFile((File) source);
	}

	@Override
	public String toString() {
		return this.file != null ? this.file.getName().toString() : "no file";
	}

	public static int getScenarioLenght(File source) {
		String ext = source.getAbsolutePath().substring(source.getAbsolutePath().length() - 3, source.getAbsolutePath().length());
		if (ext.equals("inf")) {
			String ligne;
			try (BufferedReader br = new BufferedReader(new FileReader(source))) {
				ligne = br.readLine();
				return Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			} catch (IOException | NumberFormatException | StringIndexOutOfBoundsException e) {
				return 1;
			}
		}
		return 1;
	}

	public static String getScenarioPath(File source) {
		return source.getAbsolutePath();
	}

	public static Object getScenarioSource(String type, String value) {
		if (type.equals(File.class.getSimpleName()))
			return new File(value);
		return null;
	}

	public static boolean isAvailable(File source) {
		return source.exists();
	}

	public void setColorIndex(int colorIndex) {
		this.colorIndex = colorIndex;
		updateColor();
	}

	@Override
	public Color getColor() {
		return COLORS[this.colorIndex];
	}
}
