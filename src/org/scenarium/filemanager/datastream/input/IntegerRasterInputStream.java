/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.input;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.struct.raster.IntegerRaster;

public class IntegerRasterInputStream extends DataFlowInputStream<IntegerRaster> {
	private byte[] tempData;

	@Override
	public IntegerRaster pop() throws IOException {
		IntegerRaster rb = new IntegerRaster(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());
		if (this.tempData == null || this.tempData.length != rb.getSize() * Integer.BYTES)
			this.tempData = new byte[rb.getSize() * Integer.BYTES];
		dataInput.readFully(this.tempData);
		ByteBuffer.wrap(this.tempData).asIntBuffer().put(rb.getData());
		return rb;
	}
}
