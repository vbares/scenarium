Scenarium internal functionnalities
===================================

Scenarium is based on 3 concept:
  - The code
  - The operator configuration (parameters)
  - The scenario

We need to notice here,that all these element have a separate life-time

For example, when you create a new ```operator```, this create an intance of it and it is unique.
You add it on multiple ```scenario```, it will have the same parameters, but not the same instance.
Therefore, when you change the parameter, this will affect all the scenario that use this ```operator```.



