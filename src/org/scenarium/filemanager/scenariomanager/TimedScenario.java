/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.editors.time.DynamicDateInfo;
import org.scenarium.editors.NotChangeableAtRuntime;

public abstract class TimedScenario extends LocalScenario {
	@PropertyInfo(index = 1)
	@NumberInfo(min = 1)
	@NotChangeableAtRuntime
	private double period = 40.0;
	@PropertyInfo(index = 2)
	@NotChangeableAtRuntime
	@DynamicDateInfo(minMethodName = "getMinStartTime", maxMethodName = "getMaxStartTime", timePatternMethodName = "getTimePattern")
	private Long startTime = null;
	@PropertyInfo(index = 3)
	@DynamicDateInfo(minMethodName = "getMinStopTime", maxMethodName = "getMaxStopTime", timePatternMethodName = "getTimePattern")
	@NotChangeableAtRuntime
	private Long stopTime = null;

	private long testedStartTime = Long.MAX_VALUE;
	private long testedStopTime = Long.MIN_VALUE;

	@Override
	public void birth() throws IOException, ScenarioException {
		super.birth();
		if (this.scenarioData == null) {
			this.testedStartTime = Long.MAX_VALUE;
			this.testedStopTime = Long.MIN_VALUE;
		} else {
			this.testedStartTime = this.startTime == null ? Long.MIN_VALUE : this.startTime;
			this.testedStopTime = this.stopTime == null ? Long.MAX_VALUE : this.stopTime;
		}
	}

	@Override
	public void setFile(File file) {
		this.startTime = null;
		this.stopTime = null;
		this.testedStartTime = Long.MAX_VALUE;
		this.testedStopTime = Long.MIN_VALUE;
		super.setFile(file);
	}

	@Override
	protected boolean close() {
		this.testedStartTime = Long.MAX_VALUE;
		this.testedStopTime = Long.MIN_VALUE;
		return false;
	}

	public abstract long getNbFrame();

	@Override
	public long getEndTime() {
		long endTime = (long) (getBeginningTime() + getNbFrame() * getPeriod());
		return endTime < 0 ? 1 : endTime;
	}

	@Override
	public double getPeriod() {
		return this.period;
	}

	public void setPeriod(double period) {
		if (this.period != period) {
			// if (period < 1)
			// throw new IllegalArgumentException("The period must be strictly positive");
			this.period = period;
			firePeriodChanged();
		}
	}

	private void firePeriodChanged() {
		for (PeriodListener listener : this.listeners.getListeners(PeriodListener.class))
			listener.periodChanged(this.period);
	}

	@Override
	public Date getStartTime() {
		return this.startTime == null ? null : new Date(this.startTime);
	}

	public Long getMinStartTime() {
		long time = getBeginningTime();
		return time == -1 ? Long.MIN_VALUE : time;
	}

	public Long getMaxStartTime() {
		long time = this.stopTime == null ? getEndTime() : Math.min(getEndTime(), this.stopTime);
		return time == -1 ? Long.MAX_VALUE : time;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime == null ? null : startTime.getTime();
		fireStartStopTimeChanged();
		fireAnnotationChanged(this, "stopTime");
	}

	@Override
	public Date getStopTime() {
		return this.stopTime == null ? null : new Date(this.stopTime);
	}

	public Long getMinStopTime() {
		long time = this.startTime == null ? getBeginningTime() : Math.max(getBeginningTime(), this.startTime);
		return time == -1 ? Long.MIN_VALUE : time;
	}

	public Long getMaxStopTime() {
		long time = getEndTime();
		return time == -1 ? Long.MAX_VALUE : time;
	}

	public void setStopTime(Date stopTime) {
		this.stopTime = stopTime == null ? null : stopTime.getTime();
		fireAnnotationChanged(this, "startTime");
		fireStartStopTimeChanged();
	}

	@Override
	public boolean canTrigger(long time) {
		return time >= this.testedStartTime && time <= this.testedStopTime;
	}
}
