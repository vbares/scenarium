/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.timescheduler;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.PeriodListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.TimedScenario;

public class TimerScheduler extends Scheduler implements PeriodListener {
	protected ArrayList<Schedulable> scheduleElements = new ArrayList<>();
	private Timer simulationTimer;
	private int period;
	private LocalScenario mainSchedulable;
	private Thread timerThread;

	public TimerScheduler(boolean canReverse) {
		super(canReverse);
	}

	public void addScheduleElement(Schedulable scheduleElement, int index) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspendAndWait();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(this.scheduleElements);
		if (!newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.add(index == -1 ? newScheduleElements.size() : index, scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		this.scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	@Override
	public void clean() {
		if (this.mainSchedulable != null)
			this.mainSchedulable.removePeriodListener(this);
	}

	@Override
	public long getTimeStamp() {
		return this.timePointer * this.period;
	}

	@Override
	public long getIndex() {
		return this.timePointer;
	}

	@Override
	public long getNbGap() {
		return this.endTime - this.beginTime;
	}

	@Override
	public boolean isRunning() {
		return this.simulationTimer != null;
	}

	@Override
	public void nextStep() {
		setTimePointer(this.timePointer + 1);
	}

	@Override
	public void periodChanged(double period) {
		this.period = (int) Math.round(period);
		if (this.simulationTimer != null)
			start(false);
	}

	@Override
	public void previousStep() {
		setTimePointer(this.timePointer - 1);
	}

	public void removeScheduleElement(Schedulable scheduleElement) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspendAndWait();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(this.scheduleElements);
		if (newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.remove(scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		this.scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	public void resetScheduleElement() {
		this.scheduleElements = new ArrayList<>();
	}

	public void run() {
		if (this.timePointer >= getEffectiveStop() && this.speed > 0 || this.timePointer <= getEffectiveStart() && this.speed < 0) {
			if (this.repeat)
				this.timePointer = this.speed > 0 ? getEffectiveStart() : getEffectiveStop();
			else {
				pause(); // Pour ne rien faire apèrs
				endReached();
				return;
			}
		} else
			this.timePointer = this.speed > 0 ? this.timePointer + 1 : this.timePointer - 1;
		updateSchedulableElement();
	}

	@Override
	public void setIndex(long index) {
		setTimePointer(index);
	}

	@Override
	public void start(final boolean refresh) {
		Thread oldTimerThread = this.timerThread;
		stopTimer(null);
		this.simulationTimer = new Timer();
		this.timerThread = new Thread() {
			@Override
			public void run() {
				if (oldTimerThread != null)
					try {
						oldTimerThread.join();
					} catch (InterruptedException e) {
						return;
					}
				try {
					waitStarted();
				} catch (InterruptedException ex) {
					return;
				}
				if (refresh)
					updateSchedulableElement();
				int ms = (int) (100.0 / Math.abs(TimerScheduler.this.speed) * TimerScheduler.this.period);
				if (ms == 0)
					ms = 1;
				if (TimerScheduler.this.simulationTimer == null)
					return;
				try {
					TimerScheduler.this.simulationTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							TimerScheduler.this.run();
						}
					}, ms / 2, ms);
				} catch (IllegalStateException e) {} // Le timer est peut �tre d�ja cancel
			};
		};
		this.timerThread.start();
	}

	private void stopTimer(Runnable taskWhenSuspented) {
		if (this.simulationTimer != null) {
			this.simulationTimer.cancel();
			this.simulationTimer.purge();
			this.simulationTimer = null;
			if (taskWhenSuspented == null)
				this.timerThread = null;
			else
				new Thread(() -> {
					try {
						this.timerThread.join();
						this.timerThread = null;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					taskWhenSuspented.run();
				}).start();
		} else if (taskWhenSuspented != null)
			taskWhenSuspented.run();
	}

	@Override
	public void suspend(Runnable run) {
		stopTimer(run);
	}

	@Override
	protected void suspendAndWait() {
		Thread timerThread = this.timerThread;
		stopTimer(null);
		try {
			timerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void timePointerChanged() {
		updateSchedulableElement();
	}

	@Override
	public void updateBeginAndEndTime() {
		updateBeginAndEndTime(this.scheduleElements);
	}

	private void updateBeginAndEndTime(ArrayList<Schedulable> scheduleElements) {
		this.beginTime = 0;
		this.endTime = 0;
		if (this.mainSchedulable != null)
			this.mainSchedulable.removePeriodListener(this);
		this.mainSchedulable = null;
		for (Schedulable schedulable : scheduleElements)
			if (schedulable instanceof Scenario) {
				long beginTimeScenario = ((Scenario) schedulable).getBeginningTime();
				long endTimeScenario = ((Scenario) schedulable).getEndTime();
				if (this.beginTime == -1 || beginTimeScenario < this.beginTime)
					this.beginTime = beginTimeScenario;
				if (endTimeScenario != -1) {
					endTimeScenario--;
					if (endTimeScenario > this.endTime)
						this.endTime = endTimeScenario;
				}
				if (schedulable instanceof TimedScenario)
					this.mainSchedulable = (TimedScenario) schedulable;
			}
		if (this.mainSchedulable != null) {
			this.period = (int) Math.round(this.mainSchedulable.getPeriod());
			this.mainSchedulable.addPeriodListener(this);
		}
		firePropertyChangeEvent(SchedulerState.INTERVALTIMECHANGED);
	}

	private void updateSchedulableElement() {
		try {
			for (Schedulable so : this.scheduleElements) // TODO updateSchedulableElement...
				so.update(this.timePointer);
			if (!isRunning())
				for (VisuableSchedulable visuableSchedulable : this.visualScheduleElements)
					visuableSchedulable.paint();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void updateSpeed() {
		if (this.simulationTimer != null)
			start(false);
	}
}
