Tutorial 1: First Run of Scenarium
==================================

After starting Eclipse with the correct workspace.

  - Select/open the file ```scenarium/src/org.scenarium/Scenarium.java```
  - And run the program in menu: ```run -> Run```

** Scenarium is now started**

Create our first flow
---------------------

First step is to add all needed blocks

  - Add the **data Generator**:
      - Right click on the mouse and ```Add Operator``` <br> A menu will open with all the availlable element addable.
      - Drag and drop ```Dataprocessing/DataGenerator``` in the Scenarium main windows.
  - Add the **perlin noise** filter:
      - Jump in the ```Operator``` windows.
      - Drag and drop ```Dataprocessing/PerlinNoise``` in the Scenarium main windows (must have enought place to drop).
  - Add the **Osciloscope** display:
      - Jump in the ```Operator``` windows.
      - Drag and drop ```basic/Osciloscope``` in the Scenarium main windows.

![simple blocks](tutorial_01/simple_blocks.png)


Link the generator to the other block

  - Click on the output of the **generator** block and drag it to the input of **perlin noise** block
  - Click on the output of the **generator** block and drag it to the input of **Osciloscope** block
  - Click on the output of the **perlin noise** block and drag it to the second input of **Osciloscope** block

![linked blocks](tutorial_01/linked_flow.png)

Let now start the application and play with it

  - Open the player windows with the menu: ```View -> Player```
  - Press ```Play``` button

==> An osciloscope will open and display the values generated

  - Souble click on the Generator ```Gray area```
  - Set your mouse hover the Value area and scoll or change the value


![full example](tutorial_01/full_example.png)


