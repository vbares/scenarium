/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.serial;

import gnu.io.SerialPort;

public enum RXTXStopBits {
	STOPBITS_1(SerialPort.STOPBITS_1), STOPBITS_2(SerialPort.STOPBITS_2), STOPBITS_1_5(SerialPort.STOPBITS_1_5);

	private final int value;

	private RXTXStopBits(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
