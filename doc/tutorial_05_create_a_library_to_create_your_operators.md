Create your library
===================

Create a new project in eclipse: ```file -> new -> Java Project```
  - Project name ```SampleLib```
  - Tick Use default JRE
  - ![new project](tutorial_05/new_project.png)
  - Click on ```next```
  - 
  - On tab ```Project``` select ```ModulePath```
  - ![new project](tutorial_05/add_module_path.png)
  - Click on ```Add...```
  - Tick ```beanmanager```
  - Tick ```scenaruim```
  - ![new project](tutorial_05/Select_scenarium.png)
  - Click on ```OK```
  - 
  - On tab ```Libraries``` select ```ModulePath```
  - ![new project](tutorial_05/add_jars.png)
  - Click on ```Add JARs...```
  - Select all JARs in scenarium/lib/*
  - ![new project](tutorial_05/Select_all_jars_1.png)
  - Click on ```Ok```
  - 
  - On tab ```Libraries``` select ```ModulePath```
  - Click on ```Add JARs...```
  - Select ```vecmath.jar``` in beanmanager/lib/
  - ![new project](tutorial_05/Select_all_jars_2.png)
  - Click on ```Ok```
  - 
  - Click on ```Finish```
  - Click on ```Create for the module info file.


Create the plugin elements:
---------------------------

Add the dependency of scenarium:

In the file ```module-info.java``` write:
```{.java}
module samplelib {
	// Require use of Scenarium (core)
	requires Scenarium;
}
```

Create the package:

  - ```File -> new -> package...```
  - Name: ```SampleLib```
  - Click on ```Finish```
  - Select Package ```SampleLib```
  - Right click on it & ```new -> Class```
      - Name ```SamplePlugin```
      - Click on ```Finish```

Configure the plugin in file ```SamplePlugin.java``` set ```implements PluginsSupplier```:
```{.java}
public class SamplePlugin implements PluginsSupplier  {
	
}
```

In ```module-info.java``` write:
```{.java}
import org.scenarium.PluginsSupplier;
import SampleLib.SamplePlugin;

module SampleLib {
	// Request use of the PluginsSupplier module
	uses PluginsSupplier;
	// Say that it privide an external API
	provides PluginsSupplier with SamplePlugin;
	// Require use of Scenarium (core)
	requires Scenarium;
}
```

Now we provide a generic plugin for scenarium.

We do not declare any element of the generic plugin, For that, we need to override some function in ```SamplePlugin.java```
```{.java}
package SampleLib;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.scenarium.ClonerConsumer;
import org.scenarium.DataStreamConsumer;
import org.scenarium.PluginsSupplier;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenariomanager.Scenario;

public class SamplePlugin implements PluginsSupplier  {
	@Override
	public void populateOperators(Consumer<Class<?>> operatorConsumer) {
		//operatorConsumer.accept(XxxxxxYyyyyy.class);
	}
	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		//clonerConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyy::clone);
	}
	@Override
	public void populateDataStream(DataStreamConsumer dataStreamConsumer) {
		//dataStreamConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyInputStream.class, XxxxxxYyyyyyOutputStream.class);
	}
	@Override
	public void populateFileRecorder(BiConsumer<Class<?>, Class<? extends FileStreamRecorder>> fileRecorderConsumer) {
		//fileRecorderConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyStreamRecorder.class);
	}
	@Override
	public void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {
		//scenarioConsumer.accept(XxxxxxYyyyyy.class, new XxxxxxYyyyyy().getReaderFormatNames());
	}
	@Override
	public void populateDrawers(BiConsumer<Class<? extends Object>, Class<? extends TheaterPanel>> drawersConsumer) {
		//drawersConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyDrawer.class);
	}
}
```

All Is ready, but you are not ready to run ...

Integrate your library
======================

Development mode:
-----------------

To do this simply, we use an external repository to tun scenarium with external plugins: [scenarium-run](https://gitlab.com/HeeroYui/scenarium-run)

In this project: edit property:
  - On tab ```Project``` select ```ModulePath```
  - ![new project](tutorial_05/add_project_in_path.png)
  - Click on ```Add...```
  - Tick ```pluginlib```
  - Click on ```OK```
  - Click on ```Apply and close```

Run the application ```ScenariumRun```

==> !!! enjoy !!!

Release Mode:
-------------

TODO: ...


