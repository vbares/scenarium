/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.lang.invoke.MethodHandle;

public class BlockOutput extends Output implements BlockIO {
	protected final Block block;
	private final MethodHandle methodHandle;

	public BlockOutput(Block block, MethodHandle method, Class<?> type, String name) {
		super(type, name);
		this.block = block;
		this.methodHandle = method;
	}

	@Override
	public Block getBlock() {
		return this.block;
	}

	// @Override //PK??
	// public Class<?> getType() {
	// return type;
	// }

	// @Override //PK??
	// public String getName() {
	// return name;
	// }

	// @Override
	// public Point2D getPosition() {
	// Rectangle2D rect = getBlock().getRectangle();
	// return new Point2D((float)(position.getX() + rect.getMinX()), position.getY() + rect.getMinY());
	// }

	public MethodHandle getMethodHandle() {
		return this.methodHandle;
	}
}
