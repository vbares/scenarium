/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_LINES;
import static com.jogamp.opengl.GL.GL_NICEST;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TRIANGLE_STRIP;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_COLOR_MATERIAL;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_FLAT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHT0;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

import java.util.ArrayList;
import java.util.Random;

import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix4d;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.drawer.camera3D.ArcBallCamera;
import org.scenarium.display.drawer.camera3D.Camera;
import org.scenarium.display.drawer.camera3D.CameraChangeListener;
import org.scenarium.display.drawer.camera3D.FreeFlyCamera;
import org.scenarium.struct.DrawableObject3D;
import org.scenarium.struct.OldObject3D;
import org.scenarium.test.fx.FxTest;
import org.scenarium.timescheduler.Scheduler;

import com.jogamp.opengl.DefaultGLCapabilitiesChooser;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLDrawableFactory;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class Drawer3D extends TheaterPanel implements CameraChangeListener, StackableDrawer {
	protected static Color[] colors = new Color[] { Color.RED, Color.BLUE, Color.LIME, Color.FUCHSIA, Color.CYAN, Color.YELLOW, new Color(0.5, 0, 1, 1), new Color(1, 0.5, 0, 1) };
	private static final String LIGHT = "Light";
	private static final String AXIS = "Axis";
	private static final String GRID = "Grid";
	private static final String OBJECT3D = "Object 3D";
	protected static GL2 gl;
	private static GLU glu;
	private static GLOffscreenAutoDrawable drawable;

	// private static boolean isInstance;
	// private static boolean initDone;
	public static void drawTorus(GL2 gl, float minorRadius, float majorRadius) {
		gl.glColor3f(0.75f, 0.75f, 1.0f);
		gl.glBegin(GL_TRIANGLE_STRIP);
		int nbFace = 20;
		for (int i = 0; i < nbFace; i++)
			for (int j = -1; j < nbFace; j++) {
				float wrapFrac = j % nbFace / (float) nbFace;
				double phi = Math.PI * 2.0 * wrapFrac;
				float sinphi = (float) Math.sin(phi);
				float cosphi = (float) Math.cos(phi);
				float r = majorRadius + minorRadius * cosphi;
				gl.glNormal3d(Math.sin(Math.PI * 2.0 * (i % nbFace + wrapFrac) / nbFace) * cosphi, sinphi, Math.cos(Math.PI * 2.0 * (i % nbFace + wrapFrac) / nbFace) * cosphi);
				gl.glVertex3d(Math.sin(Math.PI * 2.0 * (i % nbFace + wrapFrac) / nbFace) * r, minorRadius * sinphi, Math.cos(Math.PI * 2.0 * (i % nbFace + wrapFrac) / nbFace) * r);
				gl.glNormal3d(Math.sin(Math.PI * 2.0 * (i + 1 % nbFace + wrapFrac) / nbFace) * cosphi, sinphi, Math.cos(Math.PI * 2.0 * (i + 1 % nbFace + wrapFrac) / nbFace) * cosphi);
				gl.glVertex3d(Math.sin(Math.PI * 2.0 * (i + 1 % nbFace + wrapFrac) / nbFace) * r, minorRadius * sinphi, Math.cos(Math.PI * 2.0 * (i + 1 % nbFace + wrapFrac) / nbFace) * r);
			}
		gl.glEnd();
	}

	public static void main(String[] args) throws Exception {
		float[] vertexTest = new float[10000 * 3];
		Random r = new Random();
		for (int i = 0; i < vertexTest.length;) {
			vertexTest[i++] = r.nextFloat() * 10.0f;
			vertexTest[i++] = r.nextFloat() * 100.0f;
			vertexTest[i++] = r.nextFloat() * 10.0f;
		}
		float[] colorTest = new float[vertexTest.length];
		for (int i = 0; i < colorTest.length;) {
			colorTest[i++] = r.nextFloat();
			colorTest[i++] = r.nextFloat();
			colorTest[i++] = r.nextFloat();
		}
		new FxTest().launchIHM(args, () -> {
			Drawer3D pcdFx = new Drawer3D();
			pcdFx.init();
			pcdFx.setPrefSize(1024, 768);
			pcdFx.setDrawableElement(new OldObject3D(OldObject3D.POINTS, vertexTest, colorTest, 3));
			return pcdFx;
		});
	}

	// Filter of Theater
	private boolean filterLight = true;
	private boolean filterAxis = true;
	private boolean filterGrid = true;
	private boolean filter3DObject = true;
	private ImageView iv;
	@PropertyInfo(index = 0, nullable = false)
	@BeanInfo(possibleSubclasses = { FreeFlyCamera.class, ArcBallCamera.class }, alwaysExtend = true, inline = true)
	private Camera camera = new ArcBallCamera();
	@PropertyInfo(index = 1, nullable = false)
	private Color clearColor = new Color(0.0, 0.0, 0.0, 0.5);
	protected double[] matrix = new double[16];
	private double xMouse;

	// static { //pk???
	// DataLoader.addToBackgroundloadingtask(new Supplier<Boolean>() {
	// public Boolean get() {
	// if (isInstance && !initDone) {
	// Thread t = new Thread(() -> {
	// // GLProfile.initSingleton();
	// initDone = true;
	// });
	// t.setPriority(Thread.MIN_PRIORITY);
	// t.start();
	// }
	// return true;
	// }
	// });
	// }

	private double yMouse;

	// public Drawer3DFx() {
	// isInstance = true;
	// }

	private float pointSize = 3;

	private void adaptViewToScenario() {
		this.camera.reset();
	}

	@Override
	public void cameraChange() {
		repaint(false);
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		double dist = Math.abs(this.camera.getDistance());
		glu.gluPerspective(45.0f, getWidth() / getHeight(), dist / 10, dist * 100); // pas trop grand sinon arcball deconne
		// gl.glOrtho(0, 50, 50, 0, 0, 100);
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	@Override
	public void death() {
		super.death();
		destroyOffscreenAutoDrawable();
		getChildren().clear();
	}

	private static synchronized void destroyOffscreenAutoDrawable() {
		if (gl == null)
			return;
		gl = null;
		glu = null;
		drawable.getContext().release();
		drawable.destroy();
	}

	protected void drawAxis(GL2 gl) {
		gl.glLineWidth(3);
		gl.glBegin(GL_LINES);
		// x axis
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(1.0f, 0.0f, 0.0f);
		// y axis
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 1.0f, 0.0f);
		// Z axis
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 1.0f);
		gl.glEnd();
	}

	protected void drawGrid(GL2 gl, int min, int max) {
		gl.glLineWidth(1);
		gl.glColor3f(0.0f, 0.0f, 0.0f);
		for (int x = min; x < max + 1; x++) {
			gl.glBegin(GL.GL_LINE_LOOP);
			gl.glVertex3f(x, min, 0);
			gl.glVertex3f(x, max, 0);
			gl.glEnd();
		}
		for (int y = min; y < max + 1; y++) {
			gl.glBegin(GL.GL_LINE_LOOP);
			gl.glVertex3f(min, y, 0);
			gl.glVertex3f(max, y, 0);
			gl.glEnd();
		}
	}

	protected void drawWorld() {
		// drawTorus(gl, 0.30f, 0.5f);
		if (this.filterAxis)
			drawAxis(gl);
		if (this.filterGrid)
			drawGrid(gl, -100, 100);
		if (this.filter3DObject) {
			int colorIndex = 0;
			for (DrawableObject3D obj : getObjects3D())
				if (obj != null) {
					obj.draw(gl, colors[colorIndex++]);
					// drawObject3D(gl, (OldObject3D) obj, colors[colorIndex]);
					// if (obj.elementType != OldObject3D.TEXTURE)
					// colorIndex++;
					if (colorIndex == colors.length)
						colorIndex = 0;
				}
		}
	}

	@Override
	public void fitDocument() {
		// camera.reset();
	}

	public Camera getCamera() {
		return this.camera;
	};

	public Color getClearColor() {
		return this.clearColor;
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(1200, 800);
	}

	protected ArrayList<DrawableObject3D> getObjects3D() {
		ArrayList<DrawableObject3D> objs3D = new ArrayList<>();
		objs3D.add((DrawableObject3D) getDrawableElement());
		Object[] add = getAdditionalDrawableElement();
		if (add == null)
			return objs3D;
		for (Object addInput : add)
			if (addInput instanceof DrawableObject3D[])
				for (DrawableObject3D o : (DrawableObject3D[]) addInput)
					objs3D.add(o);
			else
				objs3D.add((DrawableObject3D) addInput);
		return objs3D;
	}

	private static synchronized void getOffscreenAutoDrawable() {
		if (gl != null)
			return;
		GLProfile glp = GLProfile.getDefault();
		GLCapabilities caps = new GLCapabilities(glp);
		caps.setHardwareAccelerated(true);
		caps.setDoubleBuffered(false);
		caps.setAlphaBits(8);
		caps.setRedBits(8);
		caps.setBlueBits(8);
		caps.setGreenBits(8);
		caps.setOnscreen(false);
		GLDrawableFactory factory = GLDrawableFactory.getFactory(glp);
		drawable = factory.createOffscreenAutoDrawable(factory.getDefaultDevice(), caps, new DefaultGLCapabilitiesChooser(), 100, 100);
		drawable.display();
		drawable.getContext().makeCurrent();
		gl = drawable.getGL().getGL2();
		gl.glClearDepth(1.0f);
		gl.glDepthFunc(GL_LEQUAL);
		gl.glEnable(GL_DEPTH_TEST);
		gl.glShadeModel(GL_FLAT);
		gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		glu = GLU.createGLU(gl);
		GLUquadric quadratic = glu.gluNewQuadric();
		glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);
		glu.gluQuadricTexture(quadratic, true);
		gl.glEnable(GL_COLOR_MATERIAL);
		gl.glDisable(GL_TEXTURE_2D);
	}

	public float getPointSize() {
		return this.pointSize;
	}

	@Override
	public String[] getStatusBarInfo() {
		gl.glLoadIdentity();
		this.camera.getMatrix(this.matrix);
		gl.glMultMatrixd(this.matrix, 0);
		Point3D p = screenToWorld2(new Point2D(this.xMouse, this.yMouse));
		return new String[] { "x: " + p.getX(), "y: " + p.getY() };
	}

	public void init() {
		getOffscreenAutoDrawable();
		if (gl == null)
			return;
		gl.glPointSize(this.pointSize);
		this.camera.init(this);
		this.camera.getMatrix(this.matrix);
		this.camera.setGl(gl);
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		this.iv = new ImageView();
		this.iv.setPreserveRatio(false);
		this.iv.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
			this.xMouse = e.getX();
			this.yMouse = e.getY();
			// refresh(false);
		});
		addEventHandler(KeyEvent.KEY_PRESSED, e -> {
			if (e.isConsumed())
				return;
			// boolean zoomIn = true;
			KeyCode keyCode = e.getCode();
			switch (keyCode) {
			case A:
				adaptViewToScenario();
				e.consume();
				break;
			// case UP:
			// zoomIn = false;
			// case DOWN:
			// if (e.isShiftDown()) {
			// zoom(zoomIn);
			// e.consume();
			// break;
			// }
			// case RIGHT:
			// case LEFT:
			// if (e.isControlDown()) {
			// double tx = getxTranslate();
			// double ty = getyTranslate();
			// tx += keyCode == KeyCode.RIGHT ? -1 : keyCode == KeyCode.LEFT ? 1 : 0;
			// ty += keyCode == KeyCode.DOWN ? -1 : keyCode == KeyCode.UP ? 1 : 0;
			// updateTransform(getScale(), tx, ty);
			// e.consume();
			// }
			default:
				return;
			}
		});
		this.iv.fitWidthProperty().bind(widthProperty());
		this.iv.fitHeightProperty().bind(heightProperty());

		setFocusTraversable(true);
		this.camera.setNode(this);
		// iv.fitWidthProperty().addListener(e -> reshape(drawable, 0, 0, (int) iv.getFitWidth(), (int) iv.getFitHeight()));
		// iv.fitHeightProperty().addListener(e -> reshape(drawable, 0, 0, (int) iv.getFitWidth(), (int) iv.getFitHeight()));
		getChildren().add(this.iv);
		init();
		// reshape(drawable, 0, 0, (int) getWidth(), (int) getHeight());
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : DrawableObject3D.class.isAssignableFrom(additionalInput) || DrawableObject3D[].class.isAssignableFrom(additionalInput);
	}

	@Override
	protected void paint(Object dataElement) {
		if (gl == null)
			return;
		gl.glClearColor((float) this.clearColor.getRed(), (float) this.clearColor.getGreen(), (float) this.clearColor.getBlue(), (float) this.clearColor.getOpacity());
		int width = (int) this.iv.getFitWidth();
		int height = (int) this.iv.getFitHeight();
		Drawer3D.drawable.setSurfaceSize(width, height);
		reshape(width, height);
		if (this.filterLight) {
			gl.glEnable(GL_LIGHT0);
			gl.glEnable(GL_LIGHTING);
		} else {
			gl.glDisable(GL_LIGHTING);
			gl.glDisable(GL_LIGHT0);
		}
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();
		gl.glPushMatrix();
		this.camera.getMatrix(this.matrix);
		gl.glMultMatrixd(this.matrix, 0);
		this.camera.drawCameraInfo(gl);
		drawWorld();
		gl.glPopMatrix();
		gl.glLoadIdentity();
		gl.glFlush();
		this.iv.setImage(SwingFXUtils.toFXImage(
				new AWTGLReadBufferUtil(drawable.getGLProfile(), true).readPixelsToBufferedImage(drawable.getGL(), 0, 0, drawable.getSurfaceWidth(), drawable.getSurfaceHeight(), true), null));
	}

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();
		this.theaterFilter.addChild(new TreeNode<>(new BooleanProperty(OBJECT3D, true)));
		this.theaterFilter.addChild(new TreeNode<>(new BooleanProperty(GRID, true)));
		this.theaterFilter.addChild(new TreeNode<>(new BooleanProperty(AXIS, true)));
		this.theaterFilter.addChild(new TreeNode<>(new BooleanProperty(LIGHT, true)));
	}

	protected void reshape(int width, int height) {
		if (gl == null)
			return;
		// texture.bind(gl); // Sinon plus rien...
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		double dist = Math.abs(this.camera.getDistance());
		// gl.glOrtho(0, 50, 50, 0, 0, 100);
		glu.gluPerspective(45.0f, (float) width / (float) height, dist * 10 / 100, dist * 10); // pas trop grand sinon arcball deconne
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
		this.camera.setBounds(width, height);
	}

	@SuppressWarnings("unused")
	private Point3D screenToWorld(Point2D point2d) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		GMatrix modelMatrix = new GMatrix(0, 0);
		modelMatrix.set(new Matrix4d(mvmatrix));
		modelMatrix.transpose();
		GMatrix projectionMatrix = new GMatrix(0, 0);
		projectionMatrix.set(new Matrix4d(projmatrix));
		projectionMatrix.transpose();

		this.xMouse = point2d.getX();
		this.yMouse = viewport[3] - point2d.getY();
		GVector point = new GVector(new double[] { this.xMouse / getWidth() * 2.0f - 1.0f, this.yMouse / getHeight() * 2.0f - 1.0f, 1, 1 });

		projectionMatrix.mul(modelMatrix);
		projectionMatrix.invert();
		point.mul(projectionMatrix, point);
		return new Point3D(point.getElement(0) / point.getElement(3), point.getElement(1) / point.getElement(3), point.getElement(2));
	}

	private Point3D screenToWorld2(Point2D point2d) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		int realy = 0;
		double[] wcoord = new double[4];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		realy = viewport[3] - (int) this.yMouse - 1;
		// position de la camera
		glu.gluUnProject((viewport[2] - viewport[0]) / 2.0, (viewport[3] - viewport[1]) / 2.0, 0, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
		// System.out.println("cam pos: x:" + wcoord[0] + "y:" + wcoord[1] + "z:" + wcoord[2]);
		double ax = wcoord[0];
		double ay = wcoord[1];
		double az = wcoord[2];
		glu.gluUnProject(this.xMouse, realy, 1, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
		double bx = wcoord[0];
		double by = wcoord[1];
		double bz = wcoord[2];
		double t = -az / (bz - az); // on calcule t en fonction de la position de la camera(Az) et de (Bz)
		double mx = t * (bx - ax) + ax; // on calcule les positions de M avec t
		double my = t * (by - ay) + ay;
		return new Point3D(mx, my, 0);
	}

	public void setCamera(Camera camera) {
		if (camera == null)
			return;
		camera.init(this); // je tue les anciennes propriété la
		camera.setNode(this);
		camera.setBounds((float) getWidth(), (float) getHeight());
		if (drawable != null)
			camera.setGl(gl);
		this.camera = camera;
	}

	public void setClearColor(Color clearColor) {
		this.clearColor = clearColor;
		if (drawable != null) {
			gl.glClearColor((float) clearColor.getRed(), (float) clearColor.getGreen(), (float) clearColor.getBlue(), (float) clearColor.getOpacity());
			repaint(false);
		}
	}

	public void setPointSize(float pointSize) {
		this.pointSize = pointSize;
		if (drawable != null)
			repaint(false);
		gl.glPointSize(pointSize);
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (filterPath[filterPath.length - 1].equals(OBJECT3D))
			this.filter3DObject = value;
		else if (filterPath[filterPath.length - 1].equals(AXIS))
			this.filterAxis = value;
		else if (filterPath[filterPath.length - 1].equals(GRID))
			this.filterGrid = value;
		else if (filterPath[filterPath.length - 1].equals(LIGHT)) {
			this.filterLight = value;
			repaint(false);
		} else
			return false;
		repaint(false);
		return true;
	}

	// http://www.songho.ca/opengl/gl_transform.html
	@SuppressWarnings("unused")
	private Point2D worldToScreen(Point3D xyzPoint) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		GMatrix viewMatrix = new GMatrix(0, 0);
		viewMatrix.set(new Matrix4d(mvmatrix));
		viewMatrix.transpose();
		GMatrix projectionMatrix = new GMatrix(0, 0);
		projectionMatrix.set(new Matrix4d(projmatrix));
		projectionMatrix.transpose();
		GVector point = new GVector(new double[] { xyzPoint.getX(), xyzPoint.getY(), xyzPoint.getZ(), 1 });
		point.mul(viewMatrix, point);
		point.mul(projectionMatrix, point);
		double w = point.getElement(3);
		double xnde = point.getElement(0) / w;
		double ynde = point.getElement(1) / w;
		double xw = getWidth() / 2 * xnde + (viewport[0] + getWidth() / 2);
		double yw = getHeight() / 2 * ynde + (viewport[1] + getHeight() / 2);

		double xndeb = (xw - (viewport[0] + getWidth() / 2)) / (getWidth() / 2);
		double yndeb = (yw - (viewport[1] + getHeight() / 2)) / (getHeight() / 2);

		point = new GVector(new double[] { xndeb, yndeb, -6, 1 });

		projectionMatrix.invert();
		viewMatrix.invert();
		projectionMatrix.mul(viewMatrix);
		point.mul(projectionMatrix, point);
		// point.mul(viewMatrix, point);

		return new Point2D(xw, getHeight() - yw + 1);
	}
}
